// Interactiveness now
$(document).ajaxStop($.unblockUI);
(function() {

    $.fn.bootstrapSwitch.defaults.onColor = 'calmgreen';
    $.fn.bootstrapSwitch.defaults.offColor = 'calmred';

    $("[data-toggle='switch-default']").bootstrapSwitch.defaults.onText = 'Yes';
    $("[data-toggle='switch-default']").bootstrapSwitch.defaults.offText = 'No';
    $("[data-toggle='switch-default']").bootstrapSwitch();

    $("[data-toggle='switch-active']").bootstrapSwitch.defaults.onText = 'Active';
    $("[data-toggle='switch-active']").bootstrapSwitch.defaults.offText = 'Inactive';
    $("[data-toggle='switch-active']").bootstrapSwitch();

    // $("#sidebar-list").niceScroll({
    //     background: 'rgba(0,0,0,0.3)', 
    //     cursorcolor: 'rgba(3,155,230,0.5)', 
    //     cursorborder: '', 
    //     cursorborderradius: '3px', 
    //     cursorwidth: '5', 
    //     hidecursordelay: 300,
    //     mousescrollstep: 20,
    //     scrollspeed: 20,
    //     zindex: '2000'
    // });

    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 850) {
                $("#container").removeClass("sidebar-opened");
                $("#container").addClass('sidebar-closed');
            }
            if (wSize > 850) {
                $("#container").removeClass("sidebar-closed");
                $("#container").addClass("sidebar-opened");
            }
            if(wSize <= 480) {
                setTimeout(function () { //for mobile view
                    $(('textarea')).css({
                        'height': 'auto'
                    });
                }, 100);
            }
            if(wSize > 480) {
                setTimeout(function () { //for mobile view
                    $(('textarea')).css({
                        'height': 'none'
                    });
                }, 100);
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });

    $('#toggle-nav').click(function () {
        if ($('#container').hasClass("sidebar-opened") === true) {
            $("#container").removeClass("sidebar-opened");
            $("#container").addClass("sidebar-closed");
        } else {
            $("#container").removeClass("sidebar-closed");
            $("#container").addClass("sidebar-opened");
        }
    });

    if ($('#sidebar-list ul .active').length > 0) { 

        //Keep panel open when child is active
        var activeParent = $('#sidebar-list ul .active').parent();
        activeParent.addClass("in");
        activeParent.prev(".list-group-title").css({"background-color": "rgba(0,0,0,0.2)", "color": "#83BF1D"});

        //Scroll nav bar to active child
        var position = $('#sidebar-list ul .active').position();
        var viewportHeight = screen.height;
        if ( position.top > viewportHeight/3 )
        {
          $('#sidebar-list').animate({
            scrollTop: position.top - viewportHeight/3
          }, 450, "swing");
        }
    }

    $(':not([title=""])').tooltip();
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on('click','.push_notification',function(event) {
        event.preventDefault();
        var $self = $(this);
        alertify.confirm('This may take several minutes.<br />Do you want to continue?').set('onok', function(closeEvent){
            $.blockUI({ message: '<h1>Sending notification...</h1>' });
            $.ajax({
                url: $self.data('href'),
                type: 'GET',
                success: function(result){
                    
                }
            });
        });
    });

    $(document).on('click','.delete',function(event) {
        event.preventDefault();
        var $self = $(this);
        alertify.confirm('Are you sure to delete this record?').set('onok', function(closeEvent){
            if($self.attr('href') != '#')
            {
                $.ajax({
                    url: $self.data('href'),
                    type: 'DELETE',
                    success: function(result){
                        $self.closest('tr').remove();
                        alertify.success('Record Deleted.');
                    }
                });
            }else{
                $self.closest('tr').remove();
                alertify.success('Record Deleted.');
            }
        });
    });
    
    autosize($('textarea'));
    $('.select2').select2();
    $('.editor').trumbowyg();
    $('.datetimepicker').datetimepicker({
        format:'Y-m-d H:i:s'
    });
    $('.datepicker').datetimepicker({
        timepicker:false,
        format:'Y-m-d'
    });
    $('.timepicker').datetimepicker({
        datepicker:false,
        format:'H:i',
        'step':30
    });
}());

//Img container style and preview
$('.imgForm-container :file').on('change', function() {
    var input = $(this),
    numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);

    var previewID = input.closest('.imgForm-container').find('img').prop('id');
    previewImg(this,previewID);
});

$('.imgForm-container :file').on('fileselect', function(event, numFiles, label) {
    var input = $(this).parents('.imgBrowse-browse-frame').find(':text'), 
        log = numFiles > 1 ? numFiles + ' files selected' : label;
    
    if( input.length ) {
        input.val(log);
    } else {
        if( log ) {
            alert(log);
        }
    }
});
//End

//File container style
$('.fileForm-container :file').on('change', function() {
    var input = $(this),
    numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

$('.fileForm-container :file').on('fileselect', function(event, numFiles, label) {
    var input = $(this).parents('.fileBrowse-browse-frame').find(':text'), 
        log = numFiles > 1 ? numFiles + ' files selected' : label;
    
    if( input.length ) {
        input.val(log);
    } else {
        if( log ) {
            alert(log);
        }
    }
});
//End

function previewImg(input,previewBox) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+previewBox).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }

}