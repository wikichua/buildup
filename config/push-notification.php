<?php

return array(

    // 'appNameIOS'     => array(
    //     'environment' =>env('TECH_PUSH_ENV'),
    //     'certificate' =>storage_path(env('IOS_CERT')),
    //     'passPhrase'  =>'qazwsx12',
    //     'service'     =>'apns'
    // ),
    // 'appNameAndroid' => array(
    //     'environment' =>env('TECH_PUSH_ENV'),
    //     'apiKey'      =>env('AND_KEY'),
    //     'service'     =>'gcm'
    // ),
    'techdome_fcm' => array(
        'environment' =>env('TECH_PUSH_ENV'),
        'apiKey'      =>env('FCM_KEY'),
        'service'     =>'fcm'
    )

);