<?php

return array(
	'brand' => 'Build Up',

	'applications' => array(
			'SAS' => 'Student Attendance System',
			'CHAMP' => 'Champs Quiz Game',
			'LBA' => 'LB Aluminium',
		),

	'modules' => array(
			'ADT_MGMT' 		=> 'Audit Trails',
			'USR_MGMT' 		=> 'Users Management',
			'USR_GRP_MGMT' 	=> 'User Groups Management',
			'USR_ACL' 		=> 'Access Control List',

			// SAS
			'SAS_STU' => 'Students',
			'SAS_ATT_REC' => 'Attendance Records',
			// end SAS

			// CHAMP
			'CHAMP_PRO' => 'Player Profiles',
			'CHAMP_SCO' => 'Player Scores',
			'CHAMP_VOU' => 'Vouchers',
			'CHAMP_REDP' => 'Voucher Redemptions',
			'CHAMP_CT' => 'Content',
			'CHAMP_QIMG' => 'Question Image',

			// end CHAMP

			//LBA
			'LBA_ABT_US' => 'About Us',
			'LBA_CTC_US' => 'Contact Us',
			'LBA_BAZ_CAT' =>' Bazaar Catalog',
			'LBA_BAZ_PROD' =>' Bazaar Product',
			'LBA_BAZ_ITM' =>' Bazaar Item',
			'LBA_STD_CAT' =>' Standard Catalog',
			'LBA_STD_PROD' =>' Standard Product',
			'LBA_STD_TSD' =>' Standard Typical Shop Drawing',
			'LBA_STD_SD_CAT' =>' Standard Section Details Category',
			'LBA_STD_SD' =>' Standard Section Details',
			'LBA_PFM_CAT' =>' Performance Catalog',
			'LBA_PFM_PROD' =>' Performance Product',
			'LBA_PFM_TSD' =>' Performance Typical Shop Drawing',
			'LBA_PFM_SD_CAT' =>' Performance Section Details Category',
			'LBA_PFM_SD' =>' Performance Section Details',
			'LBA_PFM_WC' =>' Performance Winload Chart',
			'LBA_PFM_ACC' =>' Performance Accessories',
			'LBA_PROJ_REF' => 'Project References',
			'LBA_ALOY_SPE' => 'Alloy Specification',
			'LBA_PASS_PG' => 'Data Access Password',
			'LBA_PASS_RQ' => 'Password Requester',
			'LBA_SC_PU' => 'Screen PopUp'
			//end LBA
		),

	'menus' => array(
			'ADT_MGMT',
			'USR_MGMT',
			'USR_GRP_MGMT',
			'USR_ACL',

			// SAS
			'SAS_STU',
			'SAS_ATT_REC',
			// end SAS

			// CHAMP
			'CHAMP_PRO',
			'CHAMP_SCO',
			'CHAMP_VOU',
			'CHAMP_REDP',
			'CHAMP_CT',

			// end CHAMP

			// LBA
			'LBA_ABT_US',
			'LBA_CTC_US',
			'LBA_BAZ_CAT',
			'LBA_STD_CAT',
			'LBA_PFM_CAT',
			'LBA_PROJ_REF',
			'LBA_ALOY_SPE',
			'LBA_PASS_PG',
			'LBA_PASS_RQ',
			'LBA_SC_PU'
			// end LBA
		),

	'ACL_USER'	=> array(
			'ADT_MGMT' 		=> array('Read'),
			'USR_MGMT' 		=> array('Create','Read','Update','Delete'),
			'USR_GRP_MGMT' 	=> array('Create','Read','Update','Delete'),
			'USR_ACL' 		=> array('Create','Read'),
			
			// SAS
			'SAS_STU' 		=> array('Create','Read','Update','Delete'),
			'SAS_ATT_REC' 	=> array('Read'),
			// end SAS

			// CHAMP
			'CHAMP_PRO' 	=> array('Read'),
			'CHAMP_SCO' 	=> array('Read'),
			'CHAMP_VOU' 	=> array('Create','Read','Update','Delete'),
			'CHAMP_REDP' 	=> array('Read'),
			'CHAMP_CT'		=> array('Create','Read','Update','Delete'),
			'CHAMP_QIMG'	=> array('Create','Read','Update','Delete'),

			// end CHAMP

			//LBA
			'LBA_ABT_US'		=> array('Read','Update'),
			'LBA_CTC_US'		=> array('Create','Read','Update','Delete'),
			'LBA_BAZ_CAT' 		=> array('Create','Read','Update','Delete'),
			'LBA_BAZ_PROD' 		=> array('Create','Read','Update','Delete'),
			'LBA_BAZ_ITM' 		=> array('Create','Read','Update','Delete'),
			'LBA_STD_CAT' 		=> array('Create','Read','Update','Delete'),
			'LBA_STD_PROD' 		=> array('Create','Read','Update','Delete'),
			'LBA_STD_TSD' 		=> array('Create','Read','Update','Delete'),
			'LBA_STD_SD_CAT' 	=> array('Create','Read','Update','Delete'),
			'LBA_STD_SD' 		=> array('Create','Read','Update','Delete'),
			'LBA_PFM_CAT' 		=> array('Create','Read','Update','Delete'),
			'LBA_PFM_PROD' 		=> array('Create','Read','Update','Delete'),
			'LBA_PFM_TSD' 		=> array('Create','Read','Update','Delete'),
			'LBA_PFM_SD_CAT' 	=> array('Create','Read','Update','Delete'),
			'LBA_PFM_SD' 		=> array('Create','Read','Update','Delete'),
			'LBA_PFM_WC' 		=> array('Create','Read','Update','Delete'),
			'LBA_PFM_ACC' 		=> array('Create','Read','Update','Delete'),
			'LBA_PROJ_REF' 		=> array('Create','Read','Update','Delete'),
			'LBA_ALOY_SPE' 		=> array('Create','Read','Update','Delete'),
			'LBA_PASS_PG' 		=> array('Create','Read','Update','Delete'),
			'LBA_PASS_RQ' 		=> array('Read','Delete'),
			'LBA_SC_PU' 		=> array('Create','Read','Update','Delete'),
			//end LBA
		),

	'index_route_name' => array(
			// SAS
			'SAS_STU' => 'sas.student',
			'SAS_ATT_REC' => 'sas.attendance.record',
			// end SAS

			// CHAMP
			'CHAMP_PRO' => 'champ.profile',
			'CHAMP_SCO' => 'champ.score',
			'CHAMP_VOU' => 'champ.voucher',
			'CHAMP_REDP' => 'champ.redemption',
			'CHAMP_CT' => 'champ.content',
			'CHAMP_QIMG' => 'champ.content',
			// end CHAMP

			//LBA
			'LBA_ABT_US' => 'lba.about.us.edit',
			'LBA_CTC_US' => 'lba.contact.us',
			'LBA_BAZ_CAT' =>'lba.bazaar.catalog',
			'LBA_BAZ_PROD' =>'lba.bazaar.product',
			'LBA_BAZ_ITM' =>'lba.bazaar.item',
			'LBA_STD_CAT' =>'lba.standard.catalog',
			'LBA_STD_PROD' =>'lba.standard.product',
			'LBA_STD_TSD' =>'lba.standard.tsd.gallery',
			'LBA_STD_SD_CAT' =>'lba.standard.sd.gallery.category',
			'LBA_STD_SD' =>'lba.standard.sd.gallery',
			'LBA_PFM_CAT' =>'lba.performance.catalog',
			'LBA_PFM_PROD' =>'lba.performance.product',
			'LBA_PFM_TSD' =>'lba.performance.tsd.gallery',
			'LBA_PFM_SD_CAT' =>'lba.performance.sd.gallery.category',
			'LBA_PFM_SD' =>'lba.performance.sd.gallery',
			'LBA_PFM_WC' =>'lba.performance.wc.gallery',
			'LBA_PFM_ACC' =>'lba.performance.acc.gallery',
			'LBA_PROJ_REF' =>'lba.project.references',
			'LBA_ALOY_SPE' =>'lba.alloy.specification',
			'LBA_PASS_PG' =>'lba.password.page.edit',
			'LBA_PASS_RQ' =>'lba.password.requester',
			'LBA_SC_PU' =>'lba.screen.popup.edit'
			//end LBA
		),

	'navbar_active_route' => array(
			// SAS
			'SAS_STU' => 'sas.student',
			'SAS_ATT_REC' => 'sas.attendance.record',
			// end SAS

			// CHAMP
			'CHAMP_PRO' => 'champ.profile',
			'CHAMP_SCO' => 'champ.score',
			'CHAMP_VOU' => 'champ.voucher',
			'CHAMP_REDP' => 'champ.redemption',
			'CHAMP_CT' => 'champ.content',
			// end CHAMP

			//LBA
			'LBA_ABT_US' => 'lba.about.us.edit',
			'LBA_CTC_US' => 'lba.contact.us',
			'LBA_BAZ_CAT' =>'lba.bazaar',
			'LBA_STD_CAT' =>'lba.standard',
			'LBA_PFM_CAT' =>'lba.performance',
			'LBA_PROJ_REF' =>'lba.project.references',
			'LBA_ALOY_SPE' =>'lba.alloy.specification',
			'LBA_PASS_PG' =>'lba.password.page.edit',
			'LBA_PASS_RQ' =>'lba.password.requester',
			'LBA_SC_PU' =>'lba.screen.popup'
			//end LBA
		),

	'icons' => array(
			// SAS
			'SAS_STU' => 'fa fa-users',
			'SAS_ATT_REC' => 'fa fa-calendar',
			// end SAS

			// CHAMP
			'CHAMP_PRO' => 'fa fa-user',
			'CHAMP_SCO' => 'fa fa-trophy',
			'CHAMP_VOU' => 'fa fa-ticket',
			'CHAMP_REDP' => 'fa fa-gift',
			'CHAMP_CT' => 'fa fa-ticket',

			// end CHAMP

			// LBA
			'LBA_ABT_US' => 'fa fa-user',
			'LBA_CTC_US' => 'fa fa-book',
			'LBA_BAZ_CAT' =>'fa fa-folder',
			'LBA_STD_CAT' =>'fa fa-folder',
			'LBA_PFM_CAT' =>'fa fa-folder',
			'LBA_PROJ_REF' =>'fa fa-newspaper-o',
			'LBA_ALOY_SPE' =>'fa fa-object-group',
			'LBA_PASS_PG' =>'fa fa-key',
			'LBA_PASS_RQ' =>'fa fa-male',
			'LBA_SC_PU' =>'fa fa-picture-o'
			//end LBA
		),

	'placeholder' => array(
			'file' => 'Maximum upload file size : ',
			'img'  => 'Maximum upload image size : ',
		),

	'notify' => array(
			'img' => 'Optimum dimension : ',
			'mandatory' => 'Fields marked with <span class="text-red">*</span> are mandatory',
		),

	'dropdown' => array(
			'contact_types' => array(
	                'Tel'    => 'Tel',
	                'Fax'    => 'Fax',
	                'HP'     => 'HP',
	                'Email'  => 'Email',
	            ),
		),
);