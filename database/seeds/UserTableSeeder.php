<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \BUP\User::create(
    			array(
    					'name' => 'Convep Admin',
						'email' => 'admin@convep.com',
						'password' => bcrypt('ABC123abc'),
						'isAdmin' => true,
                        'status' => 'Active',
						'usergroup_id' => 1,
    				)
    		);
        \BUP\UserGroup::create(
                array(
                        'name' => 'Super User',
                    )
            );
        \BUP\UserGroup::create(
                array(
                        'name' => 'Admin',
                    )
            );
    }
}
