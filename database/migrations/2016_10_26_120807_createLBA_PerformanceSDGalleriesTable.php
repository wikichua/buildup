<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLBAPerformanceSDGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LBA_performance_sd_galleries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('seq');
            $table->bigInteger('gallery_cat_id');
            $table->string('image');
            $table->string('title');
            $table->text('description');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('LBA_performance_sd_galleries');
    }
}
