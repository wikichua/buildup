<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSectionNoAndType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('LBA_performance_sd_galleries', function (Blueprint $table) {
            $table->string('section_no')->default("");
        });

        Schema::table('LBA_performance_product', function (Blueprint $table) {
            $table->string('type')->default("");
        });

        Schema::table('LBA_standard_sd_galleries', function (Blueprint $table) {
            $table->string('section_no')->default("");
        });
        Schema::table('LBA_standard_product', function (Blueprint $table) {
            $table->string('type')->default("");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('LBA_performance_sd_galleries', function (Blueprint $table) {
            $table->dropColumn('section_no');
            
        });
        Schema::table('LBA_standard_sd_galleries', function (Blueprint $table) {
            $table->dropColumn('section_no');
            
        });
        Schema::table('LBA_performance_product', function (Blueprint $table) {
            $table->dropColumn('type');
            
        });
        Schema::table('LBA_standard_product', function (Blueprint $table) {
            $table->dropColumn('type');
            
        });
    }
}
