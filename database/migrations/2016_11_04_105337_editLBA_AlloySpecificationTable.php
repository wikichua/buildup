<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditLBAAlloySpecificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('LBA_alloy_specification', function (Blueprint $table) {
            $table->dropColumn('html');
            $table->text('source')->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('LBA_alloy_specification', function (Blueprint $table) {
            $table->dropColumn('source');
            $table->text('html')->after('name');
        });
    }
}
