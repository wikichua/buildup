<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIssuedAtToCHAMPVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('CHAMP_vouchers', function (Blueprint $table) {
            //$table->dropColumn('image','scan_image','tnc','expired_at');
            $table->datetime('issued_at');
            $table->integer('issued_to_user_id')->default(0);
            $table->string('character')->default("");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CHAMP_vouchers', function (Blueprint $table) {
            $table->dropColumn('issued_at','issued_to_user_id','character');
        });
    }
}
