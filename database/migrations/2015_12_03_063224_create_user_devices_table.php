<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('app')->default('');
            $table->string('imei')->default('');
            $table->text('push_token')->default('');
            $table->string('width')->default('');
            $table->string('height')->default('');
            $table->string('os')->default('');
            $table->string('os_version')->default('');
            $table->string('app_version')->default('');
            $table->string('api_token')->default('');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_devices');
    }
}
