<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditLBAPerformanceProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('LBA_performance_product', function (Blueprint $table) {
            $table->dropColumn('image');
            $table->longText('images')->after('cat_id');
            $table->longText('description')->after('title');
            $table->string('type_system')->after('description');
            $table->string('thickness')->after('type_system');
            $table->string('glass_thickness')->after('thickness');
            $table->string('depth')->after('glass_thickness');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('LBA_performance_product', function (Blueprint $table) {
            $table->dropColumn('images');
            $table->dropColumn('description');
            $table->dropColumn('type_system');
            $table->dropColumn('thickness');
            $table->dropColumn('glass_thickness');
            $table->dropColumn('depth');
            $table->text('image')->after('cat_id');
        });
    }
}
