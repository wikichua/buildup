<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLBAPassworRequesterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LBA_password_requester', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('seq');
            $table->string('name');
            $table->text('company');
            $table->text('email');
            $table->text('phone');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('LBA_password_requester');
    }
}
