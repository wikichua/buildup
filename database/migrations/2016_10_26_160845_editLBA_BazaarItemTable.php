<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditLBABazaarItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('LBA_bazaar_item', function (Blueprint $table) {
            $table->dropColumn('lenged');
            $table->dropColumn('allow_type');
            $table->string('legend')->after('title');
            $table->string('alloy_type')->after('ap');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('LBA_bazaar_item', function (Blueprint $table) {
            $table->dropColumn('legend');
            $table->dropColumn('alloy_type');
            $table->string('lenged')->after('title');
            $table->string('allow_type')->after('ap');
        });
    }
}
