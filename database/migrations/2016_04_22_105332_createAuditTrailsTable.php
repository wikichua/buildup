<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditTrailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_trails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable()->default(0);
            $table->string('model')->nullable()->default('');
            $table->integer('model_id')->nullable()->default(0);
            $table->string('table')->nullable()->default('');
            $table->string('action')->nullable()->default('');
            $table->longText('old_data')->nullable()->default('');
            $table->longText('new_data')->nullable()->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('audit_trails');
    }
}
