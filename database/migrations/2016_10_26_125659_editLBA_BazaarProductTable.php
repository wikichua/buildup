<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditLBABazaarProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('LBA_bazaar_product', function (Blueprint $table) {
            $table->dropColumn('field');
            $table->longText('fields')->after('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('LBA_bazaar_product', function (Blueprint $table) {
            $table->dropColumn('fields');
            $table->longText('field')->after('title');
        });
    }
}
