<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCHAMPVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CHAMP_vouchers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('level');
            $table->string('image');
            $table->string('scan_image');
            $table->string('name');
            $table->longText('description');
            $table->longText('tnc');
            $table->datetime('expired_at');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CHAMP_vouchers');
    }
}
