<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLBABazaarProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LBA_bazaar_product', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('seq');
            $table->bigInteger('cat_id');
            $table->text('image');
            $table->string('title');
            $table->longtext('field');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('LBA_bazaar_product');
    }
}
