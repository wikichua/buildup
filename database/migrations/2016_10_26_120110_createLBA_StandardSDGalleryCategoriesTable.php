<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLBAStandardSDGalleryCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LBA_standard_sd_gallery_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('seq');
            $table->bigInteger('prod_id');
            $table->string('title');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('LBA_standard_sd_gallery_categories');
    }
}
