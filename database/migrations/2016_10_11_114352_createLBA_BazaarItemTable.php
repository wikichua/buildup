<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLBABazaarItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LBA_bazaar_item', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('seq');
            $table->bigInteger('prod_id');
            $table->bigInteger('section_no');
            $table->text('image');
            $table->string('title');
            $table->string('lenged');
            $table->string('ap');
            $table->string('weight');
            $table->string('allow_type');
            $table->longtext('values');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('LBA_bazaar_item');
    }
}
