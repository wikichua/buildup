<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLBAContactUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LBA_contact_us', function (Blueprint $table) {
            $table->increments('id');
            // $table->bigInteger('seq');
            $table->text('title');
            $table->longtext('address');
            $table->longtext('gps')->nullable();
            $table->longtext('tel');
            $table->longtext('tel_label')->nullable();
            $table->longtext('email');
            $table->longtext('email_label')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('LBA_contact_us');
    }
}
