<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditLBAContactUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('LBA_contact_us', function (Blueprint $table) {
            $table->dropColumn('gps');
            $table->dropColumn('tel');
            $table->dropColumn('tel_label');
            $table->dropColumn('email');
            $table->dropColumn('email_label');
            $table->longText('contacts')->after('address');
            $table->string('latitude')->after('contacts');
            $table->string('longitude')->after('latitude');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('LBA_contact_us', function (Blueprint $table) {
            $table->dropColumn('contacts');
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
            $table->longtext('gps')->nullable()->after('address');
            $table->longtext('tel')->after('gps');
            $table->longtext('tel_label')->nullable()->after('tel');
            $table->longtext('email')->after('tel_label');
            $table->longtext('email_label')->nullable()->after('email');
        });
    }
}
