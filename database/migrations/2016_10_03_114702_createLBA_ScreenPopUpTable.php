<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLBAScreenPopUpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LBA_screen_pop_up', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('seq');
            $table->text('image');
            $table->text('url');
            $table->datetime('publish_at');
            $table->datetime('expired_at');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('LBA_screen_pop_up');
    }
}
