<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeqToLBAContactUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('LBA_contact_us', function (Blueprint $table) {
            $table->bigInteger('seq')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('LBA_contact_us', function (Blueprint $table) {
            $table->dropColumn('seq');
        });
    }
}
