<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToSASStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('SAS_students', function (Blueprint $table) {
            $table->longText('uuid')->after('seq');
            $table->string('age')->after('photo');
            $table->string('contact_name_1')->after('age');
            $table->string('contact_number_1')->after('contact_name_1');
            $table->string('contact_name_2')->after('contact_number_1');
            $table->string('contact_number_2')->after('contact_name_2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('SAS_students', function (Blueprint $table) {
            $table->dropColumn('uuid');
            $table->dropColumn('age');
            $table->dropColumn('contact_name_1');
            $table->dropColumn('contact_number_1');
            $table->dropColumn('contact_name_2');
            $table->dropColumn('contact_number_2');
        });
    }
}
