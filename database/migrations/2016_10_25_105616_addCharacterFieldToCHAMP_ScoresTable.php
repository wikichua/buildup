<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCharacterFieldToCHAMPScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('CHAMP_scores', function (Blueprint $table) {
            $table->integer('character_id')->after('total_duration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CHAMP_scores', function (Blueprint $table) {
            $table->dropColumn('character_id');
        });
    }
}
