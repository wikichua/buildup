<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditLBAScreenPopUpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('LBA_screen_pop_up', function (Blueprint $table) {
            $table->dropColumn('publish_at');
            $table->datetime('published_at')->after('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('LBA_screen_pop_up', function (Blueprint $table) {
            $table->dropColumn('published_at');
            $table->datetime('publish_at')->after('url');
        });
    }
}
