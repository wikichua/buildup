<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditLBAStandardProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('LBA_standard_product', function (Blueprint $table) {
            $table->longText('description')->after('title');
            $table->string('type_system')->after('description');
            $table->string('thickness')->after('type_system');
            $table->string('glass_thickness')->after('thickness');
            $table->string('depth')->after('glass_thickness');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('LBA_standard_product', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('type_system');
            $table->dropColumn('thickness');
            $table->dropColumn('glass_thickness');
            $table->dropColumn('depth');
        });
    }
}
