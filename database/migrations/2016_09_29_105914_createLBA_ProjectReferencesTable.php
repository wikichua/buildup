<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLBAProjectReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LBA_project_references', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('seq');
            $table->text('image')->nullable();
            $table->string('title');
            $table->longtext('description');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('LBA_project_references');
    }
}
