<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
	var $User;
	
	public function setUp()
	{
		parent::setUp();
	}

    public function testLogin()
    {
        return $this->visit('/')
				->type('admin@convep.com', 'email')
				->type('ABC123abc', 'password')
				->press('login')
				->seePageIs('/dashboard');
    }

    public function testCreateUser()
    {
		\CMS\User::where('email','tester@convep.com')->forceDelete();
    	return $this->testLogin()->visit('/user')
    		->click('Create')
    		->seePageIs('/user/create')
    		->type('Tester','name')
    		->type('tester@convep.com','email')
    		->type('ABC123abc','password')
    		->type('ABC123abc','password_confirmation')
    		->select('1','usergroup_id')
    		->check('status')
    		->press('Submit')
    		->seePageIs('/user')
    		->see('Record created.');
    }

    public function testEditUser()
    {
    	$User = \CMS\User::where('email','tester@convep.com')->first();
    	$this->testLogin()->visit('/user/'.$User->id.'/edit')
    		->see('EDIT USER')
    		->type('Mr Tester','name')
    		->uncheck('status')
    		->press('Submit')
    		->seePageIs('/user/'.$User->id.'/edit')
    		->see('Record updated.');
    }

}
