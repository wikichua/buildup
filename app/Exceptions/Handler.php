<?php

namespace BUP\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler {
	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		AuthorizationException::class ,
		HttpException::class ,
		ModelNotFoundException::class ,
		ValidationException::class ,
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e) {
		parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e) {
		if ($e->getCode() == 4003) {
			return $this->response(array('status' => 'failed', 'message' => array('Unauthorised Access.')));
		} elseif ($e                                  ->getCode() == 4004) {
			return Response::make('Unauthorised Access.')->header('Content-Type', "application/json");
		}

		if ($e instanceof \Illuminate\Session\TokenMismatchException) {
			return redirect()->back()->with('error', 'Session Expired. Please relogin again.');
		}

		return parent::render($request, $e);
	}

	/* customize just for c2c */
	private function response($status = array(), $datas = array()) {
		$return = ['AppData' => [
				'module'           => 'no module',
				'settingIndex'     => date('Y-m-d H:i:s'),
				'portal_url'       => route('dashboard'),
				'download_url'     => route('dashboard'),
				'web_url'          => route('dashboard'),
				'totalRecords'     => count($datas),
				'last_updated_at'  => count($datas)?$this->get_last_updated_at($datas):date('Y-m-d H:i:s'),
				'version'          => '',
				'status'           => isset($status['status'])?$status['status']:'success',
				'message'          => isset($status['message'])?$status['message']:'',
			]
		];
		$return['Data'] = array();
		return Response::make(str_replace(': null', ': ""', json_encode($return, JSON_PRETTY_PRINT)))->header('Content-Type', "application/json");
	}
}
