<?php

namespace BUP\Models\SAS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;

class SAS_Attendance_Record extends Model
{
	use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'SAS_attendance_records';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function student()
    {
    	return $this->belongsTo('BUP\Models\SAS\SAS_Student','student_id','id');
    }
}
