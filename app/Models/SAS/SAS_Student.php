<?php

namespace BUP\Models\SAS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;

class SAS_Student extends Model
{
	use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'SAS_students';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

}
