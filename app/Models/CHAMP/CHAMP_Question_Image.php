<?php

namespace BUP\Models\CHAMP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;

class CHAMP_Question_Image extends Model
{
	use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'CHAMP_question_image';
    protected $guarded = [];
    protected $dates = ['deleted_at','expired_at'];

    public function content()
    {
        return $this->belongsTo('BUP\Models\CHAMP\CHAMP_Content','content_id','id');
    }
}
