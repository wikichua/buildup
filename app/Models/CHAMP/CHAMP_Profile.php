<?php

namespace BUP\Models\CHAMP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;

class CHAMP_Profile extends Model
{
	use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'CHAMP_profiles';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('BUP\User','user_id','id');
    }
}
