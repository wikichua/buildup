<?php

namespace BUP\Models\CHAMP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;

class CHAMP_Content extends Model
{
	use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'CHAMP_content';
    protected $guarded = [];
    protected $dates = ['deleted_at','expired_at'];

        public function question_image()
    {
        return $this->hasMany('BUP\Models\CHAMP\CHAMP_Question_Image','content_id','id');
    }
}
