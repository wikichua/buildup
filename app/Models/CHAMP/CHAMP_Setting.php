<?php

namespace BUP\Models\CHAMP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;

class CHAMP_Setting extends Model
{
	use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'CHAMP_settings';
    protected $guarded = [];
    protected $dates = ['deleted_at','expired_at'];
}
