<?php

namespace BUP\Models\LBA;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;


class LBA_Bazaar_Item extends Model
{
    use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'LBA_bazaar_item';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function bazaar_product()
    {
    	return $this->belongsTo('BUP\Models\LBA\LBA_Bazaar_Product','prod_id','id');
    }
}
