<?php

namespace BUP\Models\LBA;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;

class LBA_Performance_ACC_Gallery extends Model
{
    use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'LBA_performance_acc_galleries';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function performance_product()
    {
    	return $this->belongsTo('BUP\Models\LBA\LBA_Performance_Product','prod_id','id');
    }
}

