<?php

namespace BUP\Models\LBA;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;


class LBA_Performance_Product extends Model
{
    use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'LBA_performance_product';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function performance_catalog()
    {
    	return $this->belongsTo('BUP\Models\LBA\LBA_Performance_Catalog','cat_id','id');
    }

     public function performance_sd_gallery_category()
    {
    	return $this->hasMany('BUP\Models\LBA\LBA_Performance_SD_Gallery_Category','prod_id','id');
    }

 	public function performance_tsd_gallery()
    {
    	return $this->hasMany('BUP\Models\LBA\LBA_Performance_TSD_Gallery','prod_id','id');
    }
    public function performance_wc_gallery()
    {
    	return $this->hasMany('BUP\Models\LBA\LBA_Performance_WC_Gallery','prod_id','id');
    }

    public function performance_acc_gallery()
    {
    	return $this->hasMany('BUP\Models\LBA\LBA_Performance_ACC_Gallery','prod_id','id');
    }

}
