<?php

namespace BUP\Models\LBA;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;

class LBA_Performance_SD_Gallery_Category extends Model
{
    use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'LBA_performance_sd_gallery_categories';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function performance_product()
    {
    	return $this->belongsTo('BUP\Models\LBA\LBA_Performance_Product','prod_id','id');
    }

    public function performance_sd_gallery()
    {
    	return $this->hasMany('BUP\Models\LBA\LBA_Performance_SD_Gallery','gallery_cat_id','id');
    }
}


