<?php

namespace BUP\Models\LBA;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;

class LBA_Performance_SD_Gallery extends Model
{
    use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'LBA_performance_sd_galleries';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function performance_sd_gallery_category()
    {
    	return $this->belongsTo('BUP\Models\LBA\LBA_Performance_SD_Gallery_Category','gallery_cat_id','id');
    }
}



