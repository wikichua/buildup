<?php

namespace BUP\Models\LBA;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;

class LBA_Standard_TSD_Gallery extends Model
{
    use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'LBA_standard_tsd_galleries';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function standard_product()
    {
    	return $this->belongsTo('BUP\Models\LBA\LBA_Standard_Product','prod_id','id');
    }
}
