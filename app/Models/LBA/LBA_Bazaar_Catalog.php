<?php

namespace BUP\Models\LBA;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;


class LBA_Bazaar_Catalog extends Model
{
    use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'LBA_bazaar_catalog';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function bazaar_product()
    {
    	return $this->hasMany('BUP\Models\LBA\LBA_Bazaar_Product','cat_id','id');
    }
    public function bazaar_item()
    {
    	return $this->hasMany('BUP\Models\LBA\LBA_Bazaar_Item','prod_id','id');
    }
}
