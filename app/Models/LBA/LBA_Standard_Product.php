<?php

namespace BUP\Models\LBA;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;


class LBA_Standard_Product extends Model
{
    use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'LBA_standard_product';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function standard_catalog()
    {
    	return $this->belongsTo('BUP\Models\LBA\LBA_Standard_Catalog','cat_id','id');
    }

    public function standard_sd_gallery_category()
    {
    	return $this->hasMany('BUP\Models\LBA\LBA_Standard_SD_Gallery_Category','prod_id','id');
    }

 	public function standard_tsd_gallery()
    {
    	return $this->hasMany('BUP\Models\LBA\LBA_Standard_TSD_Gallery','prod_id','id');
    }
    
}
