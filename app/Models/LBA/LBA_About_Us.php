<?php

namespace BUP\Models\LBA;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LBA_About_Us extends Model
{
    use SoftDeletes;

    protected $table = 'LBA_about_us';
    protected $guarded = [];
    protected $dates = ['deleted_at'];}
