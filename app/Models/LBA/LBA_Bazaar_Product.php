<?php

namespace BUP\Models\LBA;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;


class LBA_Bazaar_Product extends Model
{
    use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'LBA_bazaar_product';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function bazaar_catalog()
    {
    	return $this->belongsTo('BUP\Models\LBA\LBA_Bazaar_Catalog','cat_id','id');
    }

     public function bazaar_item()
    {
    	return $this->hasMany('BUP\Models\LBA\LBA_Bazaar_Item','prod_id','id');
    }
}
