<?php

namespace BUP\Models\LBA;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;


class LBA_Contact_Us extends Model
{
    use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'LBA_contact_us';
    protected $guarded = [];
    protected $dates = ['deleted_at'];
}
