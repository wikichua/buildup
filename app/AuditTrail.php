<?php

namespace BUP;

use Illuminate\Database\Eloquent\Model;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;

class AuditTrail extends Model
{
    use SortableTrait;
    use SearchableTrait;

    protected $table = 'audit_trails';
    protected $guarded = [];

    public function User()
    {
        return $this->belongsTo('BUP\User','user_id','id');
    }
}
