<?php
namespace BUP\Http\Misc;

use Auth;
use BUP\AuditTrail;

trait AuditTrailTrait
{
	protected static function boot()
    {
        parent::boot();
        $user_id = Auth::user()? Auth::user()->id:0;
        static::updating(
            function($record) use($user_id)
            {
               	$model = get_class();
               	$table = (new $model)->getTable();
            	$action = 'update';

                $dirty = $record->getDirty();
                $old_data = array();
                $new_data = array();
                foreach ($dirty as $field => $newdata)
                {
                	if(!in_array($field,array('created_at','updated_at','id','password')))
                	{
	                    $olddata = $record->getOriginal($field);
	                    if ($olddata != $newdata)
	                    {
	                    	$old_data[$field] = $olddata;
	                    	$new_data[$field] = $newdata;
	                    }
                	}
                }

                if(count($old_data) && count($new_data))
                {
                	AuditTrail::create(array(
	                		'user_id' => $user_id,
	                		'model' => $model,
	                		'model_id' => $record->id,
	                		'action' => $action,
	                		'table' => $table,
	                		'old_data' => json_encode($old_data),
	                		'new_data' => json_encode($new_data),
	                	));
                }

                return true;
            }
        );

		static::created(
            function($record) use($user_id)
            {
               	$model = get_class();
               	$table = (new $model)->getTable();
            	$action = 'create';

                $dirty = $record->getDirty();
                $old_data = array();
                $new_data = array();
                foreach ($dirty as $field => $newdata)
                {
                	if(!in_array($field,array('created_at','updated_at','id','password')))
                	{
	                    $new_data[$field] = $newdata;
                	}elseif($field == 'id')
                	{
                		$record->id = $newdata;
                	}
                }

                if(count($new_data))
                {
                	AuditTrail::create(array(
	                		'user_id' => $user_id,
	                		'model' => $model,
	                		'model_id' => $record->id,
	                		'action' => $action,
	                		'table' => $table,
	                		'old_data' => null,
	                		'new_data' => json_encode($new_data),
	                	));
                }

                return true;
            }
        );

		static::deleting(
            function($record) use($user_id)
            {
               	$model = get_class();
               	$table = (new $model)->getTable();
            	$action = 'delete';

                $dirty = $record->getOriginal();
                $old_data = array();
                foreach ($dirty as $field => $olddata)
                {
                	if(!in_array($field,array('created_at','updated_at','id','password')))
                	{
	                    $old_data[$field] = $olddata;
                	}
                }

                if(count($old_data))
                {
                	AuditTrail::create(array(
	                		'user_id' => $user_id,
	                		'model' => $model,
	                		'model_id' => $record->id,
	                		'action' => $action,
	                		'table' => $table,
	                		'old_data' => json_encode($old_data),
	                		'new_data' => null,
	                	));
                }

                return true;
            }
        );

    }
}