<?php

function uuid() {
    return sprintf( '%04x%04x-%04x-%08x-%04x-%04x%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        mt_rand( 0, 0xffff ),
        mt_rand( 0, 0x0fff ) | 0x4000,
        mt_rand( 0, 0x3fff ) | 0x8000,
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}

function activeOnCurrentRoute($regex){
  if (preg_match('/\b'.$regex.'(\.)?\b/',Route::currentRouteName())) {
    return 'active';
  }
  return '';
}

function ACLButtonCheck($module,$action = 'Read')
{
    if(auth()->user()->isAdmin)
        return true;
    else
    {
        $ACL = \BUP\ACL::where('usergroup_id',auth()->user()->usergroup_id)->where('module',$module)->first();
        if($ACL)
        {
            $permissions = json_decode($ACL->permission,true);
            if(in_array($action,$permissions))
            {
                return true;
            }
        }
    }

    return false;
}

function ACLAllButtonsCheck($modules,$action)
{
    if(auth()->user()->isAdmin)
        return true;
    else
    {
        $ACLs = \BUP\ACL::where('usergroup_id',auth()->user()->usergroup_id)->whereIn('module',$modules)->get();
        foreach ($ACLs as $ACL) {
            if($ACL)
            {
                $permissions = json_decode($ACL->permission,true);
                if(in_array($action,$permissions))
                {
                    return true;
                }
            }
        }
    }

    return false;
}

function sortTableHeaderSnippet($field='',$fieldname = '')
{
    $fullUrla = $fullUrld = isset(parse_url(Request::fullUrl())['query'])? parse_url(Request::fullUrl())['query']:'';
    parse_str($fullUrla, $fullUrla);
    parse_str($fullUrld, $fullUrld);
    unset($fullUrld['a'],$fullUrla['d']);

    $fullUrla['a'] = $fieldname; 
    $fullUrla = http_build_query($fullUrla);
    $fullUrld['d'] = $fieldname; 
    $fullUrld = http_build_query($fullUrld);
    return $field.' <div class="btn-group btn-group-xs">
            <a href="'.Request::url().'?'.$fullUrla.'" style="position:absolute;top:-12px;left:1px;color:#83bf1d;display:block;overflow:hidden;line-height:1px;float:right"><i class="fa fa-lg fa-caret-up icon-sort"></i></a>
            <a href="'.Request::url().'?'.$fullUrld.'" style="position:absolute;top:-3px;left:1px;color:#83bf1d;display:block;overflow:hidden;line-height:1px;float:right"><i class="fa fa-lg fa-caret-down icon-sort"></i></a>
            </div>';
}

function searchTableHeaderSnippet($field='')
{
    return Form::text('s-'.$field, request()->get('s-'.$field), array('class'=>"input-sm form-control"));
}

function search_reset_buttons()
{
    return '
        <a href="'.Request::url().'" class="btn btn-reset btn">Reset</a>
        <button type="submit" class="btn btn-submit">Search</button>';
}

function action_add_button($route)
{
    return '<a href="'.$route.'" title="Create" data-toggle="tooltip" class="btn btn-basic"><i class="fa fa-plus"></i></a>';
}

function action_update_button($route)
{
    return '<a href="'.$route.'" title="Update" data-toggle="tooltip" class="btn btn-basic"><i class="fa fa-pencil"></i></a>';
}

function shifting_buttons($object,$route1,$route2)
{
    $str = '';
    if(isSearchOrSortExecuted())
        return '';
    if($object->p_id)
        $str .= '<a href="'.$route1.'" class="btn btn-md btn-default" data-toggle="tooltip" data-placement="auto top" title="Shift Up"><span class="fa fa-caret-up"></span></a>';
    if($object->n_id)
        $str .= '<a href="'.$route2.'" class="btn btn-md btn-default" data-toggle="tooltip" data-placement="auto bottom" title="Shift Down"><span class="fa fa-caret-down"></span></a>';
    return $str;
}

function isSearchOrSortExecuted()
{
    $hide = false;
    if(count(request()->all()))
    {
        if(request()->has('a') || request()->has('d'))
            $hide = true;
        else{
            foreach (request()->all() as $key => $value) {
                if(preg_match('/^s-/i', $key))
                {
                    $hide = true;
                    break;
                }
            }
        }
    }
    
    return $hide;
}

function uploadMultipleImage($inputKey,$directory='')
{
    $uploaded = array();
    if (request()->hasFile($inputKey))
    {
        $uploads_path = public_path().'/uploads';
        $time = !empty($directory)? $directory:time();
        $destinationPath = $uploads_path .'/'. $time;
        if(!is_dir($destinationPath))
        {
            mkdir($destinationPath, 0777);
            chmod($destinationPath, 0777);
        }

        $Files = request()->file($inputKey);
        $i = 0;
        foreach ($Files as $File) {
            $originName = $File->getClientOriginalName();
            $originExtension = $File->getClientOriginalExtension();
            $fileName = $inputKey.'_'. (str_replace(array('.',' '),'',microtime(true))) . '_'.(++$i).'.' . $originExtension;
            Image::make($File)->save($destinationPath.'/'.$fileName);
            chmod($destinationPath.'/'.$fileName, 0777);
            
            $uploaded[] = $time .'/'. $fileName;
        }
    }
    
    return $uploaded;
}

function uploadImage($inputKey,$existFile='',$directory='')
{
    if (request()->hasFile($inputKey))
    {
        $uploads_path = public_path().'/uploads';
        $time = !empty($directory)? $directory:time();
        $destinationPath = $uploads_path .'/'. $time;
        if(!is_dir($destinationPath))
        {
            mkdir($destinationPath, 0777);
            chmod($destinationPath, 0777);
        }

        $File = request()->file($inputKey);
        $originName = $File->getClientOriginalName();
        $originExtension = $File->getClientOriginalExtension();
        $fileName = $inputKey.'_'. (str_replace(array('.',' '),'',microtime(true))) . '.' . $originExtension;
        Image::make($File)->save($destinationPath.'/'.$fileName);
        chmod($destinationPath.'/'.$fileName, 0777);
        if($existFile != '' && file_exists($uploads_path .'/'.$existFile))
        {
            chmod($uploads_path.'/'.$existFile, 0777);
            unlink($uploads_path .'/'.$existFile);
        }
        return $time .'/'. $fileName;
    }else
       return $existFile;
}

function removeUploadFile($file)
{
    $uploads_path = public_path().'/uploads';
    if($file != '' && file_exists($uploads_path .'/'.$file))
    {
        chmod($uploads_path .'/'.$file, 0777);
        return unlink($uploads_path .'/'.$file);
    }
    return;
}

function removeUploadMultipleFile($files)
{
    $uploads_path = public_path().'/uploads';
    foreach (json_decode($files)? json_decode($files,true):array() as $file) {
        if($file != '' && file_exists($uploads_path .'/'.$file))
        {
            chmod($uploads_path .'/'.$file, 0777);
            unlink($uploads_path .'/'.$file);
        }
    }
    return;
}

function uploadFile($inputKey,$existFile='',$directory='')
{
    if (request()->hasFile($inputKey))
    {
        $uploads_path = public_path().'/uploads';
        $time = !empty($directory)? $directory:time();
        $destinationPath = $uploads_path .'/'. $time;
        if(!is_dir($destinationPath))
        {
            mkdir($destinationPath, 0777);
            chmod($destinationPath, 0777);
        }

        $File = request()->file($inputKey);
        $originName = $File->getClientOriginalName();
        $originExtension = $File->getClientOriginalExtension();
        $fileName = $inputKey.'_'. (str_replace(array('.',' '),'',microtime(true))) . '.' . $originExtension;
        $result = $File->move($destinationPath,$fileName);
        chmod($destinationPath.'/'.$fileName, 0777);
        if($existFile != '' && file_exists($uploads_path .'/'.$existFile))
        {
            chmod($uploads_path.'/'.$existFile, 0777);
            unlink($uploads_path .'/'.$existFile);
        }

        return $time .'/'. $fileName;
    }

    return $existFile;
}

function uploadMultipleFile($inputKey,$directory='')
{
    $uploaded = array();
    if (request()->hasFile($inputKey))
    {
        $uploads_path = public_path().'/uploads';
        $time = !empty($directory)? $directory:time();
        $destinationPath = $uploads_path .'/'. $time;
        if(!is_dir($destinationPath))
        {
            mkdir($destinationPath, 0777);
            chmod($destinationPath, 0777);
        }

        $Files = request()->file($inputKey);
        $i = 0;
        foreach ($Files as $File) {
            $originName = $File->getClientOriginalName();
            $originExtension = $File->getClientOriginalExtension();
            $fileName = $inputKey.'_'. (str_replace(array('.',' '),'',microtime(true))) . '_'.(++$i).'.' . $originExtension;
            $result = $File->move($destinationPath,$fileName);
            chmod($destinationPath.'/'.$fileName, 0777);
            
            $uploaded[] = $time .'/'. $fileName;
        }
    }
    
    return $uploaded;
}

function imgTagShow($filename,$type='default')
{
    $nopic = array(
            'default'=>'no_image.jpg',
            'profile'=>'no-profile.gif',
        );
    $pic = $nopic[strtolower($type)];
    if (!empty(trim($filename)) && file_exists(public_path().'/uploads/'.$filename))
        $pic = '/uploads/'.$filename;
    else
        $pic = './img/'.$pic;

    return $pic;
}

function fileTagShow($filename)
{
    if (!empty(trim($filename)) && file_exists(public_path().'/uploads/'.$filename))
    {
        return '<a href="'.asset('uploads/'.$filename).'" target="_blank" class="btn" data-toggle="tooltip" title="Click to download">'.basename($filename).'</a>';
    }
    return '';
}

function jsonToHTMLList($D){
    $string = '';
    $data = $D;
    if(!is_array($D))
    {
        $data = json_decode($D,true);
    }
    if(is_array($data) && count($data))
    {
        $string .= '<ul>';
        foreach ($data as $key => $value) {
            $string .= '<li>';
            if(is_array($value) || json_decode($value))
            {
                $string .= ucwords(str_replace('_', ' ', $key)) .' : '. jsonToHTMLList($value);
            }else{
                $string .= ucwords(str_replace('_', ' ', $key)) .' : '. $string .= ucwords(str_replace('_', ' ', $key)) .' : '. (trim($value)==''? 'N/A':$value);
            }
            $string .= '</li>';
        }
        $string .= '</ul>';
    }

    return $string;
}

function Push_FCM($appName,$message,$devices,$env='production',$protocol='',$id='',$custom_data='',$queue='')
{
    // Queue::push(function($job) use($message,$devices,$env,$protocol,$id,$custom_data)
    // {
        $title = 'Content Update';
        if($custom_data == '') $custom_data = array();
        config(['push-notification.appNameAndroid_Firebase.environment'=>$env]);
        $apiKey = config()->get('push-notification.'.strtolower($appName).'_fcm.apiKey');
        $client = new \paragraph1\phpFCM\Client();
        $client->setApiKey($apiKey);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        foreach ($devices as $deviceToken) {
            $Message = new \paragraph1\phpFCM\Message();
            $Message->addRecipient(new \paragraph1\phpFCM\Recipient\Device(str_replace(' ', '', $deviceToken)));
            $UserDevice = BUP\UserDevice::where('push_token',$deviceToken)->first();
        }

        $Notification = new \paragraph1\phpFCM\Notification($title, $message);
        if($protocol != '' && $id != '')
        {
            $Notification->setClickAction($protocol);
            $Message->setNotification($Notification)->setData(
                array_merge(
                    array(
                        'protocol' => $protocol,
                        'id' => $id,
                    ),
                    $custom_data
                )
            );
        }else{
            $Message->setNotification($Notification);
        }

        $Response = $client->send($Message);
        $result = $Response->getStatusCode();
        $success = $result == 200? 1:0;
        $error = $result;
        if(count($devices) == 1)
        {
            \BUP\PushRecord::create(array(
                'device_id' => $UserDevice->id,
                'os' => $UserDevice->os,
                'protocol' => $protocol,
                'module_id' => $id,
                'message' => $message,
                'success' => $success,
                'error' => $error,
            ));
        }

        // $job->delete();
    // },array(),$queue);
}

function PushNotification($appName,$module,$id,$message,$UserDevices,$env='development',$custom_data='',$queue='default')
{
    foreach ($UserDevices as $UserDevice) {
        if ($env == 'development') {
            if($UserDevice->app_version < 0)
            {
                PushIt($appName,$UserDevice,$message,$env,$module,$id,$custom_data,$queue);
            }
        }else{
            if($UserDevice->app_version > 0)
            {
                PushIt($appName,$UserDevice,$message,$env,$module,$id,$custom_data,$queue);
            }
        }
    }
}

function PushIt($appName,$UserDevice,$message,$env,$module,$id,$custom_data,$queue)
{
    $PT[] = $UserDevice->push_token;
    try {
        Push_FCM($appName,$message,$PT,$env,$module,$id,$custom_data,$queue);
    } catch (Exception $e) {
        if($env == 'development')
            dd($e->getMessage());
    }
}