<?php

Route::group(['middleware' => ['web']], function () {
	Route::get('/reseq', function () {

		$LBA_Bazaar_Item_max=BUP\Models\LBA\LBA_Bazaar_Item::max("prod_id");
			for($i=1;$i<=$LBA_Bazaar_Item_max;$i++){
				$tsd=BUP\Models\LBA\LBA_Bazaar_Item::where('prod_id',$i)->withTrashed()->get();
				foreach ($tsd as $key => $value) {
					$value->seq=$key+1;
					$value->save();
				}
			}
			$LBA_Bazaar_Product_max=BUP\Models\LBA\LBA_Bazaar_Product::max("cat_id");
			for($i=1;$i<=$LBA_Bazaar_Product_max;$i++){
				$tsd=BUP\Models\LBA\LBA_Bazaar_Product::where('cat_id',$i)->withTrashed()->get();
				foreach ($tsd as $key => $value) {
					$value->seq=$key+1;
					$value->save();
				}
			}
			$LBA_Performance_TSD_Gallery_max=BUP\Models\LBA\LBA_Performance_TSD_Gallery::max("prod_id");
			for($i=1;$i<=$LBA_Performance_TSD_Gallery_max;$i++){
				$tsd=BUP\Models\LBA\LBA_Performance_TSD_Gallery::where('prod_id',$i)->withTrashed()->get();
				foreach ($tsd as $key => $value) {
					$value->seq=$key+1;
					$value->save();
				}
			}
			$LBA_Performance_WC_Gallery_max=BUP\Models\LBA\LBA_Performance_WC_Gallery::max("prod_id");
			for($i=1;$i<=$LBA_Performance_WC_Gallery_max;$i++){
				$tsd=BUP\Models\LBA\LBA_Performance_WC_Gallery::where('prod_id',$i)->withTrashed()->get();
				foreach ($tsd as $key => $value) {
					$value->seq=$key+1;
					$value->save();
				}
			}
			$LBA_Standard_Product_max=BUP\Models\LBA\LBA_Standard_Product::max("cat_id");
			for($i=1;$i<=$LBA_Standard_Product_max;$i++){
				$tsd=BUP\Models\LBA\LBA_Standard_Product::where('cat_id',$i)->withTrashed()->get();
				foreach ($tsd as $key => $value) {
					$value->seq=$key+1;
					$value->save();
				}
			}
			$LBA_Standard_SD_Gallery_Category_max=BUP\Models\LBA\LBA_Standard_SD_Gallery_Category::max("prod_id");
			for($i=1;$i<=$LBA_Standard_SD_Gallery_Category_max;$i++){
				$tsd=BUP\Models\LBA\LBA_Standard_SD_Gallery_Category::where('prod_id',$i)->withTrashed()->get();
				foreach ($tsd as $key => $value) {
					$value->seq=$key+1;
					$value->save();
				}
			}
			$LBA_Standard_SD_Gallery_max=BUP\Models\LBA\LBA_Standard_SD_Gallery::max("gallery_cat_id");
			for($i=1;$i<=$LBA_Standard_SD_Gallery_max;$i++){
				$tsd=BUP\Models\LBA\LBA_Standard_SD_Gallery::where('gallery_cat_id',$i)->withTrashed()->get();
				foreach ($tsd as $key => $value) {
					$value->seq=$key+1;
					$value->save();
				}
			}
			$LBA_Standard_TSD_Gallery_max=BUP\Models\LBA\LBA_Standard_TSD_Gallery::max("prod_id");
			for($i=1;$i<=$LBA_Standard_TSD_Gallery_max;$i++){
				$tsd=BUP\Models\LBA\LBA_Standard_TSD_Gallery::where('prod_id',$i)->withTrashed()->get();
				foreach ($tsd as $key => $value) {
					$value->seq=$key+1;
					$value->save();
				}
			}




		
		$bazaar_item=BUP\Models\LBA\LBA_Bazaar_Item::all();

	});
});

Route::group(['middleware' => ['web','auth']], function () {

	Route::group(['prefix' => 'lba'], function () {
		Route::get('about/us/edit', array('as'=>'lba.about.us.edit','uses'=>'LBA\LBA_AboutUsController@edit'));
		Route::put('about/us/update', array('as'=>'lba.about.us.update','uses'=>'LBA\LBA_AboutUsController@update'));

		Route::get('contact/us', array('as'=>'lba.contact.us','uses'=>'LBA\LBA_ContactUsController@index'));
		Route::get('contact/us/create', array('as'=>'lba.contact.us.create','uses'=>'LBA\LBA_ContactUsController@create'));
		Route::post('contact/us/store', array('as'=>'lba.contact.us.store','uses'=>'LBA\LBA_ContactUsController@store'));
		Route::get('contact/us/{id}/edit', array('as'=>'lba.contact.us.edit','uses'=>'LBA\LBA_ContactUsController@edit'));
		Route::put('contact/us/{id}/update', array('as'=>'lba.contact.us.update','uses'=>'LBA\LBA_ContactUsController@update'));
		Route::delete('contact/us/{id}/destroy', array('as'=>'lba.contact.us.destroy','uses'=>'LBA\LBA_ContactUsController@destroy'));
		Route::get('contact/us/{id}/shift/{shift_id}', array('as'=>'lba.contact.us.shift','uses'=>'LBA\LBA_ContactUsController@shift'));

		Route::get('bazaar/catalog', array('as'=>'lba.bazaar.catalog','uses'=>'LBA\LBA_BazaarCatalogController@index'));
		Route::get('bazaar/catalog/create', array('as'=>'lba.bazaar.catalog.create','uses'=>'LBA\LBA_BazaarCatalogController@create'));
		Route::post('bazaar/catalog/store', array('as'=>'lba.bazaar.catalog.store','uses'=>'LBA\LBA_BazaarCatalogController@store'));
		Route::get('bazaar/catalog/{id}/edit', array('as'=>'lba.bazaar.catalog.edit','uses'=>'LBA\LBA_BazaarCatalogController@edit'));
		Route::put('bazaar/catalog/{id}/update', array('as'=>'lba.bazaar.catalog.update','uses'=>'LBA\LBA_BazaarCatalogController@update'));
		Route::delete('bazaar/catalog/{id}/destroy', array('as'=>'lba.bazaar.catalog.destroy','uses'=>'LBA\LBA_BazaarCatalogController@destroy'));
		Route::get('bazaar/catalog/{id}/shift/{shift_id}', array('as'=>'lba.bazaar.catalog.shift','uses'=>'LBA\LBA_BazaarCatalogController@shift'));

		Route::get('bazaar/catalog/{cat_id}/product', array('as'=>'lba.bazaar.product','uses'=>'LBA\LBA_BazaarProductController@index'));
		Route::get('bazaar/catalog/{cat_id}/product/create', array('as'=>'lba.bazaar.product.create','uses'=>'LBA\LBA_BazaarProductController@create'));
		Route::post('bazaar/catalog/{cat_id}/product/store', array('as'=>'lba.bazaar.product.store','uses'=>'LBA\LBA_BazaarProductController@store'));
		Route::get('bazaar/catalog/{cat_id}/product/{id}/edit', array('as'=>'lba.bazaar.product.edit','uses'=>'LBA\LBA_BazaarProductController@edit'));
		Route::put('bazaar/catalog/{cat_id}/product/{id}/update', array('as'=>'lba.bazaar.product.update','uses'=>'LBA\LBA_BazaarProductController@update'));
		Route::delete('bazaar/catalog/{cat_id}/product/{id}/destroy', array('as'=>'lba.bazaar.product.destroy','uses'=>'LBA\LBA_BazaarProductController@destroy'));
		Route::get('bazaar/catalog/{cat_id}/product/{id}/shift/{shift_id}', array('as'=>'lba.bazaar.product.shift','uses'=>'LBA\LBA_BazaarProductController@shift'));

		Route::get('bazaar/catalog/{cat_id}/product/{prod_id}/item', array('as'=>'lba.bazaar.item','uses'=>'LBA\LBA_BazaarItemController@index'));
		Route::get('bazaar/catalog/{cat_id}/product/{prod_id}/item/create', array('as'=>'lba.bazaar.item.create','uses'=>'LBA\LBA_BazaarItemController@create'));
		Route::post('bazaar/catalog/{cat_id}/product/{prod_id}/item/store', array('as'=>'lba.bazaar.item.store','uses'=>'LBA\LBA_BazaarItemController@store'));
		Route::get('bazaar/catalog/{cat_id}/product/{prod_id}/item/{id}/edit', array('as'=>'lba.bazaar.item.edit','uses'=>'LBA\LBA_BazaarItemController@edit'));
		Route::put('bazaar/catalog/{cat_id}/product/{prod_id}/item/{id}/update', array('as'=>'lba.bazaar.item.update','uses'=>'LBA\LBA_BazaarItemController@update'));
		Route::delete('bazaar/catalog/{cat_id}/product/{prod_id}/item/{id}/destroy', array('as'=>'lba.bazaar.item.destroy','uses'=>'LBA\LBA_BazaarItemController@destroy'));
		Route::get('bazaar/catalog/{cat_id}/product/{prod_id}/item/{id}/shift/{shift_id}', array('as'=>'lba.bazaar.item.shift','uses'=>'LBA\LBA_BazaarItemController@shift'));

		Route::get('standard/catalog', array('as'=>'lba.standard.catalog','uses'=>'LBA\LBA_StandardCatalogController@index'));
		Route::get('standard/catalog/create', array('as'=>'lba.standard.catalog.create','uses'=>'LBA\LBA_StandardCatalogController@create'));
		Route::post('standard/catalog/store', array('as'=>'lba.standard.catalog.store','uses'=>'LBA\LBA_StandardCatalogController@store'));
		Route::get('standard/catalog/{id}/edit', array('as'=>'lba.standard.catalog.edit','uses'=>'LBA\LBA_StandardCatalogController@edit'));
		Route::put('standard/catalog/{id}/update', array('as'=>'lba.standard.catalog.update','uses'=>'LBA\LBA_StandardCatalogController@update'));
		Route::delete('standard/catalog/{id}/destroy', array('as'=>'lba.standard.catalog.destroy','uses'=>'LBA\LBA_StandardCatalogController@destroy'));
		Route::get('standard/catalog/{id}/shift/{shift_id}', array('as'=>'lba.standard.catalog.shift','uses'=>'LBA\LBA_StandardCatalogController@shift'));

		Route::get('standard/catalog/{cat_id}/product', array('as'=>'lba.standard.product','uses'=>'LBA\LBA_StandardProductController@index'));
		Route::get('standard/catalog/{cat_id}/product/create', array('as'=>'lba.standard.product.create','uses'=>'LBA\LBA_StandardProductController@create'));
		Route::post('standard/catalog/{cat_id}/product/store', array('as'=>'lba.standard.product.store','uses'=>'LBA\LBA_StandardProductController@store'));
		Route::get('standard/catalog/{cat_id}/product/{id}/edit', array('as'=>'lba.standard.product.edit','uses'=>'LBA\LBA_StandardProductController@edit'));
		Route::put('standard/catalog/{cat_id}/product/{id}/update', array('as'=>'lba.standard.product.update','uses'=>'LBA\LBA_StandardProductController@update'));
		Route::delete('standard/catalog/{cat_id}/product/{id}/destroy', array('as'=>'lba.standard.product.destroy','uses'=>'LBA\LBA_StandardProductController@destroy'));
		Route::get('standard/catalog/{cat_id}/product/{id}/shift/{shift_id}', array('as'=>'lba.standard.product.shift','uses'=>'LBA\LBA_StandardProductController@shift'));

		Route::get('standard/catalog/{cat_id}/product/{prod_id}/tsd', array('as'=>'lba.standard.tsd.gallery','uses'=>'LBA\LBA_StandardTSDGalleryController@index'));
		Route::get('standard/catalog/{cat_id}/product/{prod_id}/tsd/create', array('as'=>'lba.standard.tsd.gallery.create','uses'=>'LBA\LBA_StandardTSDGalleryController@create'));
		Route::post('standard/catalog/{cat_id}/product/{prod_id}/tsd/store', array('as'=>'lba.standard.tsd.gallery.store','uses'=>'LBA\LBA_StandardTSDGalleryController@store'));
		Route::get('standard/catalog/{cat_id}/product/{prod_id}/tsd/{id}/edit', array('as'=>'lba.standard.tsd.gallery.edit','uses'=>'LBA\LBA_StandardTSDGalleryController@edit'));
		Route::put('standard/catalog/{cat_id}/product/{prod_id}/tsd/{id}/update', array('as'=>'lba.standard.tsd.gallery.update','uses'=>'LBA\LBA_StandardTSDGalleryController@update'));
		Route::delete('standard/catalog/{cat_id}/product/{prod_id}/tsd/{id}/destroy', array('as'=>'lba.standard.tsd.gallery.destroy','uses'=>'LBA\LBA_StandardTSDGalleryController@destroy'));
		Route::get('standard/catalog/{cat_id}/product/{prod_id}/tsd/{id}/shift/{shift_id}', array('as'=>'lba.standard.tsd.gallery.shift','uses'=>'LBA\LBA_StandardTSDGalleryController@shift'));

		Route::get('standard/catalog/{cat_id}/product/{prod_id}/sd/category', array('as'=>'lba.standard.sd.gallery.category','uses'=>'LBA\LBA_StandardSDGalleryCategoryController@index'));
		Route::get('standard/catalog/{cat_id}/product/{prod_id}/sd/category/create', array('as'=>'lba.standard.sd.gallery.category.create','uses'=>'LBA\LBA_StandardSDGalleryCategoryController@create'));
		Route::post('standard/catalog/{cat_id}/product/{prod_id}/sd/category/store', array('as'=>'lba.standard.sd.gallery.category.store','uses'=>'LBA\LBA_StandardSDGalleryCategoryController@store'));
		Route::get('standard/catalog/{cat_id}/product/{prod_id}/sd/category/{id}/edit', array('as'=>'lba.standard.sd.gallery.category.edit','uses'=>'LBA\LBA_StandardSDGalleryCategoryController@edit'));
		Route::put('standard/catalog/{cat_id}/product/{prod_id}/sd/category/{id}/update', array('as'=>'lba.standard.sd.gallery.category.update','uses'=>'LBA\LBA_StandardSDGalleryCategoryController@update'));
		Route::delete('standard/catalog/{cat_id}/product/{prod_id}/sd/category/{id}/destroy', array('as'=>'lba.standard.sd.gallery.category.destroy','uses'=>'LBA\LBA_StandardSDGalleryCategoryController@destroy'));
		Route::get('standard/catalog/{cat_id}/product/{prod_id}/sd/category/{id}/shift/{shift_id}', array('as'=>'lba.standard.sd.gallery.category.shift','uses'=>'LBA\LBA_StandardSDGalleryCategoryController@shift'));

		Route::get('standard/catalog/{cat_id}/product/{prod_id}/sd/category/{gallery_cat_id}/sd', array('as'=>'lba.standard.sd.gallery','uses'=>'LBA\LBA_StandardSDGalleryController@index'));
		Route::get('standard/catalog/{cat_id}/product/{prod_id}/sd/category/{gallery_cat_id}/sd/create', array('as'=>'lba.standard.sd.gallery.create','uses'=>'LBA\LBA_StandardSDGalleryController@create'));
		Route::post('standard/catalog/{cat_id}/product/{prod_id}/sd/category/{gallery_cat_id}/sd/store', array('as'=>'lba.standard.sd.gallery.store','uses'=>'LBA\LBA_StandardSDGalleryController@store'));
		Route::get('standard/catalog/{cat_id}/product/{prod_id}/sd/category/{gallery_cat_id}/sd/{id}/edit', array('as'=>'lba.standard.sd.gallery.edit','uses'=>'LBA\LBA_StandardSDGalleryController@edit'));
		Route::put('standard/catalog/{cat_id}/product/{prod_id}/sd/category/{gallery_cat_id}/sd/{id}/update', array('as'=>'lba.standard.sd.gallery.update','uses'=>'LBA\LBA_StandardSDGalleryController@update'));
		Route::delete('standard/catalog/{cat_id}/product/{prod_id}/sd/category/{gallery_cat_id}/sd/{id}/destroy', array('as'=>'lba.standard.sd.gallery.destroy','uses'=>'LBA\LBA_StandardSDGalleryController@destroy'));
		Route::get('standard/catalog/{cat_id}/product/{prod_id}/sd/category/{gallery_cat_id}/sd/{id}/shift/{shift_id}', array('as'=>'lba.standard.sd.gallery.shift','uses'=>'LBA\LBA_StandardSDGalleryController@shift'));

		Route::get('performance/catalog', array('as'=>'lba.performance.catalog','uses'=>'LBA\LBA_PerformanceCatalogController@index'));
		Route::get('performance/catalog/create', array('as'=>'lba.performance.catalog.create','uses'=>'LBA\LBA_PerformanceCatalogController@create'));
		Route::post('performance/catalog/store', array('as'=>'lba.performance.catalog.store','uses'=>'LBA\LBA_PerformanceCatalogController@store'));
		Route::get('performance/catalog/{id}/edit', array('as'=>'lba.performance.catalog.edit','uses'=>'LBA\LBA_PerformanceCatalogController@edit'));
		Route::put('performance/catalog/{id}/update', array('as'=>'lba.performance.catalog.update','uses'=>'LBA\LBA_PerformanceCatalogController@update'));
		Route::delete('performance/catalog/{id}/destroy', array('as'=>'lba.performance.catalog.destroy','uses'=>'LBA\LBA_PerformanceCatalogController@destroy'));
		Route::get('performance/catalog/{id}/shift/{shift_id}', array('as'=>'lba.performance.catalog.shift','uses'=>'LBA\LBA_PerformanceCatalogController@shift'));

		Route::get('performance/catalog/{cat_id}/product', array('as'=>'lba.performance.product','uses'=>'LBA\LBA_PerformanceProductController@index'));
		Route::get('performance/catalog/{cat_id}/product/create', array('as'=>'lba.performance.product.create','uses'=>'LBA\LBA_PerformanceProductController@create'));
		Route::post('performance/catalog/{cat_id}/product/store', array('as'=>'lba.performance.product.store','uses'=>'LBA\LBA_PerformanceProductController@store'));
		Route::get('performance/catalog/{cat_id}/product/{id}/edit', array('as'=>'lba.performance.product.edit','uses'=>'LBA\LBA_PerformanceProductController@edit'));
		Route::put('performance/catalog/{cat_id}/product/{id}/update', array('as'=>'lba.performance.product.update','uses'=>'LBA\LBA_PerformanceProductController@update'));
		Route::delete('performance/catalog/{cat_id}/product/{id}/destroy', array('as'=>'lba.performance.product.destroy','uses'=>'LBA\LBA_PerformanceProductController@destroy'));
		Route::get('performance/catalog/{cat_id}/product/{id}/shift/{shift_id}', array('as'=>'lba.performance.product.shift','uses'=>'LBA\LBA_PerformanceProductController@shift'));

		Route::get('performance/catalog/{cat_id}/product/{prod_id}/tsd', array('as'=>'lba.performance.tsd.gallery','uses'=>'LBA\LBA_PerformanceTSDGalleryController@index'));
		Route::get('performance/catalog/{cat_id}/product/{prod_id}/tsd/create', array('as'=>'lba.performance.tsd.gallery.create','uses'=>'LBA\LBA_PerformanceTSDGalleryController@create'));
		Route::post('performance/catalog/{cat_id}/product/{prod_id}/tsd/store', array('as'=>'lba.performance.tsd.gallery.store','uses'=>'LBA\LBA_PerformanceTSDGalleryController@store'));
		Route::get('performance/catalog/{cat_id}/product/{prod_id}/tsd/{id}/edit', array('as'=>'lba.performance.tsd.gallery.edit','uses'=>'LBA\LBA_PerformanceTSDGalleryController@edit'));
		Route::put('performance/catalog/{cat_id}/product/{prod_id}/tsd/{id}/update', array('as'=>'lba.performance.tsd.gallery.update','uses'=>'LBA\LBA_PerformanceTSDGalleryController@update'));
		Route::delete('performance/catalog/{cat_id}/product/{prod_id}/tsd/{id}/destroy', array('as'=>'lba.performance.tsd.gallery.destroy','uses'=>'LBA\LBA_PerformanceTSDGalleryController@destroy'));
		Route::get('performance/catalog/{cat_id}/product/{prod_id}/tsd/{id}/shift/{shift_id}', array('as'=>'lba.performance.tsd.gallery.shift','uses'=>'LBA\LBA_PerformanceTSDGalleryController@shift'));

		Route::get('performance/catalog/{cat_id}/product/{prod_id}/sd/category', array('as'=>'lba.performance.sd.gallery.category','uses'=>'LBA\LBA_PerformanceSDGalleryCategoryController@index'));
		Route::get('performance/catalog/{cat_id}/product/{prod_id}/sd/category/create', array('as'=>'lba.performance.sd.gallery.category.create','uses'=>'LBA\LBA_PerformanceSDGalleryCategoryController@create'));
		Route::post('performance/catalog/{cat_id}/product/{prod_id}/sd/category/store', array('as'=>'lba.performance.sd.gallery.category.store','uses'=>'LBA\LBA_PerformanceSDGalleryCategoryController@store'));
		Route::get('performance/catalog/{cat_id}/product/{prod_id}/sd/category/{id}/edit', array('as'=>'lba.performance.sd.gallery.category.edit','uses'=>'LBA\LBA_PerformanceSDGalleryCategoryController@edit'));
		Route::put('performance/catalog/{cat_id}/product/{prod_id}/sd/category/{id}/update', array('as'=>'lba.performance.sd.gallery.category.update','uses'=>'LBA\LBA_PerformanceSDGalleryCategoryController@update'));
		Route::delete('performance/catalog/{cat_id}/product/{prod_id}/sd/category/{id}/destroy', array('as'=>'lba.performance.sd.gallery.category.destroy','uses'=>'LBA\LBA_PerformanceSDGalleryCategoryController@destroy'));
		Route::get('performance/catalog/{cat_id}/product/{prod_id}/sd/category/{id}/shift/{shift_id}', array('as'=>'lba.performance.sd.gallery.category.shift','uses'=>'LBA\LBA_PerformanceSDGalleryCategoryController@shift'));

		Route::get('performance/catalog/{cat_id}/product/{prod_id}/sd/category/{gallery_cat_id}/sd', array('as'=>'lba.performance.sd.gallery','uses'=>'LBA\LBA_PerformanceSDGalleryController@index'));
		Route::get('performance/catalog/{cat_id}/product/{prod_id}/sd/category/{gallery_cat_id}/sd/create', array('as'=>'lba.performance.sd.gallery.create','uses'=>'LBA\LBA_PerformanceSDGalleryController@create'));
		Route::post('performance/catalog/{cat_id}/product/{prod_id}/sd/category/{gallery_cat_id}/sd/store', array('as'=>'lba.performance.sd.gallery.store','uses'=>'LBA\LBA_PerformanceSDGalleryController@store'));
		Route::get('performance/catalog/{cat_id}/product/{prod_id}/sd/category/{gallery_cat_id}/sd/{id}/edit', array('as'=>'lba.performance.sd.gallery.edit','uses'=>'LBA\LBA_PerformanceSDGalleryController@edit'));
		Route::put('performance/catalog/{cat_id}/product/{prod_id}/sd/category/{gallery_cat_id}/sd/{id}/update', array('as'=>'lba.performance.sd.gallery.update','uses'=>'LBA\LBA_PerformanceSDGalleryController@update'));
		Route::delete('performance/catalog/{cat_id}/product/{prod_id}/sd/category/{gallery_cat_id}/sd/{id}/destroy', array('as'=>'lba.performance.sd.gallery.destroy','uses'=>'LBA\LBA_PerformanceSDGalleryController@destroy'));
		Route::get('performance/catalog/{cat_id}/product/{prod_id}/sd/category/{gallery_cat_id}/sd/{id}/shift/{shift_id}', array('as'=>'lba.performance.sd.gallery.shift','uses'=>'LBA\LBA_PerformanceSDGalleryController@shift'));

		Route::get('performance/catalog/{cat_id}/product/{prod_id}/wc', array('as'=>'lba.performance.wc.gallery','uses'=>'LBA\LBA_PerformanceWCGalleryController@index'));
		Route::get('performance/catalog/{cat_id}/product/{prod_id}/wc/create', array('as'=>'lba.performance.wc.gallery.create','uses'=>'LBA\LBA_PerformanceWCGalleryController@create'));
		Route::post('performance/catalog/{cat_id}/product/{prod_id}/wc/store', array('as'=>'lba.performance.wc.gallery.store','uses'=>'LBA\LBA_PerformanceWCGalleryController@store'));
		Route::get('performance/catalog/{cat_id}/product/{prod_id}/wc/{id}/edit', array('as'=>'lba.performance.wc.gallery.edit','uses'=>'LBA\LBA_PerformanceWCGalleryController@edit'));
		Route::put('performance/catalog/{cat_id}/product/{prod_id}/wc/{id}/update', array('as'=>'lba.performance.wc.gallery.update','uses'=>'LBA\LBA_PerformanceWCGalleryController@update'));
		Route::delete('performance/catalog/{cat_id}/product/{prod_id}/wc/{id}/destroy', array('as'=>'lba.performance.wc.gallery.destroy','uses'=>'LBA\LBA_PerformanceWCGalleryController@destroy'));
		Route::get('performance/catalog/{cat_id}/product/{prod_id}/wc/{id}/shift/{shift_id}', array('as'=>'lba.performance.wc.gallery.shift','uses'=>'LBA\LBA_PerformanceWCGalleryController@shift'));

		Route::get('performance/catalog/{cat_id}/product/{prod_id}/acc', array('as'=>'lba.performance.acc.gallery','uses'=>'LBA\LBA_PerformanceACCGalleryController@index'));
		Route::get('performance/catalog/{cat_id}/product/{prod_id}/acc/create', array('as'=>'lba.performance.acc.gallery.create','uses'=>'LBA\LBA_PerformanceACCGalleryController@create'));
		Route::post('performance/catalog/{cat_id}/product/{prod_id}/acc/store', array('as'=>'lba.performance.acc.gallery.store','uses'=>'LBA\LBA_PerformanceACCGalleryController@store'));
		Route::get('performance/catalog/{cat_id}/product/{prod_id}/acc/{id}/edit', array('as'=>'lba.performance.acc.gallery.edit','uses'=>'LBA\LBA_PerformanceACCGalleryController@edit'));
		Route::put('performance/catalog/{cat_id}/product/{prod_id}/acc/{id}/update', array('as'=>'lba.performance.acc.gallery.update','uses'=>'LBA\LBA_PerformanceACCGalleryController@update'));
		Route::delete('performance/catalog/{cat_id}/product/{prod_id}/acc/{id}/destroy', array('as'=>'lba.performance.acc.gallery.destroy','uses'=>'LBA\LBA_PerformanceACCGalleryController@destroy'));
		Route::get('performance/catalog/{cat_id}/product/{prod_id}/acc/{id}/shift/{shift_id}', array('as'=>'lba.performance.acc.gallery.shift','uses'=>'LBA\LBA_PerformanceACCGalleryController@shift'));

		Route::get('project/references', array('as'=>'lba.project.references','uses'=>'LBA\LBA_ProjectReferencesController@index'));
		Route::get('project/references/create', array('as'=>'lba.project.references.create','uses'=>'LBA\LBA_ProjectReferencesController@create'));
		Route::post('project/references/store', array('as'=>'lba.project.references.store','uses'=>'LBA\LBA_ProjectReferencesController@store'));
		Route::get('project/references/{id}/edit', array('as'=>'lba.project.references.edit','uses'=>'LBA\LBA_ProjectReferencesController@edit'));
		Route::put('project/references/{id}/update', array('as'=>'lba.project.references.update','uses'=>'LBA\LBA_ProjectReferencesController@update'));
		Route::delete('project/references/{id}/destroy', array('as'=>'lba.project.references.destroy','uses'=>'LBA\LBA_ProjectReferencesController@destroy'));
		Route::get('project/references/{id}/shift/{shift_id}', array('as'=>'lba.project.references.shift','uses'=>'LBA\LBA_ProjectReferencesController@shift'));

		Route::get('alloy/specification', array('as'=>'lba.alloy.specification','uses'=>'LBA\LBA_AlloySpecificationController@index'));
		Route::get('alloy/specification/create', array('as'=>'lba.alloy.specification.create','uses'=>'LBA\LBA_AlloySpecificationController@create'));
		Route::post('alloy/specification/store', array('as'=>'lba.alloy.specification.store','uses'=>'LBA\LBA_AlloySpecificationController@store'));
		Route::get('alloy/specification/{id}/edit', array('as'=>'lba.alloy.specification.edit','uses'=>'LBA\LBA_AlloySpecificationController@edit'));
		Route::put('alloy/specification/{id}/update', array('as'=>'lba.alloy.specification.update','uses'=>'LBA\LBA_AlloySpecificationController@update'));
		Route::delete('alloy/specification/{id}/destroy', array('as'=>'lba.alloy.specification.destroy','uses'=>'LBA\LBA_AlloySpecificationController@destroy'));
		Route::get('alloy/specification/{id}/shift/{shift_id}', array('as'=>'lba.alloy.specification.shift','uses'=>'LBA\LBA_AlloySpecificationController@shift'));

		Route::get('password/page/edit', array('as'=>'lba.password.page.edit','uses'=>'LBA\LBA_PasswordPageController@edit'));
		Route::put('password/page/update', array('as'=>'lba.password.page.update','uses'=>'LBA\LBA_PasswordPageController@update'));

		Route::get('password/requester', array('as'=>'lba.password.requester','uses'=>'LBA\LBA_PasswordRequesterController@index'));
		Route::delete('password/requester/{id}/destroy', array('as'=>'lba.password.requester.destroy','uses'=>'LBA\LBA_PasswordRequesterController@destroy'));

		Route::get('screen/popup', array('as'=>'lba.screen.popup','uses'=>'LBA\LBA_ScreenPopUpController@index'));
		Route::get('screen/popup/create', array('as'=>'lba.screen.popup.create','uses'=>'LBA\LBA_ScreenPopUpController@create'));
		Route::post('screen/popup/store', array('as'=>'lba.screen.popup.store','uses'=>'LBA\LBA_ScreenPopUpController@store'));
		Route::get('screen/popup/edit', array('as'=>'lba.screen.popup.edit','uses'=>'LBA\LBA_ScreenPopUpController@edit'));
		Route::put('screen/popup/{id}/update', array('as'=>'lba.screen.popup.update','uses'=>'LBA\LBA_ScreenPopUpController@update'));
		Route::delete('screen/popup/{id}/destroy', array('as'=>'lba.screen.popup.destroy','uses'=>'LBA\LBA_ScreenPopUpController@destroy'));
		Route::get('screen/popup/{id}/shift/{shift_id}', array('as'=>'lba.screen.popup.shift','uses'=>'LBA\LBA_ScreenPopUpController@shift'));

	});
});

Route::group(['middleware' => ['web'],'prefix' => 'api'], function () {
	
	Route::group(['prefix' => 'lba'], function () {
		Route::get('test/{api?}', array('as'=>'lba.api','uses'=>'LBA\LBA_APIController@index'));
		Route::any('screen_popup', array('as'=>'lba.api.screen_popup','uses'=>'LBA\LBA_APIController@screen_popup'));
		Route::any('bazaar', array('as'=>'lba.api.bazaar','uses'=>'LBA\LBA_APIController@bazaar'));
		Route::any('standard', array('as'=>'lba.api.standard','uses'=>'LBA\LBA_APIController@standard'));
		Route::any('performance', array('as'=>'lba.api.performance','uses'=>'LBA\LBA_APIController@performance'));
		Route::any('about_us', array('as'=>'lba.api.about_us','uses'=>'LBA\LBA_APIController@about_us'));
		Route::any('password', array('as'=>'lba.api.password','uses'=>'LBA\LBA_APIController@password'));
		Route::any('project_references', array('as'=>'lba.api.project_references','uses'=>'LBA\LBA_APIController@project_references'));
		Route::any('alloy_specification', array('as'=>'lba.api.alloy_specification','uses'=>'LBA\LBA_APIController@alloy_specification'));
		Route::any('get_password', array('as'=>'lba.api.get_password','uses'=>'LBA\LBA_APIController@get_password'));
		// Route::get('test/{api?}', array('as'=>'lba.api','uses'=>'LBA\LBA_APIController@index'));
		// Route::any('login', array('as'=>'lba.api.login','uses'=>'LBA\LBA_APIController@login'));
		// Route::any('logout', array('as'=>'lba.api.logout','uses'=>'LBA\LBA_APIController@logout'));
		Route::any('pt', array('as'=>'lba.api.pt','uses'=>'LBA\LBA_APIController@pt'));
	});
});
