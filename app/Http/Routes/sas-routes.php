<?php

Route::group(['middleware' => ['web']], function () {

});

Route::group(['middleware' => ['web','auth']], function () {
	Route::group(['prefix' => 'sas'], function () {
		Route::get('student', array('as'=>'sas.student','uses'=>'SAS\SAS_StudentController@index'));
		Route::get('student/create', array('as'=>'sas.student.create','uses'=>'SAS\SAS_StudentController@create'));
		Route::post('student/store', array('as'=>'sas.student.store','uses'=>'SAS\SAS_StudentController@store'));
		Route::get('student/{id}/edit', array('as'=>'sas.student.edit','uses'=>'SAS\SAS_StudentController@edit'));
		Route::put('student/{id}/update', array('as'=>'sas.student.update','uses'=>'SAS\SAS_StudentController@update'));
		Route::delete('student/{id}/destroy', array('as'=>'sas.student.destroy','uses'=>'SAS\SAS_StudentController@destroy'));
		Route::get('student/{id}/shift/{shift_id}', array('as'=>'sas.student.shift','uses'=>'SAS\SAS_StudentController@shift'));
	
		Route::get('attendance/record', array('as'=>'sas.attendance.record','uses'=>'SAS\SAS_AttendanceRecordController@index'));
	});
});

Route::group(['middleware' => ['web'],'prefix' => 'api'], function () {
	Route::group(['prefix' => 'sas'], function () {
		Route::get('test/{api?}', array('as'=>'sas.api','uses'=>'SAS\SAS_APIController@index'));
		Route::any('login', array('as'=>'sas.api.login','uses'=>'SAS\SAS_APIController@login'));
		Route::any('logout', array('as'=>'sas.api.logout','uses'=>'SAS\SAS_APIController@logout'));
		Route::any('pt', array('as'=>'sas.api.pt','uses'=>'SAS\SAS_APIController@pt'));

		Route::any('attendance_records', array('as'=>'sas.api.attendance_records','uses'=>'SAS\SAS_APIController@attendance_records'));
		
		Route::any('students', array('as'=>'sas.api.students','uses'=>'SAS\SAS_APIController@students'));
		Route::any('add_student', array('as'=>'sas.api.add_student','uses'=>'SAS\SAS_APIController@add_student'));
		Route::any('edit_student', array('as'=>'sas.api.edit_student','uses'=>'SAS\SAS_APIController@edit_student'));
		Route::any('delete_student', array('as'=>'sas.api.delete_student','uses'=>'SAS\SAS_APIController@delete_student'));
		Route::any('scan_code', array('as'=>'sas.api.scan_code','uses'=>'SAS\SAS_APIController@scan_code'));
		Route::any('check_in', array('as'=>'sas.api.check_in','uses'=>'SAS\SAS_APIController@check_in'));
		Route::any('check_out', array('as'=>'sas.api.check_out','uses'=>'SAS\SAS_APIController@check_out'));
		Route::any('check_out_all', array('as'=>'sas.api.check_out_all','uses'=>'SAS\SAS_APIController@check_out_all'));
	});
});
