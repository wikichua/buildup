<?php

Route::group(['middleware' => ['web']], function () {
	Route::get('/', array('as'=>'auth','uses'=>'AuthController@index'));
	Route::post('/login', array('as'=>'login','uses'=>'AuthController@login'));
	Route::get('/logout', array('as'=>'logout','uses'=>'AuthController@logout'));
	Route::get('/link/{app}', array('as'=>'link','uses'=>'LinkController@redirect'));
});

Route::group(['middleware' => ['web','auth']], function () {
	Route::get('/dashboard', array('as'=>'dashboard','uses'=>'DashboardController@index'));
	Route::get('/user', array('as'=>'user','uses'=>'UserController@index'));
	Route::get('/profile', array('as'=>'profile','uses'=>'AuthController@profile'));
	Route::put('/profile/update', array('as'=>'profile.update','uses'=>'AuthController@profile_update'));

	Route::get('/audit/trail', array('as'=>'audit_trail','uses'=>'AuditTrailController@index'));
	Route::get('/audit/trail/{id}/show', array('as'=>'audit_trail.show','uses'=>'AuditTrailController@show'));

	Route::get('/user', array('as'=>'user','uses'=>'UserController@index'));
	Route::get('/user/create', array('as'=>'user.create','uses'=>'UserController@create'));
	Route::post('/user/store', array('as'=>'user.store','uses'=>'UserController@store'));
	Route::get('/user/{id}/edit', array('as'=>'user.edit','uses'=>'UserController@edit'));
	Route::put('/user/{id}/update', array('as'=>'user.update','uses'=>'UserController@update'));
	Route::delete('/user/{id}/destroy', array('as'=>'user.destroy','uses'=>'UserController@destroy'));
	Route::get('/user/{id}/switch', array('as'=>'user.switch','uses'=>'UserController@switch_user'));

	Route::get('/usergroup', array('as'=>'usergroup','uses'=>'UserGroupController@index'));
	Route::get('/usergroup/create', array('as'=>'usergroup.create','uses'=>'UserGroupController@create'));
	Route::post('/usergroup/store', array('as'=>'usergroup.store','uses'=>'UserGroupController@store'));
	Route::get('/usergroup/{id}/edit', array('as'=>'usergroup.edit','uses'=>'UserGroupController@edit'));
	Route::put('/usergroup/{id}/update', array('as'=>'usergroup.update','uses'=>'UserGroupController@update'));
	Route::delete('/usergroup/{id}/destroy', array('as'=>'usergroup.destroy','uses'=>'UserGroupController@destroy'));

	Route::get('/acl/{usergroup_id}', array('as'=>'acl','uses'=>'ACLController@index'));
	Route::get('/acl/{usergroup_id}/create', array('as'=>'acl.create','uses'=>'ACLController@create'));
	Route::post('/acl/{usergroup_id}/store', array('as'=>'acl.store','uses'=>'ACLController@store'));

	// Route::get('/sample', array('as'=>'sample','uses'=>'SampleController@index'));
	// Route::get('/sample/create', array('as'=>'sample.create','uses'=>'SampleController@create'));
	// Route::post('/sample/store', array('as'=>'sample.store','uses'=>'SampleController@store'));
	// Route::get('/sample/{id}/edit', array('as'=>'sample.edit','uses'=>'SampleController@edit'));
	// Route::put('/sample/{id}/update', array('as'=>'sample.update','uses'=>'SampleController@update'));
	// Route::delete('/sample/{id}/destroy', array('as'=>'sample.destroy','uses'=>'SampleController@destroy'));
	// Route::get('/sample/{id}/shift/{shift_id}', array('as'=>'sample.shift','uses'=>'SampleController@shift'));
});

Route::group(['middleware' => ['web'],'prefix' => 'api'], function () {
    // Route::get('test/{api}', array('as'=>'api','uses'=>'APIController@index'));
	// Route::any('/login', array('as'=>'api.login','uses'=>'APIController@login'));
	// Route::any('/logout', array('as'=>'api.logout','uses'=>'APIController@logout'));
	// Route::any('/pt', array('as'=>'api.pt','uses'=>'APIController@pt'));
});
