<?php

Route::group(['middleware' => ['web']], function () {
	Route::group(['prefix' => 'champ'], function () {
		Route::get('tnc', array('as'=>'champ.tnc','uses'=>'CHAMP\CHAMP_TnCController@index'));
		Route::any('profile/active_user/{email}', array('as'=>'champ.profile.active_user','uses'=>'CHAMP\CHAMP_TnCController@active_user'));
	});
});

Route::group(['middleware' => ['web','auth']], function () {
	Route::group(['prefix' => 'champ'], function () {
		Route::get('profile', array('as'=>'champ.profile','uses'=>'CHAMP\CHAMP_ProfileController@index'));
		Route::get('profile/create', array('as'=>'champ.profile.create','uses'=>'CHAMP\CHAMP_ProfileController@create'));
		Route::post('profile/store', array('as'=>'champ.profile.store','uses'=>'CHAMP\CHAMP_ProfileController@store'));
		Route::get('profile/{id}/edit', array('as'=>'champ.profile.edit','uses'=>'CHAMP\CHAMP_ProfileController@edit'));
		Route::put('profile/{id}/update', array('as'=>'champ.profile.update','uses'=>'CHAMP\CHAMP_ProfileController@update'));
		Route::delete('profile/{id}/destroy', array('as'=>'champ.profile.destroy','uses'=>'CHAMP\CHAMP_ProfileController@destroy'));
		// Route::get('profile/{id}/shift/{shift_id}', array('as'=>'champ.profile.shift','uses'=>'CHAMP\CHAMP_ProfileController@shift'));
		


		Route::get('redemption', array('as'=>'champ.redemption','uses'=>'CHAMP\CHAMP_RedemptionController@index'));
		Route::get('redemption/create', array('as'=>'champ.redemption.create','uses'=>'CHAMP\CHAMP_RedemptionController@create'));
		Route::post('redemption/store', array('as'=>'champ.redemption.store','uses'=>'CHAMP\CHAMP_RedemptionController@store'));
		Route::get('redemption/{id}/edit', array('as'=>'champ.redemption.edit','uses'=>'CHAMP\CHAMP_RedemptionController@edit'));
		Route::put('redemption/{id}/update', array('as'=>'champ.redemption.update','uses'=>'CHAMP\CHAMP_RedemptionController@update'));
		Route::delete('redemption/{id}/destroy', array('as'=>'champ.redemption.destroy','uses'=>'CHAMP\CHAMP_RedemptionController@destroy'));
		// Route::get('redemption/{id}/shift/{shift_id}', array('as'=>'champ.redemption.shift','uses'=>'CHAMP\CHAMP_RedemptionController@shift'));

		Route::get('score', array('as'=>'champ.score','uses'=>'CHAMP\CHAMP_ScoreController@index'));
		Route::get('score/create', array('as'=>'champ.score.create','uses'=>'CHAMP\CHAMP_ScoreController@create'));
		Route::post('score/store', array('as'=>'champ.score.store','uses'=>'CHAMP\CHAMP_ScoreController@store'));
		Route::get('score/{id}/edit', array('as'=>'champ.score.edit','uses'=>'CHAMP\CHAMP_ScoreController@edit'));
		Route::put('score/{id}/update', array('as'=>'champ.score.update','uses'=>'CHAMP\CHAMP_ScoreController@update'));
		Route::delete('score/{id}/destroy', array('as'=>'champ.score.destroy','uses'=>'CHAMP\CHAMP_ScoreController@destroy'));
		// Route::get('score/{id}/shift/{shift_id}', array('as'=>'champ.score.shift','uses'=>'CHAMP\CHAMP_ScoreController@shift'));

		Route::get('voucher', array('as'=>'champ.voucher','uses'=>'CHAMP\CHAMP_VoucherController@index'));
		Route::get('voucher/create', array('as'=>'champ.voucher.create','uses'=>'CHAMP\CHAMP_VoucherController@create'));
		Route::post('voucher/store', array('as'=>'champ.voucher.store','uses'=>'CHAMP\CHAMP_VoucherController@store'));
		Route::get('voucher/{id}/edit', array('as'=>'champ.voucher.edit','uses'=>'CHAMP\CHAMP_VoucherController@edit'));
		Route::put('voucher/{id}/update', array('as'=>'champ.voucher.update','uses'=>'CHAMP\CHAMP_VoucherController@update'));
		Route::delete('voucher/{id}/destroy', array('as'=>'champ.voucher.destroy','uses'=>'CHAMP\CHAMP_VoucherController@destroy'));
		Route::get('voucher/active',array('as'=>'champ.voucher.active','uses'=>'CHAMP\CHAMP_VoucherController@active'));


		// Route::get('voucher/{id}/shift/{shift_id}', array('as'=>'champ.voucher.shift','uses'=>'CHAMP\CHAMP_VoucherController@shift'));

		Route::get('question_image/{id}', array('as'=>'champ.question_image','uses'=>'CHAMP\CHAMP_Question_ImageController@index'));

		Route::get('question_image/create/{id}', array('as'=>'champ.question_image.create','uses'=>'CHAMP\CHAMP_Question_ImageController@create'));
		Route::post('question_image/store/{id}', array('as'=>'champ.question_image.store','uses'=>'CHAMP\CHAMP_Question_ImageController@store'));
		Route::get('question_image/{id}/edit', array('as'=>'champ.question_image.edit','uses'=>'CHAMP\CHAMP_Question_ImageController@edit'));
		Route::put('question_image/{id}/update', array('as'=>'champ.question_image.update','uses'=>'CHAMP\CHAMP_Question_ImageController@update'));
		Route::delete('question_image/{id}/destroy', array('as'=>'champ.question_image.destroy','uses'=>'CHAMP\CHAMP_Question_ImageController@destroy'));
		// Route::get('voucher/{id}/shift/{shift_id}', array('as'=>'champ.voucher.shift','uses'=>'CHAMP\CHAMP_VoucherController@shift'));

		Route::get('content', array('as'=>'champ.content','uses'=>'CHAMP\CHAMP_ContentController@index'));
		Route::get('content/create', array('as'=>'champ.content.create','uses'=>'CHAMP\CHAMP_ContentController@create'));
		Route::post('content/store', array('as'=>'champ.content.store','uses'=>'CHAMP\CHAMP_ContentController@store'));
		Route::get('content/{id}/edit', array('as'=>'champ.content.edit','uses'=>'CHAMP\CHAMP_ContentController@edit'));
		Route::put('content/{id}/update', array('as'=>'champ.content.update','uses'=>'CHAMP\CHAMP_ContentController@update'));
		Route::delete('content/{id}/destroy', array('as'=>'champ.content.destroy','uses'=>'CHAMP\CHAMP_ContentController@destroy'));
		// Route::get('content/{id}/shift/{shift_id}', array('as'=>'champ.content.shift','uses'=>'CHAMP\CHAMP_ProfileController@shift'));


	});
});

Route::group(['middleware' => ['web'],'prefix' => 'api'], function () {
	Route::group(['prefix' => 'champ'], function () {
		Route::get('test/{api?}', array('as'=>'champ.api','uses'=>'CHAMP\CHAMP_APIController@index'));
		// Route::any('login', array('as'=>'champ.api.login','uses'=>'CHAMP\CHAMP_APIController@login'));
		Route::any('logout', array('as'=>'champ.api.logout','uses'=>'CHAMP\CHAMP_APIController@logout'));
		Route::any('pt', array('as'=>'champ.api.pt','uses'=>'CHAMP\CHAMP_APIController@pt'));

		Route::any('register', array('as'=>'champ.api.register','uses'=>'CHAMP\CHAMP_APIController@register'));
		Route::any('email_login', array('as'=>'champ.api.email_login','uses'=>'CHAMP\CHAMP_APIController@email_login'));
		Route::any('fb_login', array('as'=>'champ.api.fb_login','uses'=>'CHAMP\CHAMP_APIController@fb_login'));
		Route::any('guest_login', array('as'=>'champ.api.guest_login','uses'=>'CHAMP\CHAMP_APIController@guest_login'));
		Route::any('get_profile', array('as'=>'champ.api.get_profile','uses'=>'CHAMP\CHAMP_APIController@get_profile'));
		Route::any('update_profile_fb', array('as'=>'champ.api.update_profile_fb','uses'=>'CHAMP\CHAMP_APIController@update_profile_fb'));
		// Route::any('update_profile_email', array('as'=>'champ.api.update_profile_email','uses'=>'CHAMP\CHAMP_APIController@update_profile_email'));
		Route::any('submit_score', array('as'=>'champ.api.submit_score','uses'=>'CHAMP\CHAMP_APIController@submit_score'));
		Route::any('submit_all_scores', array('as'=>'champ.api.submit_all_scores','uses'=>'CHAMP\CHAMP_APIController@submit_all_scores'));
		Route::any('get_voucher_list', array('as'=>'champ.api.get_voucher_list','uses'=>'CHAMP\CHAMP_APIController@get_voucher_list'));
		Route::any('get_redeemable_vouchers', array('as'=>'champ.api.get_redeemable_vouchers','uses'=>'CHAMP\CHAMP_APIController@get_redeemable_vouchers'));
		Route::any('redeem_voucher', array('as'=>'champ.api.redeem_voucher','uses'=>'CHAMP\CHAMP_APIController@redeem_voucher'));
		Route::any('get_content', array('as'=>'champ.api.get_content','uses'=>'CHAMP\CHAMP_APIController@get_content'));
		// Route::any('get_ambition', array('as'=>'champ.api.get_ambition','uses'=>'CHAMP\CHAMP_APIController@get_ambition'));
		Route::any('active_user', array('as'=>'champ.api.active_user','uses'=>'CHAMP\CHAMP_APIController@active_user'));
		Route::any('question_image', array('as'=>'champ.api.question_image','uses'=>'CHAMP\CHAMP_APIController@question_image'));
		Route::any('my_vouchers', array('as'=>'champ.api.my_vouchers','uses'=>'CHAMP\CHAMP_APIController@my_vouchers'));
		Route::any('forget_password', array('as'=>'champ.api.forget_password','uses'=>'CHAMP\CHAMP_APIController@forget_password'));
		Route::any('change_password', array('as'=>'champ.api.change_password','uses'=>'CHAMP\CHAMP_APIController@change_password'));
	});
});
