<?php

namespace BUP\Http\Requests\CHAMP;

use BUP\Http\Requests\Request;

class CHAMP_FBRegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fb_id' => 'required|unique:CHAMP_profiles,fb_id',
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
        ];
    }
}
