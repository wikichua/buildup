<?php

namespace BUP\Http\Requests\CHAMP;

use BUP\Http\Requests\Request;

class CHAMP_GuestRegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:users,name,NULL,id,usergroup_id,3',
        ];
    }
}
