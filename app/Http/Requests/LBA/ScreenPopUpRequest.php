<?php

namespace BUP\Http\Requests\LBA;

use BUP\Http\Requests\Request;

class ScreenPopUpRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url' => 'url',
            'published_at' => 'required|date|after:yesterday',
            'expired_at'   => 'required|date|after:published_at',
        ];
    }
}
