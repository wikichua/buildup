<?php

namespace BUP\Http\Requests\LBA;

use BUP\Http\Requests\Request;
use Auth;

class StandardProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            // 'type_system'=>'required',
            'thickness'=>'required',
            // 'glass_thickness'=>'required',
        ];
    }
}
