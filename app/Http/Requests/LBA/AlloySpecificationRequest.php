<?php

namespace BUP\Http\Requests\LBA;

use BUP\Http\Requests\Request;
use Auth;

class AlloySpecificationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'source' => 'required|mimes:zip|max:2048'
        ];
    }
}
