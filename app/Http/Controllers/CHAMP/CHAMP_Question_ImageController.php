<?php

namespace BUP\Http\Controllers\CHAMP;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
// use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\CHAMP\CHAMP_Question_Image;

class CHAMP_Question_ImageController extends Controller
{
    // use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($Content_id)
    {
        // $this->reorganizeSEQ(new CHAMP_Question_Image);
     	$Question_images = CHAMP_Question_Image::where('content_id',$Content_id)->search()->sort()->orderBy('created_at','desc')->paginate(25);
        // $this->index_shift($Vouchers, new CHAMP_Question_Image);
        $image_for=array("Image"=>"Queastion Image","ImageDYK"=>"DYK Image");
        // dd($image_for);
        return view('CHAMP.question_image.index')->with(compact('Question_images','Content_id','image_for'));
    }

    public function create($Content_id)
    {
        return view('CHAMP.question_image.create')->with(compact('Content_id'));
    }

    public function store(Request $request,$Content_id)
    {
        $Voucher = CHAMP_Question_Image::create(
                array(
                        'content_id' => $Content_id,
                        'image' => uploadImage('image','','champ-question-image'),
                        'name' => $request->get('name'),
                        'image_for' => $request->get('image_for'),
                    )
            );

        return redirect()->route('champ.question_image',$Content_id)->with('success','Record created.');
    }

    public function edit($id)
    {
        $Question_image = CHAMP_Question_Image::find($id);
        return view('CHAMP.question_image.edit')->with(compact('Question_image'));
    }

    public function update(Request $request, $id)
    {
        $Question_image = CHAMP_Question_Image::find($id);
        $Question_image->image = uploadImage('image',$Question_image->image,'champ-question-image');
        $Question_image->name = trim($request->get('name',$Question_image->name),' ');
        $Question_image->image_for = $request->get('image_for',$Question_image->image_for);

        $Question_image->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return CHAMP_Question_Image::destroy($id);
    }

    // public function shift($id, $shift_id)
    // {
    //     $this->shifting(new CHAMP_Question_Image, $id, $shift_id);

    //     return back();
    // }
}
