<?php

namespace BUP\Http\Controllers\CHAMP;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
// use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\CHAMP\CHAMP_Score;

class CHAMP_ScoreController extends Controller
{
    // use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $this->reorganizeSEQ(new CHAMP_Score);
     	$Scores = CHAMP_Score::search()->sort()->orderBy('updated_at','desc')->paginate(25);
        // $this->index_shift($Scores, new CHAMP_Score);
        return view('CHAMP.score.index')->with(compact('Scores'));
    }

    public function create()
    {
        return view('CHAMP.score.create');
    }

    public function store(Request $request)
    {
        $Score = CHAMP_Score::create(
                array(
                        'user_id' => $request->get('user_id'),
                        'level' => $request->get('level'),
                        'best_score' => $request->get('best_score'),
                        'total_duration' => $request->get('total_duration'),
                        'character_id' => $request->get('character_id'),
                        'questions_answered' => $request->get('questions_answered'),
                    )
            );

        return redirect()->route('champ.score')->with('success','Record created.');
    }

    public function edit($id)
    {
        $Score = CHAMP_Score::find($id);
        return view('CHAMP.score.edit')->with(compact('Score'));
    }

    public function update(Request $request, $id)
    {
        $Score = CHAMP_Score::find($id);
        $Score->user_id = $request->get('user_id',$Score->user_id);
        $Score->level = $request->get('level',$Score->level);
        $Score->best_score = $request->get('best_score',$Score->best_score);
        $Score->total_duration = $request->get('total_duration',$Score->total_duration);
        $Score->character_id = $request->get('character_id',$Score->character_id);
        $Score->questions_answered = $request->get('questions_answered',$Score->questions_answered);
        $Score->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return CHAMP_Score::destroy($id);
    }

    // public function shift($id, $shift_id)
    // {
    //     $this->shifting(new CHAMP_Score, $id, $shift_id);

    //     return back();
    // }
}
