<?php

namespace BUP\Http\Controllers\CHAMP;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
// use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\CHAMP\CHAMP_Profile;
use BUP\User;

class CHAMP_ProfileController extends Controller
{
    // use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }



    public function index()
    {
        // $this->reorganizeSEQ(new CHAMP_Profile);
     	$Profiles = CHAMP_Profile::search()->sort()->orderBy('created_at','desc')->paginate(25);
        // $this->index_shift($Profiles, new CHAMP_Profile);
        return view('CHAMP.profile.index')->with(compact('Profiles'));
    }

    public function create()
    {
        return view('CHAMP.profile.create');
    }

    public function store(Request $request)
    {
        $Profile = CHAMP_Profile::create(
                array(
                        'user_id' => $request->get('user_id'),
                        'fb_id' => $request->get('fb_id'),
                    )
            );

        return redirect()->route('champ.profile')->with('success','Record created.');
    }

    public function edit($id)
    {
        $Profile = CHAMP_Profile::find($id);
        return view('CHAMP.profile.edit')->with(compact('Profile'));
    }

    public function update(Request $request, $id)
    {
        $Profile = CHAMP_Profile::find($id);
        $Profile->user_id = $request->get('user_id',$Profile->user_id);
        $Profile->fb_id = $request->get('fb_id',$Profile->fb_id);
        $Profile->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return CHAMP_Profile::destroy($id);
    }

    // public function shift($id, $shift_id)
    // {
    //     $this->shifting(new CHAMP_Profile, $id, $shift_id);

    //     return back();
    // }
}
