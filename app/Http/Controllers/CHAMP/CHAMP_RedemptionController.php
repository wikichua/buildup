<?php

namespace BUP\Http\Controllers\CHAMP;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
// use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\CHAMP\CHAMP_Redemption;

class CHAMP_RedemptionController extends Controller
{
    // use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $this->reorganizeSEQ(new CHAMP_Redemption);
     	$Redemptions = CHAMP_Redemption::search()->sort()->orderBy('created_at','desc')->paginate(25);
        // $this->index_shift($Redemptions, new CHAMP_Redemption);
        return view('CHAMP.redemption.index')->with(compact('Redemptions'));
    }

    public function create()
    {
        return view('CHAMP.redemption.create');
    }

    public function store(Request $request)
    {
        $Redemption = CHAMP_Redemption::create(
                array(
                        'voucher_id' => $request->get('voucher_id'),
                        'user_id' => $request->get('user_id'),
                    )
            );

        return redirect()->route('champ.redemption')->with('success','Record created.');
    }

    public function edit($id)
    {
        $Redemption = CHAMP_Redemption::find($id);
        return view('CHAMP.redemption.edit')->with(compact('Redemption'));
    }

    public function update(Request $request, $id)
    {
        $Redemption = CHAMP_Redemption::find($id);
        $Redemption->voucher_id = $request->get('voucher_id',$Redemption->voucher_id);
        $Redemption->user_id = $request->get('user_id',$Redemption->user_id);
        $Redemption->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return CHAMP_Redemption::destroy($id);
    }

    // public function shift($id, $shift_id)
    // {
    //     $this->shifting(new CHAMP_Redemption, $id, $shift_id);

    //     return back();
    // }
}
