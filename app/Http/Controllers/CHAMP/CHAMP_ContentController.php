<?php

namespace BUP\Http\Controllers\CHAMP;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
// use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\CHAMP\CHAMP_Content;

class CHAMP_ContentController extends Controller
{
    // use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $this->reorganizeSEQ(new CHAMP_Content);
     	$Contents = CHAMP_Content::search()->sort()->orderBy('created_at','desc')->paginate(25);
        // $this->index_shift($Contents, new CHAMP_Content);
        return view('CHAMP.content.index')->with(compact('Contents'));
    }

    public function create()
    {
        return view('CHAMP.content.create');
    }

    public function store(Request $request)
    {
        $Content = CHAMP_Content::create(
                array(
                        'file' =>uploadFile('file','','champ-content-image') ,
                        'character'=>$request->get('character'),

                        'level'=>$request->get('level'),
                        'language'=>$request->get('language'),
                    )
            );

        return redirect()->route('champ.content')->with('success','Record created.');
    }

    public function edit($id)
    {
        $Content = CHAMP_Content::find($id);
        return view('CHAMP.content.edit')->with(compact('Content'));
    }

    public function update(Request $request, $id)
    {
        $Content = CHAMP_Content::find($id);
        $Content->file = uploadFile('file', $Content->file ,'champ-content-image') ;
        $Content->character = $request->get('character');
        $Content->level = $request->get('level',$Content->character);
        $Content->language = $request->get('language',$Content->language);


        $Content->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return CHAMP_Content::destroy($id);
    }

    // public function shift($id, $shift_id)
    // {
    //     $this->shifting(new CHAMP_Content, $id, $shift_id);

    //     return back();
    // }
}
