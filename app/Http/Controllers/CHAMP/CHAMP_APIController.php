<?php

namespace BUP\Http\Controllers\CHAMP;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Log;

use BUP\Http\Requests;
use BUP\Http\Controllers\APIController;
use Validator, Response, Image;
use Carbon\Carbon;
use Mail;
use BUP\User;
use BUP\Http\Controllers\CHAMP\URL;
use BUP\UserDevice;
use BUP\Models\CHAMP\CHAMP_Profile;
use BUP\Models\CHAMP\CHAMP_Redemption;
use BUP\Models\CHAMP\CHAMP_Score;
use BUP\Models\CHAMP\CHAMP_Voucher;
use BUP\Models\CHAMP\CHAMP_Question_Image;
use BUP\Models\CHAMP\CHAMP_Content;
use BUP\Models\CHAMP\CHAMP_Setting;
use BUP\Models\CHAMP\CHAMP_UseInfo;

class CHAMP_APIController extends APIController
{
    public function index($api = 'login')
    {
        $app = 'champ';
        $sample = $file_keys = $links = array();
        // $links[] = route($app.'.api',array('login'));
        $links[] = route($app.'.api',array('register'));
        $links[] = route($app.'.api',array('email_login'));
        $links[] = route($app.'.api',array('fb_login'));
        $links[] = route($app.'.api',array('guest_login'));
        $links[] = route($app.'.api',array('logout'));
        $links[] = route($app.'.api',array('pt'));

        $links[] = route($app.'.api',array('get_profile'));
        $links[] = route($app.'.api',array('update_profile_fb'));
        // $links[] = route($app.'.api',array('update_profile_email'));
        $links[] = route($app.'.api',array('submit_score'));
        $links[] = route($app.'.api',array('submit_all_scores'));
        $links[] = route($app.'.api',array('get_voucher_list'));
        $links[] = route($app.'.api',array('get_redeemable_vouchers'));
        $links[] = route($app.'.api',array('redeem_voucher'));
        $links[] = route($app.'.api',array('get_content'));
        $links[] = route($app.'.api',array('active_user'));
        $links[] = route($app.'.api',array('question_image'));
        $links[] = route($app.'.api',array('my_vouchers'));
        $links[] = route($app.'.api',array('forget_password'));
        $links[] = route($app.'.api',array('change_password'));
        // $links[] = route($app.'.api',array('get_ambition'));

        $api_token = session()->get('api_token','');

        switch (strtolower($api)) {

        	case 'register':
                $sample = array(
                        'name' => 'CHAMP API Tester',
                        'email' => 'tester@convep.com',
                        'password' => 'ABC123abc',
                        'password_confirmation' => 'ABC123abc',
                    );
                break;
            case 'email_login':
                $sample = array(
                        'app' => strtoupper($app),
                        'email' => 'tester@convep.com',
                        'password' => 'ABC123abc',
                        'imei' => 'IOS:123',
                        'push_token' => 'push_token123',
                        'width' => '100',
                        'height' => '100',
                        'os_version' => '1.0',
                        'app_version' => '1.0',
                    );
                break;
            case 'fb_login':
                $sample = array(
                        'app' => strtoupper($app),
                        'fb_id' => '',
                        'name' => '',
                        'email' => 'admin@convep.com',
                        'imei' => 'IOS:123',
                        'push_token' => 'push_token123',
                        'width' => '100',
                        'height' => '100',
                        'os_version' => '1.0',
                        'app_version' => '1.0',
                    );
                break;
            case 'guest_login':
                $sample = array(
                        'app' => strtoupper($app),
                        'name' => 'CHAMP Guest Tester',
                        'imei' => 'IOS:123',
                        'push_token' => 'push_token123',
                        'width' => '100',
                        'height' => '100',
                        'os_version' => '1.0',
                        'app_version' => '1.0',
                    );
                break;
            case 'logout':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                    );
                break;
            case 'pt':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'imei' => 'IOS:123',
                        'push_token' => 'push_token123',
                        'width' => '100',
                        'height' => '100',
                        'os_version' => '1.0',
                        'app_version' => '1.0',
                    );
                break;
            case 'get_profile':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                    );
                break;
            case 'update_profile_fb':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'name' => '',
                        'email' => '',
                        'fb_id' => '',
                    );
                break;
            // case 'update_profile_email':
            //     $sample = array(
            //             'app' => strtoupper($app),
            //             'api_token' => $api_token,
            //             'name' => '',
            //             'email' => '',
            //             'password' => '',
            //             'password_confirmation' => '',
            //         );
            //     break;
            case 'submit_score':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'level' => '1',
                        'best_score' => 100,
                        'total_duration' => 500,
                        'character_id' => 1,
                        'questions_answered' => array(1,2,1,2,2),
                    );
                break;
            case 'submit_all_scores':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'scores' => array(
                                array(
                                        'level' => '1',
                                        'best_score' => 100,
                                        'total_duration' => 200,
                                        'character_id' => 1,
                                        'questions_answered' => array(1,2,1,2,2),
                                    ),
                                array(
                                        'level' => '2',
                                        'best_score' => 200,
                                        'total_duration' => 500,
                                        'character_id' => 1,
                                        'questions_answered' => array(1,2,1,2,2),
                                    ),
                                array(
                                        'level' => '3',
                                        'best_score' => 300,
                                        'total_duration' => 300,
                                        'character_id' => 1,
                                        'questions_answered' => array(1,2,1,2,2),
                                    ),
                                array(
                                        'level' => '4',
                                        'best_score' => 400,
                                        'total_duration' => 200,
                                        'character_id' => 1,
                                        'questions_answered' => array(1,2,1,2,2),
                                    ),
                                array(
                                        'level' => '5',
                                        'best_score' => 200,
                                        'total_duration' => 300,
                                        'character_id' => 1,
                                        'questions_answered' => array(1,2,1,2,2),
                                    ),
                            ),
                    );
                break;
            case 'get_voucher_list':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                    );
                break;
            case 'get_redeemable_vouchers':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                    );
                break;
            case 'redeem_voucher':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'level' => 1,
                        'character' => 1,
                    );
                break;
            case 'get_content':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'last_updated_at' => date('Y-m-d H:i:s'),
                        'language' =>"English"

                    );
                break;
            
            case 'active_user':
                $sample = array(
                        'app' => strtoupper($app),
                        'email' => '',
                        
                    );
                break;
            case 'question_image':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'name' => '',
                        
                    );
                break;
            case 'my_vouchers':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        
                        
                    );
                break;
            case 'forget_password':
                $sample = array(
                        'app' => strtoupper($app),
                        'email' => '',
                        
                    );
                break;
            case 'change_password':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'password' => '',
                        'password_confirmation' => '',
                        
                    );
                break;
                
            // case 'get_ambition':
            //     $sample = array(
            //             'app' => strtoupper($app),
            //             'api_token' => $api_token,
            //         );
            //     break;
        }
        return view('api.index')->with(compact('api','sample','file_keys','links','app'));
    }

    public function forget_password()
    {
        $status= true;
        
        if ($status === true) {
            // $AuthController = new \BUP\Http\Controllers\AuthController;

            $status = array();

            if($User=User::where('email',$this->Request['email'])->where('app','CHAMP')->first()){

                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';    
                $randstring = '';    
                for ($i = 0; $i < 10; $i++) {        
                    $randstring = $randstring."".$characters[rand(0, strlen($characters))];    
                }

                $dates=['username'=> $User->name,'link'=>route('champ.api.change_password'),'password'=>$randstring];

                Mail::send('CHAMP.Email.forget_password', $dates, function ($message) use($User) {
                $message->from('support@convep.com');
                $message->to($User->email, $User->name)->subject('Welcom to MyChampion');
            });
        }
    }
        return $this->response(__FUNCTION__,$status,$datas=$User);
    }

    public function change_password()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);

        if($this->Request['password']==$this->Request['password_confirmation']){

            $User->password= bcrypt($this->Request['password']);
            $User->save();
        }

        return $this->response(__FUNCTION__,$status,$datas=$User);
    }

    public function active_user()
    {
        $status= true;
        

        if ($status === true) {
            // $AuthController = new \BUP\Http\Controllers\AuthController;
           $User=User::where('email',$this->Request['email'])->first();
            $User->status="Active";
            $User->save();
            $status = array();
            $datas = $User;
        }

        return $this->response(__FUNCTION__,$status,$datas);
    
    }

    public function register()
    {
        $status = $this->validation(new \BUP\Http\Requests\CHAMP\CHAMP_RegisterRequest);
        if ($status === true) {
            // $AuthController = new \BUP\Http\Controllers\AuthController;
            $User = User::create(
                array(
                        'name' => $this->Request['name'],
                        'email' => $this->Request['email'],
                        'password' => bcrypt($this->Request['password']),
                        'isAdmin' => false,
                        'status' => 'Unactive',
                        'photo' => uploadImage('photo','','profile-photo'),
                        'usergroup_id' => 3,
                        'app' => 'CHAMP',
                    )
            );
            $User_info = CHAMP_UseInfo::create(
                array(
                    'user_id'=> $User->id,
                    'name'=> $this->Request['name'],
                    'parent_guardian'=> isset($this->Request['parent_name'])?$this->Request['parent_name']:"",
                    'school'=>isset($this->Request['school_name'])?$this->Request['school_name']:"",
                    'pdpa'=>isset($this->Request['pdpa'])?$this->Request['pdpa']:"",
                    )
            );

            $Profile = CHAMP_Profile::create(
            	array(
            			'user_id' => $User->id,
            			'fb_id' => '',
            		)
            );
            Mail::send('CHAMP.Email.index1', ['active_link'=>route("champ.profile.active_user",$User->email),'password'=>$User,'name'=>trim($this->Request['name'])], function ($message) use($User) {
            $message->from('support@convep.com');
            $message->to($User->email, $User->name)->subject('Welcom to MyChampion');
        });

            // $AuthController->sendMailNewUser($User);
            $status = array();
            
            $datas = $User;
        }

        return $this->response(__FUNCTION__,$status);
    }

    public function email_login()
    {
        $status = $this->validation(new \BUP\Http\Requests\AuthRequest);
        $datas = array();
        if ($status === true) {
            if (auth()->once(['email' => $this->Request['email'],'password' => $this->Request['password'], 'status' => 'Active','app' => $this->Request['app']]))
            {
                $scores=array();
                $User = User::where('email',$this->Request['email'])->where('usergroup_id',3)->where('status','Active')->first();
                $Score = CHAMP_Score::where('user_id',$User->id)->get();
                $user_id = $User->id;
                $User->save();
                $User->api_token = uuid();

                for($i=1;$i<6;$i++){
                    for($j=1;$j<6;$j++){
                    $scores['character'.$i.'_level'.$j]='';
                    }
                }


                foreach ($Score as $key => $value) {

                  $scores["character".$value->character_id."_level".$value->level]= $value->best_score;
                }
                $User["score"]=$scores;
                $datas = $User;
                // $datas["Score"] = $Score;
                $status = array();

                session()->put('api_token',$User->api_token);

                $os = explode(':', $this->Request['imei'])[0];
                $imei = explode(':', $this->Request['imei'])[1];
                $UserDevice = UserDevice::firstOrCreate(array(
                    'user_id' => $user_id,
                    'imei' => $imei,
                    'os' => $os,
                    'width' => trim($this->Request['width']),
                    'height' => trim($this->Request['height']),
                    'os_version' => trim($this->Request['os_version']),
                    'app_version' => trim($this->Request['app_version']),
                    'app' => trim($this->Request['app']),
                ));
                $UserDevice->push_token = trim($this->Request['push_token']);
                $UserDevice->api_token = trim($User->api_token);
                $UserDevice->save();
            }else{
                $status = array('status' => 'failed','message' => array('Invalid login credential.'));
            }
        }

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function fb_login()
    {
        $status = $this->validation(new \BUP\Http\Requests\CHAMP\CHAMP_FBRegisterRequest);
        $datas = array();
        $User=array();
        $Score=array();

        if ($status === true) {
        	$User = User::create(
                array(
                        'name' => $this->Request['name'],
                        'email' => $this->Request['email'],
                        'password' => bcrypt('ABC123abc'),
                        'isAdmin' => false,
                        'status' => 'Active',
                        'photo' => uploadImage('photo','','profile-photo'),
                        'usergroup_id' => 3,
                        'app' => 'CHAMP',
                    )
            );

            $Profile = CHAMP_Profile::create(
            	array(
            			'user_id' => $User->id,
            			'fb_id' => $this->Request['fb_id'],
            		)
            );

            $user_id = $User->id;
            $User->save();
        $Score = CHAMP_Score::where('user_id',$User->id)->get();

            $User->api_token = uuid();
            $datas = $User;
            $status = array();

            session()->put('api_token',$User->api_token);

            $os = explode(':', $this->Request['imei'])[0];
            $imei = explode(':', $this->Request['imei'])[1];
            $UserDevice = UserDevice::firstOrCreate(array(
                'user_id' => $user_id,
                'imei' => $imei,
                'os' => $os,
                'width' => trim($this->Request['width']),
                'height' => trim($this->Request['height']),
                'os_version' => trim($this->Request['os_version']),
                'app_version' => trim($this->Request['app_version']),
                'app' => trim($this->Request['app']),
            ));
            $UserDevice->push_token = trim($this->Request['push_token']);
            $UserDevice->api_token = trim($User->api_token);
            $UserDevice->save();
        } else {
        	if (auth()->once(['email' => $this->Request['email'], 'password' => 'ABC123abc', 'usergroup_id' => 3, 'status' => 'Active', 'app' => $this->Request['app']]))
            {
                $User = User::where('email',$this->Request['email'])->where('usergroup_id',3)->where('status','Active')->first();
                $user_id = $User->id;
                $User->save();
                $User->api_token = uuid();
                $datas = $User;
                $Score = CHAMP_Score::where('user_id',$User->id)->get();

                $status = array();

                session()->put('api_token',$User->api_token);

                $os = explode(':', $this->Request['imei'])[0];
                $imei = explode(':', $this->Request['imei'])[1];
                $UserDevice = UserDevice::firstOrCreate(array(
                    'user_id' => $user_id,
                    'imei' => $imei,
                    'os' => $os,
                    'width' => trim($this->Request['width']),
                    'height' => trim($this->Request['height']),
                    'os_version' => trim($this->Request['os_version']),
                    'app_version' => trim($this->Request['app_version']),
                    'app' => trim($this->Request['app']),
                ));
                $UserDevice->push_token = trim($this->Request['push_token']);
                $UserDevice->api_token = trim($User->api_token);
                $UserDevice->save();
            } else {
                $status = array('status' => 'failed','message' => array('Invalid login credential.'));
            }
        }

        for($i=1;$i<6;$i++){
                    for($j=1;$j<6;$j++){
                    $scores['character'.$i.'_level'.$j]='';
                    }
                }


                foreach ($Score as $key => $value) {

                  $scores["character".$value->character_id."_level".$value->level]= $value->best_score;
                }
                $User["score"]=$scores;
                $datas = $User;

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function guest_login()
    {
        $status = $this->validation(new \BUP\Http\Requests\CHAMP\CHAMP_GuestRegisterRequest);
        $datas = array();
        if ($status === true) {
        	$User = User::create(
                array(
                        'name' => $this->Request['name'],
                        'email' => 'guest_'.time().mt_rand(1,100).'@convep.com',
                        'password' => bcrypt('ABC123abc'),
                        'isAdmin' => false,
                        'status' => 'Active',
                        'photo' => uploadImage('photo','','profile-photo'),
                        'usergroup_id' => 3,
                        'app' => 'CHAMP',
                    )
            );

            $Profile = CHAMP_Profile::create(
            	array(
            			'user_id' => $User->id,
            			'fb_id' => '',
            		)
            );

            $user_id = $User->id;
            $User->save();
            $User->api_token = uuid();
            $datas = $User;
            $status = array();

            session()->put('api_token',$User->api_token);

            $os = explode(':', $this->Request['imei'])[0];
            $imei = explode(':', $this->Request['imei'])[1];
            $UserDevice = UserDevice::firstOrCreate(array(
                'user_id' => $user_id,
                'imei' => $imei,
                'os' => $os,
                'width' => trim($this->Request['width']),
                'height' => trim($this->Request['height']),
                'os_version' => trim($this->Request['os_version']),
                'app_version' => trim($this->Request['app_version']),
                'app' => trim($this->Request['app']),
            ));
            $UserDevice->push_token = trim($this->Request['push_token']);
            $UserDevice->api_token = trim($User->api_token);
            $UserDevice->save();
        } else {
        	if (auth()->once(['name' => $this->Request['name'], 'password' => 'ABC123abc', 'usergroup_id' => 3, 'status' => 'Active', 'app' => $this->Request['app']]))
            {
                $User = User::where('name',$this->Request['name'])->where('usergroup_id',3)->where('status','Active')->first();
                $user_id = $User->id;
                $User->save();
                $User->api_token = uuid();
                $datas = $User;
                $status = array();

                session()->put('api_token',$User->api_token);

                $os = explode(':', $this->Request['imei'])[0];
                $imei = explode(':', $this->Request['imei'])[1];
                $UserDevice = UserDevice::firstOrCreate(array(
                    'user_id' => $user_id,
                    'imei' => $imei,
                    'os' => $os,
                    'width' => trim($this->Request['width']),
                    'height' => trim($this->Request['height']),
                    'os_version' => trim($this->Request['os_version']),
                    'app_version' => trim($this->Request['app_version']),
                    'app' => trim($this->Request['app']),
                ));
                $UserDevice->push_token = trim($this->Request['push_token']);
                $UserDevice->api_token = trim($User->api_token);
                $UserDevice->save();
            } else {
                Log::info($this->Request);
                $status = array('status' => 'failed','message' => array('Invalid login credential.'));
            }
        }
        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function get_profile()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);

        $Scores = CHAMP_Score::where('user_id', $User->id)->get();
        $Redemptions = CHAMP_Redemption::where('user_id',$User->id)->get();

        if ($Scores)
        {
            foreach ($Scores as $Score)
            {
                $Score->questions_answered = json_decode($Score->questions_answered)? json_decode($Score->questions_answered,true):array();
            }
        }

        if ($Redemptions)
        {
            foreach ($Redemptions as $Redemption)
            {
            	$Redemption->voucher = CHAMP_Voucher::find($Redemption->voucher_id);
            }
        }

        $User->scores = $Scores;
        $User->redemptions = $Redemptions;

        $datas = $User;

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function update_profile_fb()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);

        $UserDB = User::find($User->id);
        $Profile = CHAMP_Profile::where('user_id',$User->id)->first();

        // $status = $this->validation(new \BUP\Http\Requests\CHAMP\CHAMP_FBRegisterRequest);
		// if ($status === true) {
			$Profile->fb_id = trim($this->Request['fb_id']);
			$Profile->save();
			$UserDB->name = trim($this->Request['name']);
			$UserDB->email = trim($this->Request['email']);
			$UserDB->save();
			$User = User::find($User->id);
			$User->fb_id = $Profile->fb_id;

            $Scores = CHAMP_Score::where('user_id', $User->id)->get();
            $Redemptions = CHAMP_Redemption::where('user_id',$User->id)->get();

            if ($Scores)
            {
                foreach ($Scores as $Score)
                {
                    $Score->questions_answered = json_decode($Score->questions_answered)? json_decode($Score->questions_answered,true):array();
                }
            }

            if ($Redemptions)
            {
                foreach ($Redemptions as $Redemption)
                {
                    $Redemption->voucher = CHAMP_Voucher::find($Redemption->voucher_id);
                }
            }

            $User->scores = $Scores;
            $User->redemptions = $Redemptions;
            
        	$datas = $User;
		// } else {
		// 	$status = array('status' => 'failed','message' => array('Invalid inputs.'));
		// }

        return $this->response(__FUNCTION__,$status,$datas);
    }

  //   public function update_profile_email()
  //   {
  //       $datas = array();
  //       $status = array();
  //       $User = $this->getUserIDByApiToken($this->Request['api_token']);

  //       $UserDB = User::find($User->id);
  //       $Profile = CHAMP_Profile::where('user_id',$User->id)->first();

		// $status = $this->validation(new \BUP\Http\Requests\CHAMP\CHAMP_RegisterRequest);
		// if ($status === true) {
		// 	$UserDB->name = trim($this->Request['name']);
		// 	$UserDB->email = trim($this->Request['email']);
		// 	$UserDB->password = bcrypt($this->Request['password']);
		// 	$UserDB->save();
		// 	$User = User::find($User->id);
  //       	$datas = $User;
		// } else {
		// 	$status = array('status' => 'failed','message' => array('Invalid inputs.'));
		// }

  //       return $this->response(__FUNCTION__,$status,$datas);
  //   }

    public function submit_score()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);

        if ( $Score = CHAMP_Score::where('user_id',$User->id)->where('character_id',$this->Request['character_id'])->where('level',$this->Request['level'])->first() )
        {
        	$Score->best_score = trim($this->Request['best_score']);
        	$Score->total_duration = trim($this->Request['total_duration']);
        	$Score->questions_answered = json_encode($this->Request['questions_answered'])? json_encode($this->Request['questions_answered'],true):array();
        	$Score->save();
    	} else {
	        $Score = CHAMP_Score::Create(array(
	                'user_id' => $User->id,
	                'level' => trim($this->Request['level']),
	                'best_score' => trim($this->Request['best_score']),
                    'total_duration' => trim($this->Request['total_duration']),
	                'character_id' => trim($this->Request['character_id']),
	                'questions_answered' => json_encode($this->Request['questions_answered'])? json_encode($this->Request['questions_answered'],true):array(),
	            ));
    	}

        return $this->response(__FUNCTION__,$status,$datas);
    }

   

    public function submit_all_scores()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);

        $reqScores = $this->Request['scores'];

        foreach ($reqScores as $reqScore) {
            if ( $Score = CHAMP_Score::where('user_id',$User->id)->where('character_id',$reqScore['character_id'])->where('level',$reqScore['level'])->first() )
            {
                if ($Score->best_score < $reqScore['best_score'])
                {
                    $Score->best_score = trim($reqScore['best_score']);
                    $Score->total_duration = trim($reqScore['total_duration']);
                    $Score->questions_answered = json_encode($reqScore['questions_answered'])? json_encode($reqScore['questions_answered'],true):array();
                    $Score->save();
                }
            } else {
                $Score = CHAMP_Score::Create(array(
                        'user_id' => $User->id,
                        'level' => trim($reqScore['level']),
                        'best_score' => trim($reqScore['best_score']),
                        'total_duration' => trim($reqScore['total_duration']),
                        'character_id' => trim($reqScore['character_id']),
                        'questions_answered' => json_encode($reqScore['questions_answered'])? json_encode($reqScore['questions_answered'],true):array(),
                    ));
            }
        }

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function get_voucher_list()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);
            $Vouchers = CHAMP_Voucher::where('issued_to_user_id',$User->id)->get();


        $datas = $Vouchers;

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function get_redeemable_vouchers()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);

        $checkOne = strpos($User->email, 'guest_');
        $checkTwo = strpos($User->email, '@convep.com');

        if($checkOne === false || $checkTwo === false) {

            $Vouchers = CHAMP_Voucher::where('issued_to_user_id','')->get();
            $Redemptions = CHAMP_Redemption::where('user_id', $User->id)->get();
            $Scores = CHAMP_Score::where('user_id', $User->id)->get();
            $Redeemables = array();

            foreach ($Vouchers as $Voucher) 
            {
    		    foreach ($Scores as $Score)
    		    {
    		       	if ($Voucher->level == $Score->level)
    		       	{
    		       		$voucherStatus = 0;
    		        	foreach ($Redemptions as $Redemption)
    		        	{
    		        		if ($Voucher->id == $Redemption->voucher_id)
    		        		{
    		        			$voucherStatus = 1;
    		        		}
    		        	}
    	        		if ($voucherStatus == 0)
    	        		{
        					$Redeemables[] = $Voucher;
    	        		}
            		}
            	}
            }

            $datas = $Redeemables;

        } else {
            $status = array('status' => 'failed','message' => array('Guest will not be able to redeem vouchers. Connect with Facebook or email to enjoy these benefits.'));
        }

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function redeem_voucher()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);
        $ram=array();
        $checkOne = strpos($User->email, 'guest_');
        $checkTwo = strpos($User->email, '@convep.com');
        $Voucher_actived = CHAMP_Setting::all()->first();
        if($Voucher_actived->voucher_actived=="YES")
        {
    		if($checkOne === false||$checkTwo === false) {
    	        if ( !(CHAMP_Voucher::where('issued_to_user_id',$User->id)->where('level',$this->Request['level'])->where('character',$this->Request['character'])->first()) )
    	        {
                    $all_Voucher = CHAMP_Voucher::where('issued_to_user_id','')->get();
                    if(count($all_Voucher)==0){
                         $status = array('status' => 'failed','message' => array(' Redeemtion vouchers not available now'));
                         return $this->response(__FUNCTION__,$status,$datas);
                    }
                    foreach ($all_Voucher as $key => $value) {
                        $ram[]=$value->id;
                    }

                    $ram=$ram[array_rand($ram)];
                    
                    $Voucher = CHAMP_Voucher::find($ram);


                        $Voucher->level = $this->Request['level'];
                        $Voucher->character = trim($this->Request['character']);
                        $Voucher->issued_at = Carbon::now();
                        $Voucher->issued_to_user_id = $User->id;
                        $Voucher->save();
        	        	$Redemption = CHAMP_Redemption::create(array(
        	        			'voucher_id' => $ram,
        	        			'user_id' => $User->id,
        	        		));

    	        } else {
    	        	$status =  $this->response(__FUNCTION__,$status,$datas=CHAMP_Voucher::where('issued_to_user_id',$User->id)->where('level',$this->Request['level'])->where('character',$this->Request['character'])->first());
    	        }
    		} else {
    			$status = array('status' => 'failed','message' => array('Guest will not be able to redeem vouchers. Connect with Facebook or email to enjoy these benefits.'));
    		}
        }else{
            $status = array('status' => 'failed','message' => array(' Redeemtion vouchers not available now'));
            return $this->response(__FUNCTION__,$status,$datas);
        }


        return $this->response(__FUNCTION__,$status,$datas=$Voucher);
    }

    public function get_content()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);
        $datas['XML'] = CHAMP_Content::where("updated_at",">",$this->Request['last_updated_at'])->get();

        $datas['IMAGE'] = CHAMP_Question_Image::where("updated_at",">",$this->Request['last_updated_at'])->get();

        foreach ($datas['XML'] as $key => $value) {
            if($value->file!=""){
                $value->file = url('uploads/'.$value->file);
            }
        }
        

        return $this->response(__FUNCTION__,$status,$datas);
    }


    public function question_image()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);
        $datas=CHAMP_Question_Image::all();

        

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function my_vouchers()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);
        $datas = CHAMP_Voucher::where('issued_to_user_id',$User->id)->get();

        

        return $this->response(__FUNCTION__,$status,$datas);
    }

    // public function get_ambition()
    // {
    //     $datas = array();
    //     $status = array();
    //     $User = $this->getUserIDByApiToken($this->Request['api_token']);

    //     $qArray = array();
    //     $Scores = CHAMP_Score::where('user_id', $User->id)->get();

    // 	foreach ($Scores as $Score)
    // 	{
    // 		$qArray = json_decode($Score->questions_answered)? json_decode($Score->questions_answered,true):array();
    // 		foreach ($qArray as $key => $qAns)
    // 		{
    // 			$qArray[$key] = $qArray[$key] + $qAns;
    // 		}
    // 	}

    // 	$highestValue = max($qArray);
    // 	$highestValueArray = array_keys($qArray, $highestValue);
    // 	$ambition = $highestValueArray[mt_rand(0, count($highestValueArray) - 1)];

    //     $datas['ambition'] = $ambition;

    //     return $this->response(__FUNCTION__,$status,$datas);
    // }
}