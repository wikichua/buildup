<?php

namespace BUP\Http\Controllers\CHAMP;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
// use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\CHAMP\CHAMP_Voucher;
use BUP\Models\CHAMP\CHAMP_Setting;

class CHAMP_VoucherController extends Controller
{
    // use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $this->reorganizeSEQ(new CHAMP_Voucher);
     	$Vouchers = CHAMP_Voucher::search()->sort()->orderBy('created_at','desc')->paginate(25);
        $active=CHAMP_Setting::find(1);
        $Active=$active->voucher_actived;
        // $this->index_shift($Vouchers, new CHAMP_Voucher);
        return view('CHAMP.voucher.index')->with(compact('Vouchers','Active'));
    }

    public function active()
    {
        $active=CHAMP_Setting::find(1);
        if($active->voucher_actived=='YES'){
            $active->voucher_actived='NO';
            $active->save();
        }else{
            $active->voucher_actived='YES';
            $active->save();
        }
         return redirect()->route('champ.voucher')->with('success',($active->voucher_actived=='YES')?"Voucher Actived":"Voucher Inactived");
    }

    public function create()
    {
        return view('CHAMP.voucher.create');
    }

    public function store(Request $request)
    {
        $Voucher = CHAMP_Voucher::create(
                array(
                        
                        'name' => $request->get('name'),
                        'description' => $request->get('description'),
                        
                    )
            );

        return redirect()->route('champ.voucher')->with('success','Record created.');
    }

    public function edit($id)
    {
        $Voucher = CHAMP_Voucher::find($id);
        return view('CHAMP.voucher.edit')->with(compact('Voucher'));
    }

    public function update(Request $request, $id)
    {
        $Voucher = CHAMP_Voucher::find($id);
        $Voucher->name = $request->get('name',$Voucher->name);
        $Voucher->description = $request->get('description',$Voucher->description);
        $Voucher->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return CHAMP_Voucher::destroy($id);
    }

    // public function shift($id, $shift_id)
    // {
    //     $this->shifting(new CHAMP_Voucher, $id, $shift_id);

    //     return back();
    // }
}
