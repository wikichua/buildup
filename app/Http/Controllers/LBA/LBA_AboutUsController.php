<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
use BUP\Models\LBA\LBA_About_Us;

class LBA_AboutUsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
        ($About = LBA_About_Us::find(1)) ? $About = LBA_About_Us::find(1) : $About = LBA_About_Us::create(array());
        return view('LBA.about_us.edit')->with(compact('About'));
    }

    public function update(Request $request)
    {
        $About = LBA_About_Us::find(1);
        $About->image = uploadImage('image',$About->image,'lba-about-us-image');
        $About->description = $request->get('description','');
        $About->save();
        
        return back()->with('success','Record updated.');
    }
}
