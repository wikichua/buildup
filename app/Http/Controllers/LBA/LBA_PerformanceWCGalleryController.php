<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\PerformanceWCGalleryRequest;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Performance_WC_Gallery;

class LBA_PerformanceWCGalleryController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($cat_id, $prod_id)
    {
    	$this->reorganizeSEQ(LBA_Performance_WC_Gallery::where('prod_id',$prod_id));
        $PerformanceWCGalleries = LBA_Performance_WC_Gallery::where('prod_id',$prod_id)->search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($PerformanceWCGalleries, LBA_Performance_WC_Gallery::where('prod_id',$prod_id));
        return view('LBA.performance_wc_gallery.index')->with(compact('PerformanceWCGalleries','cat_id','prod_id'));
    }

    public function create($cat_id, $prod_id)
    {
        return view('LBA.performance_wc_gallery.create')->with(compact('cat_id','prod_id'));
    }

    public function store(PerformanceWCGalleryRequest $request, $cat_id, $prod_id)
    {
        $PerformanceWCGallery = LBA_Performance_WC_Gallery::create(
        		array(
	                    'seq' => LBA_Performance_WC_Gallery::where('prod_id',$prod_id)->max('seq') + 1,
	                    'prod_id' => $prod_id,
	                    'image' => uploadImage('image','','lba-performance-wc-gallery-image'),
	                    'title' => '',
	                    'description' => '',
                    )
            );

        return redirect()->route('lba.performance.wc.gallery',array($cat_id, $prod_id))->with('success','Record created.');
    }

    public function edit($cat_id, $prod_id, $id)
    {
        $PerformanceWCGallery = LBA_Performance_WC_Gallery::find($id);
        return view('LBA.performance_wc_gallery.edit')->with(compact('PerformanceWCGallery','cat_id','prod_id'));
    }

    public function update(PerformanceWCGalleryRequest $request, $cat_id, $prod_id, $id)
    {
        $PerformanceWCGallery = LBA_Performance_WC_Gallery::find($id);
        $PerformanceWCGallery->image = uploadImage('image',$PerformanceWCGallery->image,'lba-performance-wc-gallery-image');
        // $PerformanceWCGallery->title = $request->get('title',$PerformanceWCGallery->title);
        // $PerformanceWCGallery->description = $request->get('description',$PerformanceWCGallery->description);
        $PerformanceWCGallery->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($cat_id, $prod_id, $id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Performance_WC_Gallery::destroy($id);
    }

    public function shift($cat_id, $prod_id, $id, $shift_id)
    {
        $this->shifting(new LBA_Performance_WC_Gallery, $id, $shift_id);

        return back();
    }
}


