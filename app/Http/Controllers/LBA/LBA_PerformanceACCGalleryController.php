<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\PerformanceACCGalleryRequest;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Performance_ACC_Gallery;

class LBA_PerformanceACCGalleryController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($cat_id, $prod_id)
    {
    	$this->reorganizeSEQ(LBA_Performance_ACC_Gallery::where('prod_id',$prod_id));
        $PerformanceACCGalleries = LBA_Performance_ACC_Gallery::where('prod_id',$prod_id)->search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($PerformanceACCGalleries, LBA_Performance_ACC_Gallery::where('prod_id',$prod_id));
        return view('LBA.performance_acc_gallery.index')->with(compact('PerformanceACCGalleries','cat_id','prod_id'));
    }

    public function create($cat_id, $prod_id)
    {
        return view('LBA.performance_acc_gallery.create')->with(compact('cat_id','prod_id'));
    }

    public function store(PerformanceACCGalleryRequest $request, $cat_id, $prod_id)
    {
        $PerformanceACCGallery = LBA_Performance_ACC_Gallery::create(
        		array(
	                    'seq' => LBA_Performance_ACC_Gallery::where('prod_id',$prod_id)->max('seq') + 1,
	                    'prod_id' => $prod_id,
	                    'image' => uploadImage('image','','lba-performance-acc-gallery-image'),
	                    'title' => '',
	                    'description' => '',
                    )
            );

        return redirect()->route('lba.performance.acc.gallery',array($cat_id, $prod_id))->with('success','Record created.');
    }

    public function edit($cat_id, $prod_id, $id)
    {
        $PerformanceACCGallery = LBA_Performance_ACC_Gallery::find($id);
        return view('LBA.performance_acc_gallery.edit')->with(compact('PerformanceACCGallery','cat_id','prod_id'));
    }

    public function update(PerformanceACCGalleryRequest $request, $cat_id, $prod_id, $id)
    {
        $PerformanceACCGallery = LBA_Performance_ACC_Gallery::find($id);
        $PerformanceACCGallery->image = uploadImage('image',$PerformanceACCGallery->image,'lba-performance-acc-gallery-image');
        // $PerformanceACCGallery->title = $request->get('title',$PerformanceACCGallery->title);
        // $PerformanceACCGallery->description = $request->get('description',$PerformanceACCGallery->description);
        $PerformanceACCGallery->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($cat_id, $prod_id, $id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Performance_ACC_Gallery::destroy($id);
    }

    public function shift($cat_id, $prod_id, $id, $shift_id)
    {
        $this->shifting(new LBA_Performance_ACC_Gallery, $id, $shift_id);

        return back();
    }
}



