<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\PerformanceCatalogRequest;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Performance_Catalog;

class LBA_PerformanceCatalogController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->reorganizeSEQ(new LBA_Performance_Catalog);
        $PerformanceCatalogs = LBA_Performance_Catalog::search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($PerformanceCatalogs, new LBA_Performance_Catalog);
        return view('LBA.performance_catalog.index')->with(compact('PerformanceCatalogs'));
    }

    public function create()
    {
        return view('LBA.performance_catalog.create');
    }

    public function store(PerformanceCatalogRequest $request)
    {
        $PerformanceCatalog = LBA_Performance_Catalog::create(
                array(
                        'seq' => LBA_Performance_Catalog::max('seq') + 1,
                        'image' => uploadImage('image','','lba-performance-catalog-image'),
                        'title' => $request->get('title'),
                    )
            );

        return redirect()->route('lba.performance.catalog')->with('success','Record created.');
    }

    public function edit($id)
    {
        $PerformanceCatalog = LBA_Performance_Catalog::find($id);
        return view('LBA.performance_catalog.edit')->with(compact('PerformanceCatalog'));
    }

    public function update(PerformanceCatalogRequest $request, $id)
    {
        $PerformanceCatalog = LBA_Performance_Catalog::find($id);
        $PerformanceCatalog->image = uploadImage('image', $PerformanceCatalog->image,'lba-performance-catalog-image');
        $PerformanceCatalog->title = $request->get('title',$PerformanceCatalog->title);
        $PerformanceCatalog->save();
        
        return back()->with('success','Record updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Performance_Catalog::destroy($id);
    }

    public function shift($id, $shift_id)
    {
        $this->shifting(new LBA_Performance_Catalog, $id, $shift_id);

        return back();
    }
}
