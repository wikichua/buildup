<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\ProjectReferencesRequest;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Project_References;

class LBA_ProjectReferencesController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->reorganizeSEQ(new LBA_Project_References);
        $ProjectReferences = LBA_Project_References::search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($ProjectReferences, new LBA_Project_References);
        return view('LBA.project_references.index')->with(compact('ProjectReferences'));
    }

    public function create()
    {
        return view('LBA.project_references.create');
    }

    public function store(ProjectReferencesRequest $request)
    {
        $ProjectReference = LBA_Project_References::create(
                array(
                        'seq' => LBA_Project_References::max('seq') + 1,
                        'image' => uploadImage('image','','lba-project-references-image'),
                        'title' => $request->get('title'),
                        'description' => $request->get('description','N/A'),
                    )
            );

        return redirect()->route('lba.project.references')->with('success','Record created.');
    }

    public function edit($id)
    {
        $ProjectReference = LBA_Project_References::find($id);
        return view('LBA.project_references.edit')->with(compact('ProjectReference'));
    }

    public function update(ProjectReferencesRequest $request, $id)
    {
        $ProjectReference = LBA_Project_References::find($id);
        $ProjectReference->title = $request->get('title',$ProjectReference->title);
        $ProjectReference->image = uploadImage('image',$ProjectReference->image,'lba-project-references-image');
        $ProjectReference->description = $request->get('description',$ProjectReference->description);
        $ProjectReference->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Project_References::destroy($id);
    }

    public function shift($id, $shift_id)
    {
        $this->shifting(new LBA_Project_References, $id, $shift_id);

        return back();
    }
}
