<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\ContactRequest;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Contact_Us;

class LBA_ContactUsController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->reorganizeSEQ(new LBA_Contact_Us);
        $Contacts = LBA_Contact_Us::search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($Contacts, new LBA_Contact_Us);
        return view('LBA.contact_us.index')->with(compact('Contacts'));
    }

    public function create()
    {
        return view('LBA.contact_us.create');
    }

    public function store(ContactRequest $request)
    {
        $contacts = array();

        if($request->has('c_types') && $request->has('c_inputs'))
        {
            foreach (array_keys($request->get('c_types')) as $key) {
                $contacts[] = array(
                        'c_types'   => $request->get('c_types')[$key],
                        'c_inputs' => $request->get('c_inputs')[$key],
                        'c_tags' => $request->get('c_tags')[$key],
                    );
            }
        }

        $Contact = LBA_Contact_Us::create(
                array(
                        'seq' => LBA_Contact_Us::max('seq') + 1,
                        'title' => $request->get('title'),
                        'address' => $request->get('address'),
                        'latitude' => $request->get('latitude'),
                        'longitude' => $request->get('longitude'),
                        'contacts' => json_encode($contacts)? json_encode($contacts):array(),
                    )
            );

        return redirect()->route('lba.contact.us')->with('success','Record created.');
    }

    public function edit($id)
    {
        $Contact = LBA_Contact_Us::find($id);
        $Contact->contacts = json_decode($Contact->contacts)? json_decode($Contact->contacts):array();
        return view('LBA.contact_us.edit')->with(compact('Contact'));
    }

    public function update(ContactRequest $request, $id)
    {
        $contacts = array();

        if($request->has('c_types') && $request->has('c_inputs'))
        {
            foreach (array_keys($request->get('c_types')) as $key) {
                $contacts[] = array(
                        'c_types'   => $request->get('c_types')[$key],
                        'c_inputs' => $request->get('c_inputs')[$key],
                        'c_tags' => $request->get('c_tags')[$key],
                    );
            }
        }

        $Contact = LBA_Contact_Us::find($id);
        $Contact->title = $request->get('title',$Contact->title);
        $Contact->address = $request->get('address',$Contact->address);
        $Contact->latitude = $request->get('latitude',$Contact->latitude);
        $Contact->longitude = $request->get('longitude',$Contact->longitude);
        $Contact->contacts = json_encode($contacts)? json_encode($contacts):array();
        $Contact->save();
        
        return back()->with('success','Record updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Contact_Us::destroy($id);
    }

    public function shift($id, $shift_id)
    {
        $this->shifting(new LBA_Contact_Us, $id, $shift_id);

        return back();
    }

    // public function edit()
    // {

    //     $email_label=array();
    //     $email=array();
    //     $tel=array();
    //     $tel_label=array();
    //     for($i=0;$i<5;$i++){
    //         // if($request->get('email_label'.$i,'N/A')!='')
    //             $email_label['email_label'.$i]= '';
    //         // if($request->get('email'.$i,'N/A')!='')
    //             $email['email'.$i] = '';
    //         // if($request->get('tel'.$i,'N/A')!='')
    //             $tel['tel'.$i] = '';
    //         // if($request->get('tel_label'.$i,'N/A')!='')
    //             $tel_label['tel_label'.$i] = '';
    //     }

    //     $Email_label = array();
    //     $Email =array();
    //     $Tel_label =array();
    //     $Tel =array();
    //     ($ContactUs = LBA_Contact_Us::find(1)) ? $ContactUs = LBA_Contact_Us::find(1) : $ContactUs = LBA_Contact_Us::create(
    //     [
    //         'email_label'=>json_encode($email_label,JSON_NUMERIC_CHECK),
    //         'email'=>json_encode($email,JSON_NUMERIC_CHECK),
    //         'tel_label'=>json_encode($tel_label,JSON_NUMERIC_CHECK),
    //         'tel'=>json_encode($tel,JSON_NUMERIC_CHECK),
    //     ]
    //         );
    //     // $ContactUs = LBA_Contact_Us::find(1);
    //     if(isset($ContactUs->email_label)?'yes':'no'=='yes'){
    //         $Email_label = isset($ContactUs->email_label)?array_values(json_decode($ContactUs->email_label,true)):'';
    //         $Email = isset($ContactUs->email)?array_values(json_decode($ContactUs->email,true)):'';
    //         $Tel_label = isset($ContactUs->tel_label)?array_values(json_decode($ContactUs->tel_label,true)):'';
    //         $Tel = isset($ContactUs->tel)?array_values(json_decode($ContactUs->tel,true)):'';
    //     }
        
    //     return view('LBA.contact_us.edit')->with(compact('ContactUs','Email_label','Email','Tel_label','Tel'));
    // }

    // public function update(ContactRequest $request)
    // {

    //     $email_label=array();
    //     $email=array();
    //     $tel=array();
    //     $tel_label=array();
    //     for($i=0;$i<5;$i++){
    //         // if($request->get('email_label'.$i,'N/A')!='')
    //             $email_label['email_label'.$i]= $request->get('email_label'.$i,'N/A');
    //         // if($request->get('email'.$i,'N/A')!='')
    //             $email['email'.$i] = $request->get('email'.$i,'N/A');
    //         // if($request->get('tel'.$i,'N/A')!='')
    //             $tel['tel'.$i] = $request->get('tel'.$i,'N/A');
    //         // if($request->get('tel_label'.$i,'N/A')!='')
    //             $tel_label['tel_label'.$i] = $request->get('tel_label'.$i,'N/A');
    //     }
    //     $ContactUs = LBA_Contact_Us::find(1);
    //     $ContactUs->title = $request->get('title',$ContactUs->name);
    //     $ContactUs->email = json_encode($email,JSON_NUMERIC_CHECK);
    //     $ContactUs->email_label = json_encode($email_label,JSON_NUMERIC_CHECK);
    //     $ContactUs->tel = json_encode($tel);
    //     $ContactUs->tel_label = json_encode($tel_label,JSON_NUMERIC_CHECK);
    //     $ContactUs->address = $request->get('address',$ContactUs->contact_name_2);
    //     $ContactUs->gps = $request->get('gps',$ContactUs->contact_number_2);
    //     $ContactUs->save();

    //     return back()->with('success','Record updated.');
    // }
}
