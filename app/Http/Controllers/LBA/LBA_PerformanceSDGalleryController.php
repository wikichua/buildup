<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\PerformanceSDGalleryRequest;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Performance_SD_Gallery;

class LBA_PerformanceSDGalleryController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($cat_id, $prod_id, $gallery_cat_id)
    {
    	$this->reorganizeSEQ(LBA_Performance_SD_Gallery::where('gallery_cat_id',$gallery_cat_id));
        $PerformanceSDGalleries = LBA_Performance_SD_Gallery::where('gallery_cat_id',$gallery_cat_id)->search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($PerformanceSDGalleries, LBA_Performance_SD_Gallery::where('gallery_cat_id',$gallery_cat_id));
        return view('LBA.performance_sd_gallery.index')->with(compact('PerformanceSDGalleries','cat_id','prod_id','gallery_cat_id'));
    }

    public function create($cat_id, $prod_id, $gallery_cat_id)
    {
        return view('LBA.performance_sd_gallery.create')->with(compact('cat_id','prod_id','gallery_cat_id'));
    }

    public function store(PerformanceSDGalleryRequest $request, $cat_id, $prod_id, $gallery_cat_id)
    {
                $PerformanceSDGallery = LBA_Performance_SD_Gallery::create(
        		array(
	                    'seq' => LBA_Performance_SD_Gallery::where('gallery_cat_id',$gallery_cat_id)->max('seq') + 1,
	                    'gallery_cat_id' => $gallery_cat_id,
                        'section_no' => $request->get('section_no'),
	                    'image' => uploadImage('image','','lba-performance-sd-gallery-image'),
	                    'title' => '',
	                    'description' => '',
                    )
            );

        return redirect()->route('lba.performance.sd.gallery',array($cat_id, $prod_id, $gallery_cat_id))->with('success','Record created.');
    }

    public function edit($cat_id, $prod_id, $gallery_cat_id, $id)
    {
        $PerformanceSDGallery = LBA_Performance_SD_Gallery::find($id);
        return view('LBA.performance_sd_gallery.edit')->with(compact('PerformanceSDGallery','cat_id','prod_id','gallery_cat_id'));
    }

    public function update(PerformanceSDGalleryRequest $request, $cat_id, $prod_id, $gallery_cat_id, $id)
    {
        $PerformanceSDGallery = LBA_Performance_SD_Gallery::find($id);
        $PerformanceSDGallery->image = uploadImage('image',$PerformanceSDGallery->image,'lba-performance-sd-gallery-image');
         $PerformanceSDGallery->section_no=$request->get('section_no');
        // $PerformanceSDGallery->title = $request->get('title',$PerformanceSDGallery->title);
        // $PerformanceSDGallery->description = $request->get('description',$PerformanceSDGallery->description);
        $PerformanceSDGallery->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($cat_id, $prod_id, $gallery_cat_id, $id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Performance_SD_Gallery::destroy($id);
    }

    public function shift($cat_id, $prod_id, $gallery_cat_id, $id, $shift_id)
    {
        $this->shifting(new LBA_Performance_SD_Gallery, $id, $shift_id);

        return back();
    }
}


