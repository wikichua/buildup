<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\StandardSDGalleryRequest;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Standard_SD_Gallery;

class LBA_StandardSDGalleryController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($cat_id, $prod_id, $gallery_cat_id)
    {
    	$this->reorganizeSEQ(LBA_Standard_SD_Gallery::where('gallery_cat_id',$gallery_cat_id));
        $StandardSDGalleries = LBA_Standard_SD_Gallery::where('gallery_cat_id',$gallery_cat_id)->search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($StandardSDGalleries, LBA_Standard_SD_Gallery::where('gallery_cat_id',$gallery_cat_id));
        return view('LBA.standard_sd_gallery.index')->with(compact('StandardSDGalleries','gallery_cat_id','cat_id','prod_id'));
    }

    public function create($cat_id, $prod_id, $gallery_cat_id)
    {
        return view('LBA.standard_sd_gallery.create')->with(compact('gallery_cat_id','cat_id','prod_id'));
    }

    public function store(StandardSDGalleryRequest $request, $cat_id, $prod_id, $gallery_cat_id)
    {
        $StandardSDGallery = LBA_Standard_SD_Gallery::create(
        		array(
	                    'seq' => LBA_Standard_SD_Gallery::where('gallery_cat_id',$gallery_cat_id)->max('seq') + 1,
	                    'gallery_cat_id' => $gallery_cat_id,
                        'section_no' => $request->get('section_no'),
	                    'image' => uploadImage('image','','lba-standard-sd-gallery-image'),
	                    'title' => '',
	                    'description' => '',
                    )
            );

        return redirect()->route('lba.standard.sd.gallery',array($cat_id,$prod_id,$gallery_cat_id))->with('success','Record created.');
    }

    public function edit($cat_id, $prod_id, $gallery_cat_id, $id)
    {
        $StandardSDGallery = LBA_Standard_SD_Gallery::find($id);
        return view('LBA.standard_sd_gallery.edit')->with(compact('StandardSDGallery','cat_id','prod_id','gallery_cat_id'));
    }

    public function update(StandardSDGalleryRequest $request, $cat_id, $prod_id, $gallery_cat_id, $id)
    {
        $StandardSDGallery = LBA_Standard_SD_Gallery::find($id);
        $StandardSDGallery->image = uploadImage('image',$StandardSDGallery->image,'lba-standard-sd-gallery-image');
        $StandardSDGallery->section_no = $request->get('section_no');
        // $StandardSDGallery->title = $request->get('title',$StandardSDGallery->title);
        // $StandardSDGallery->description = $request->get('description',$StandardSDGallery->description);
        $StandardSDGallery->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($cat_id, $prod_id, $gallery_cat_id, $id)
    {
        session()->flash('success','Record deleteds.');
        return LBA_Standard_SD_Gallery::destroy($id);
    }

    public function shift($cat_id, $prod_id, $gallery_cat_id, $id, $shift_id)
    {
        $this->shifting(new LBA_Standard_SD_Gallery, $id, $shift_id);

        return back();
    }
}

