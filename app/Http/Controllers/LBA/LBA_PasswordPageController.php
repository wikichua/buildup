<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\PasswordPageRequest;
use BUP\Http\Controllers\Controller;
use BUP\Models\LBA\LBA_Password_Page;

class LBA_PasswordPageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
        ($PasswordPage = LBA_Password_Page::find(1)) ? $PasswordPage = LBA_Password_Page::find(1) : $PasswordPage = LBA_Password_Page::create(
            array(
                    'seq' => LBA_Password_Page::max('seq') + 1,
                    'password' => 'ABC123abc',
                )
        );
        return view('LBA.password_page.edit')->with(compact('PasswordPage'));
    }

    public function update(PasswordPageRequest $request)
    {
        $PasswordPage = LBA_Password_Page::find(1);
        $PasswordPage->password = $request->get('password',$PasswordPage->password);
        $PasswordPage->save();

        return back()->with('success','Record updated.');
    }
}
