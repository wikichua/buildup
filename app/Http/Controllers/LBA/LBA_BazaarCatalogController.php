<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\BazaarCatalogRequest;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Bazaar_Catalog;

class LBA_BazaarCatalogController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->reorganizeSEQ(new LBA_Bazaar_Catalog);
        $BazaarCatalogs = LBA_Bazaar_Catalog::search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($BazaarCatalogs, new LBA_Bazaar_Catalog);
        return view('LBA.bazaar_catalog.index')->with(compact('BazaarCatalogs'));
    }

    public function create()
    {
        return view('LBA.bazaar_catalog.create');
    }

    public function store(BazaarCatalogRequest $request)
    {
        $BazaarCatalog = LBA_Bazaar_Catalog::create(
                array(
                        'seq' => LBA_Bazaar_Catalog::max('seq') + 1,
                        'image' => uploadImage('image','','lba-bazaar-catalog-image'),
                        'title' => $request->get('title'),
                    )
            );

        return redirect()->route('lba.bazaar.catalog')->with('success','Record created.');
    }

    public function edit($id)
    {
        $BazaarCatalog = LBA_Bazaar_Catalog::find($id);
        return view('LBA.bazaar_catalog.edit')->with(compact('BazaarCatalog'));
    }

    public function update(BazaarCatalogRequest $request, $id)
    {
        $BazaarCatalog = LBA_Bazaar_Catalog::find($id);
        $BazaarCatalog->image = uploadImage('image',$BazaarCatalog->image,'lba-bazaar-catalog-image');
        $BazaarCatalog->title = $request->get('title',$BazaarCatalog->title);
        $BazaarCatalog->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Bazaar_Catalog::destroy($id);
    }

    public function shift($id, $shift_id)
    {
        $this->shifting(new LBA_Bazaar_Catalog, $id, $shift_id);

        return back();
    }
}
