<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\PerformanceTSDGalleryRequest;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Performance_TSD_Gallery;

class LBA_PerformanceTSDGalleryController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($cat_id, $prod_id)
    {
    	$this->reorganizeSEQ(LBA_Performance_TSD_Gallery::where('prod_id',$prod_id));
        $PerformanceTSDGalleries = LBA_Performance_TSD_Gallery::where('prod_id',$prod_id)->search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($PerformanceTSDGalleries, LBA_Performance_TSD_Gallery::where('prod_id',$prod_id));
        return view('LBA.performance_tsd_gallery.index')->with(compact('PerformanceTSDGalleries','cat_id','prod_id'));
    }

    public function create($cat_id, $prod_id)
    {
        return view('LBA.performance_tsd_gallery.create')->with(compact('cat_id','prod_id'));
    }

    public function store(PerformanceTSDGalleryRequest $request, $cat_id, $prod_id)
    {
        $PerformanceTSDGallery = LBA_Performance_TSD_Gallery::create(
        		array(
	                    'seq' => LBA_Performance_TSD_Gallery::where('prod_id',$prod_id)->max('seq') + 1,
	                    'prod_id' => $prod_id,
	                    'image' => uploadImage('image','','lba-performance-tsd-gallery-image'),
	                    'title' => '',
	                    'description' => '',
                    )
            );

        return redirect()->route('lba.performance.tsd.gallery',array($cat_id, $prod_id))->with('success','Record created.');
    }

    public function edit($cat_id, $prod_id, $id)
    {
        $PerformanceTSDGallery = LBA_Performance_TSD_Gallery::find($id);
        return view('LBA.performance_tsd_gallery.edit')->with(compact('PerformanceTSDGallery','cat_id','prod_id'));
    }

    public function update(PerformanceTSDGalleryRequest $request, $cat_id, $prod_id, $id)
    {
        $PerformanceTSDGallery = LBA_Performance_TSD_Gallery::find($id);
        $PerformanceTSDGallery->image = uploadImage('image',$PerformanceTSDGallery->image,'lba-performance-tsd-gallery-image');
        // $PerformanceTSDGallery->title = $request->get('title',$PerformanceTSDGallery->title);
        // $PerformanceTSDGallery->description = $request->get('description',$PerformanceTSDGallery->description);
        $PerformanceTSDGallery->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($cat_id, $prod_id, $id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Performance_TSD_Gallery::destroy($id);
    }

    public function shift($cat_id, $prod_id, $id, $shift_id)
    {
        $this->shifting(new LBA_Performance_TSD_Gallery, $id, $shift_id);

        return back();
    }
}

