<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\StandardTSDGalleryRequest;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Standard_TSD_Gallery;

class LBA_StandardTSDGalleryController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($cat_id, $prod_id)
    {
    	$this->reorganizeSEQ(LBA_Standard_TSD_Gallery::where('prod_id',$prod_id));
        $StandardTSDGalleries = LBA_Standard_TSD_Gallery::where('prod_id',$prod_id)->search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($StandardTSDGalleries, LBA_Standard_TSD_Gallery::where('prod_id',$prod_id));
        return view('LBA.standard_tsd_gallery.index')->with(compact('StandardTSDGalleries','prod_id','cat_id'));
    }

    public function create($cat_id, $prod_id)
    {
        return view('LBA.standard_tsd_gallery.create')->with(compact('prod_id','cat_id'));
    }


    public function store(StandardTSDGalleryRequest $request, $cat_id, $prod_id)
    {
        $StandardTSDGallery = LBA_Standard_TSD_Gallery::create(
        		array(
	                    'seq' => LBA_Standard_TSD_Gallery::where('prod_id',$prod_id)->max('seq') + 1,
	                    'prod_id' => $prod_id,
	                    'image' => uploadImage('image','','lba-standard-tsd-gallery-image'),
	                    'title' => '',
	                    'description' => '',
                    )
            );

        return redirect()->route('lba.standard.tsd.gallery',array($cat_id,$prod_id))->with('success','Record created.');
    }

    public function edit($cat_id, $prod_id, $id)
    {
        $StandardTSDGallery = LBA_Standard_TSD_Gallery::find($id);
        return view('LBA.standard_tsd_gallery.edit')->with(compact('StandardTSDGallery','cat_id','prod_id'));
    }


    public function update(StandardTSDGalleryRequest $request, $cat_id, $prod_id, $id)
    {
        $StandardTSDGallery = LBA_Standard_TSD_Gallery::find($id);
        $StandardTSDGallery->image = uploadImage('image',$StandardTSDGallery->image,'lba-standard-tsd-gallery-image');
        // $StandardTSDGallery->title = $request->get('title',$StandardTSDGallery->title);
        // $StandardTSDGallery->description = $request->get('description',$StandardTSDGallery->description);
        $StandardTSDGallery->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($cat_id, $prod_id, $id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Standard_TSD_Gallery::destroy($id);
    }

    public function shift($cat_id, $prod_id, $id, $shift_id)
    {
        $this->shifting(new LBA_Standard_TSD_Gallery, $id, $shift_id);

        return back();
    }
}
