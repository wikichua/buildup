<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
use BUP\Http\Requests\LBA\BazaarProductRequest;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Bazaar_Item;
use BUP\Models\LBA\LBA_Bazaar_Product;

class LBA_BazaarProductController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($cat_id)
    {
        $this->reorganizeSEQ( LBA_Bazaar_Product::where('cat_id',$cat_id));
        $BazaarProducts = LBA_Bazaar_Product::where('cat_id',$cat_id)->search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($BazaarProducts,LBA_Bazaar_Product::where('cat_id',$cat_id));
        return view('LBA.bazaar_product.index')->with(compact('BazaarProducts','cat_id'));
    }

    public function create($cat_id)
    {
        return view('LBA.bazaar_product.create')->with(compact('cat_id'));
    }

    public function store(BazaarProductRequest $request, $cat_id)
    {
        $fields = array();
        foreach ($request->get('fields') as $key => $field)
        {
            $fields['field_'.($key + 1)] = $field;
        }

        $BazaarProduct = LBA_Bazaar_Product::create(
                array(
                        'seq' => LBA_Bazaar_Product::where("cat_id",$cat_id)->max('seq') + 1,
                        'cat_id' => $cat_id,
                        'image' => uploadImage('image','','lba-bazaar-product-image'),
                        'title' => $request->get('title'),
                        'fields' => json_encode($fields)? json_encode($fields,true):array(),
                    )
            );

        return redirect()->route('lba.bazaar.product',$cat_id)->with('success','Record created.');
    }

    public function edit($cat_id, $id)
    {
        $BazaarProduct = LBA_Bazaar_Product::find($id);
        $BazaarProduct->fields = json_decode($BazaarProduct->fields)? json_decode($BazaarProduct->fields,true):array();

        return view('LBA.bazaar_product.edit')->with(compact('BazaarProduct','cat_id'));
    }

    public function update(BazaarProductRequest $request, $cat_id, $id)
    {
        $fields = array();
        foreach ($request->get('fields') as $key => $field)
        {
            $fields['field_'.($key + 1)] = $field;
        }

        $BazaarProduct = LBA_Bazaar_Product::find($id);
        $BazaarProduct->image = uploadImage('image',$BazaarProduct->image,'lba-bazaar-product-image');
        $BazaarProduct->title = $request->get('title',$BazaarProduct->title);
        $BazaarProduct->fields = json_encode($fields)? json_encode($fields,true):array();
        $BazaarProduct->save();

        $BazaarProduct->fields = json_decode($BazaarProduct->fields)? json_decode($BazaarProduct->fields,true):array();

        $BazaarItem = LBA_Bazaar_Item::where('prod_id',$id)->get();
            foreach ($BazaarItem as $key => $value) {
                $Values = isset($value->values)?json_decode($value->values,true):array();
                $newValues=array();

                foreach ($BazaarProduct->fields as $key => $field) {
                    $newValues[$key] = isset($Values[$key])?$Values[$key]:'0';
                    $value->values = json_encode($newValues)? json_encode($newValues,true):array();
                    $value->save();
                }
                
            }
        

        

        return back()->with('success','Record updated.');
    }

    public function destroy($cat_id, $id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Bazaar_Product::destroy($id);
    }

    public function shift($cat_id, $id, $shift_id)
    {
        $this->shifting(new LBA_Bazaar_Product, $id, $shift_id);

        return back();
    }
}
