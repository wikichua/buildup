<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\StandardSDGalleryCategoryRequest;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Standard_SD_Gallery_Category;

class LBA_StandardSDGalleryCategoryController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($cat_id, $prod_id)
    {
    	$this->reorganizeSEQ(LBA_Standard_SD_Gallery_Category::where('prod_id',$prod_id));
        $StandardSDGalleryCategories = LBA_Standard_SD_Gallery_Category::where('prod_id',$prod_id)->search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($StandardSDGalleryCategories, LBA_Standard_SD_Gallery_Category::where('prod_id',$prod_id));
        return view('LBA.standard_sd_gallery_category.index')->with(compact('StandardSDGalleryCategories','cat_id','prod_id'));
    }

    public function create($cat_id, $prod_id)
    {
        return view('LBA.standard_sd_gallery_category.create')->with(compact('cat_id','prod_id'));
    }

    public function store(StandardSDGalleryCategoryRequest $request, $cat_id, $prod_id)
    {
        $StandardSDGalleryCategory = LBA_Standard_SD_Gallery_Category::create(
        		array(
	                    'seq' => LBA_Standard_SD_Gallery_Category::where('prod_id',$prod_id)->max('seq') + 1,
	                    'prod_id' => $prod_id,
	                    'title' => $request->get('title'),
                    )
            );

        return redirect()->route('lba.standard.sd.gallery.category',array($cat_id,$prod_id))->with('success','Record created.');
    }

    public function edit($cat_id, $prod_id, $id)
    {
        $StandardSDGalleryCategory = LBA_Standard_SD_Gallery_Category::find($id);
        return view('LBA.standard_sd_gallery_category.edit')->with(compact('StandardSDGalleryCategory','prod_id','cat_id'));
    }

    public function update(StandardSDGalleryCategoryRequest $request, $cat_id, $prod_id, $id)
    {
        $StandardSDGalleryCategory = LBA_Standard_SD_Gallery_Category::find($id);
        $StandardSDGalleryCategory->title = $request->get('title',$StandardSDGalleryCategory->title);
        $StandardSDGalleryCategory->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($cat_id, $prod_id, $id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Standard_SD_Gallery_Category::destroy($id);
    }

    public function shift($cat_id, $prod_id, $id, $shift_id)
    {
        $this->shifting(new LBA_Standard_SD_Gallery_Category, $id, $shift_id);

        return back();
    }
}
