<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\AlloySpecificationRequest;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Alloy_Specification;

class LBA_AlloySpecificationController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->reorganizeSEQ(new LBA_Alloy_Specification);
        $AlloySpecifications = LBA_Alloy_Specification::search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($AlloySpecifications, new LBA_Alloy_Specification);
        return view('LBA.alloy_specification.index')->with(compact('AlloySpecifications'));
    }

    public function create()
    {
        return view('LBA.alloy_specification.create');
    }

    public function store(AlloySpecificationRequest $request)
    {
        $AlloySpecification = LBA_Alloy_Specification::create(
                array(
                        'seq' => LBA_Alloy_Specification::max('seq') + 1,
                        'name' => $request->get('name'),
                        'source' => uploadFile('source','','lba-alloy-specification-files')
                    )
            );

        return redirect()->route('lba.alloy.specification')->with('success','Record created.');
    }

    public function edit($id)
    {
        $AlloySpecification = LBA_Alloy_Specification::find($id);
        return view('LBA.alloy_specification.edit')->with(compact('AlloySpecification'));
    }

    public function update(AlloySpecificationRequest $request, $id)
    {
        $AlloySpecification = LBA_Alloy_Specification::find($id);
        $AlloySpecification->name = $request->get('name',$AlloySpecification->name);
        $AlloySpecification->source = uploadFile('source', $AlloySpecification->source,'lba-alloy-specification-files');
        $AlloySpecification->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Alloy_Specification::destroy($id);
    }

    public function shift($id, $shift_id)
    {
        $this->shifting(new LBA_Alloy_Specification, $id, $shift_id);

        return back();
    }
}
