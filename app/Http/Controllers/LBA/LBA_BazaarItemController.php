<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
use BUP\Http\Requests\LBA\BazaarItemRequest;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Bazaar_Product;
use BUP\Models\LBA\LBA_Bazaar_Item;

class LBA_BazaarItemController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($cat_id, $prod_id)
    {
        $this->reorganizeSEQ( LBA_Bazaar_Item::where('prod_id',$prod_id));
        $BazaarItems = LBA_Bazaar_Item::where('prod_id', $prod_id)->search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($BazaarItems, LBA_Bazaar_Item::where('prod_id',$prod_id));
        return view('LBA.bazaar_item.index')->with(compact('BazaarItems','cat_id','prod_id'));
    }

    public function create($cat_id, $prod_id)
    {
        $BazaarProduct = LBA_Bazaar_Product::find($prod_id);
        $BazaarProduct->fields = json_decode($BazaarProduct->fields)? json_decode($BazaarProduct->fields,true):array();
        return view('LBA.bazaar_item.create')->with(compact('BazaarProduct','cat_id','prod_id'));
    }

    public function store(BazaarItemRequest $request, $cat_id, $prod_id)
    {
        $BazaarProduct = LBA_Bazaar_Product::find($prod_id);
        $BazaarProduct->fields = json_decode($BazaarProduct->fields)? json_decode($BazaarProduct->fields,true):array();
        $Values = array();
        foreach ($BazaarProduct->fields as $key => $field) {
            $Values[$key] = $request->get($key);
        }

        $BazaarItem = LBA_Bazaar_Item::create(
                array(
                        'seq' => LBA_Bazaar_Item::where('prod_id',$prod_id)->max('seq') + 1,
                        'prod_id' => $prod_id,
                        'section_no' => $request->get('section_no'),
                        'image' => uploadImage('image','','lba-bazaar-item-image'),
                        'title' => $request->get('title'),
                        'legend' => '',
                        'weight'=>$request->get('weight'),
                        'ap' => $request->get('ap'),
                        'alloy_type' => $request->get('alloy_type'),
                        'values' => json_encode($Values)? json_encode($Values,true):array(),
                    )
            );

        return redirect()->route('lba.bazaar.item', array($cat_id, $prod_id))->with('success','Record created.');
    }

    public function edit($cat_id, $prod_id, $id)
    {
        $BazaarProduct = LBA_Bazaar_Product::find($prod_id);
        $BazaarProduct->fields = json_decode($BazaarProduct->fields)? json_decode($BazaarProduct->fields,true):array();
        $BazaarItem = LBA_Bazaar_Item::find($id);
        $BazaarItem->values = json_decode($BazaarItem->values)? json_decode($BazaarItem->values,true):array();
        return view('LBA.bazaar_item.edit')->with(compact('BazaarProduct','BazaarItem','cat_id','prod_id'));
    }

    public function update(BazaarItemRequest $request, $cat_id, $prod_id, $id)
    {
        // $Bazaar_Item = LBA_Bazaar_Item::find($id);
        // $Bazaar_Item =json_decode($Bazaar_Item->values,true);
        // $Values = array();
        // foreach ($Bazaar_Item as $key => $Value) {
        //     $Values[$key] = $request->get($key);
        // }

        $BazaarProduct = LBA_Bazaar_Product::find($prod_id);
        $BazaarProduct->fields = json_decode($BazaarProduct->fields)? json_decode($BazaarProduct->fields,true):array();

        $Values = array();
        foreach ($BazaarProduct->fields as $key => $field) {
            $Values[$key] = ($request->get($key)!='')?$request->get($key):'0';
        }

        $BazaarItem = LBA_Bazaar_Item::find($id);
        $BazaarItem->section_no = $request->get('section_no',$BazaarItem->section_no);
        $BazaarItem->image = uploadImage('image',$BazaarItem->image,'lba-bazaar-item-image');
        $BazaarItem->title = $request->get('title',$BazaarItem->title);
        $BazaarItem->weight = $request->get('weight',$BazaarItem->weight);
        $BazaarItem->ap = $request->get('ap',$BazaarItem->ap);
        $BazaarItem->alloy_type = $request->get('alloy_type',$BazaarItem->alloy_type);
        $BazaarItem->values = json_encode($Values)? json_encode($Values,true):array();
        $BazaarItem->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($cat_id, $prod_id, $id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Bazaar_Item::destroy($id);
    }

    public function shift($cat_id, $prod_id, $id, $shift_id)
    {
        $this->shifting(new LBA_Bazaar_Item, $id, $shift_id);

        return back();
    }
}
