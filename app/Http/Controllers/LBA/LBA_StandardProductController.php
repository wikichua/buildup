<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\StandardProductRequest;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Standard_Product;

class LBA_StandardProductController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($cat_id)
    {
        $this->reorganizeSEQ(LBA_Standard_Product::where('cat_id',$cat_id));
        $StandardProducts = LBA_Standard_Product::where('cat_id',$cat_id)->search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($StandardProducts, LBA_Standard_Product::where('cat_id',$cat_id));
        return view('LBA.standard_product.index')->with(compact('StandardProducts','cat_id'));
    }

    public function create($cat_id)
    {
        return view('LBA.standard_product.create')->with(compact('cat_id'));
    }

    public function store(StandardProductRequest $request, $cat_id)
    {
        $StandardProduct = LBA_Standard_Product::create(
                array(
                        'seq' => LBA_Standard_Product::where('cat_id',$cat_id)->max('seq') + 1,
                        'cat_id' => $cat_id,
                        'title' => $request->get('title'),
                        'description' => $request->get('description'),
                        'type_system' => $request->get('type_system',''),
                        'thickness' => $request->get('thickness',''),
                        'type' => $request->get('type',''),
                        // 'glass_thickness' => $request->get('glass_thickness',''),
                        // 'depth' => $request->get('depth',''),
                    )
            );

        return redirect()->route('lba.standard.product',$cat_id)->with('success','Record created.');
    }

    public function edit($cat_id, $id)
    {
        $StandardProduct = LBA_Standard_Product::find($id);
        return view('LBA.standard_product.edit')->with(compact('StandardProduct','cat_id'));
    }

    public function update(StandardProductRequest $request, $cat_id, $id)
    {
        $StandardProduct = LBA_Standard_Product::find($id);
        $StandardProduct->title = $request->get('title',$StandardProduct->title);
        $StandardProduct->description = $request->get('description',$StandardProduct->description);
        $StandardProduct->type_system = $request->get('type_system',$StandardProduct->type_system);
        $StandardProduct->thickness = $request->get('thickness',$StandardProduct->thickness);
        $StandardProduct->type = $request->get('type',$StandardProduct->type);
        // $StandardProduct->glass_thickness = $request->get('glass_thickness',$StandardProduct->glass_thickness);
        // $StandardProduct->depth = $request->get('depth',$StandardProduct->depth);
        $StandardProduct->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($cat_id, $id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Standard_Product::destroy($id);
    }

    public function shift($cat_id, $id, $shift_id)
    {
        $this->shifting(new LBA_Standard_Product, $id, $shift_id);

        return back();
    }
}
