<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\ScreenPopUpRequest;
use BUP\Http\Controllers\Controller;
// use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Screen_Pop_Up;
use Carbon\Carbon;

class LBA_ScreenPopUpController extends Controller
{
    // use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $ScreenPopUps = LBA_Screen_Pop_Up::search()->sort()->orderBy('created_at','desc')->paginate(25);
        return view('LBA.screen_pop_up.index')->with(compact('ScreenPopUps'));
    }

    public function create()
    {
        return view('LBA.screen_pop_up.create');
    }

    public function store(ScreenPopUpRequest $request)
    {
        $ScreenPopUp = LBA_Screen_Pop_Up::create(
                array(
                        'seq' => LBA_Screen_Pop_Up::max('seq') + 1,
                        'image' => uploadImage('image','','lba-screen-popup-image'),
                        'url' => $request->get('url','N/A'),
                        'published_at' =>  $request->get('published_at'),
                        'expired_at' =>  $request->get('expired_at'),
                    )
            );


        return redirect()->route('lba.screen.popup')->with('success','Record created.');
    }

    public function edit()
    {
        ($ScreenPopUp = LBA_Screen_Pop_Up::find(1)) ? $ScreenPopUp = LBA_Screen_Pop_Up::find(1) : $ScreenPopUp = LBA_Screen_Pop_Up::create(
            array(            
                     'published_at' =>Carbon::now(),
                        'expired_at' =>Carbon::now(),
                )
        );
        $ScreenPopUp = LBA_Screen_Pop_Up::find(1);
        return view('LBA.screen_pop_up.edit')->with(compact('ScreenPopUp'));
    }

    public function update(ScreenPopUpRequest $request, $id)
    {
        $ScreenPopUp = LBA_Screen_Pop_Up::find($id);
        $ScreenPopUp->image = uploadImage('image',$ScreenPopUp->image,'lba-screen-popup-image');
        $ScreenPopUp->url = $request->get('url',$ScreenPopUp->url);
        $ScreenPopUp->published_at = $request->get('published_at', $ScreenPopUp->published_at);
        $ScreenPopUp->expired_at = $request->get('expired_at',$ScreenPopUp->expired_at);
        $ScreenPopUp->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Screen_Pop_Up::destroy($id);
    }

    // public function shift($id, $shift_id)
    // {
    //     $this->shifting(new LBA_Screen_Pop_Up, $id, $shift_id);

    //     return back();
    // }
}
