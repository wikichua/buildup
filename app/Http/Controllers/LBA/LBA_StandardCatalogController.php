<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\StandardCatalogRequest;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Standard_Catalog;

class LBA_StandardCatalogController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->reorganizeSEQ(new LBA_Standard_Catalog);
        $StandardCatalogs = LBA_Standard_Catalog::search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($StandardCatalogs, new LBA_Standard_Catalog);
        return view('LBA.standard_catalog.index')->with(compact('StandardCatalogs'));
    }

    public function create()
    {
        return view('LBA.standard_catalog.create');
    }

    public function store(StandardCatalogRequest $request)
    {
        $StandardCatalog = LBA_Standard_Catalog::create(
                array(
                        'seq' => LBA_Standard_Catalog::max('seq') + 1,
                        'image' => uploadImage('image','','lba-standard-catalog-image'),
                        'title' => $request->get('title'),
                    )
            );

        return redirect()->route('lba.standard.catalog')->with('success','Record created.');
    }

    public function edit($id)
    {
        $StandardCatalog = LBA_Standard_Catalog::find($id);
        return view('LBA.standard_catalog.edit')->with(compact('StandardCatalog'));
    }

    public function update(StandardCatalogRequest $request, $id)
    {
        $StandardCatalog = LBA_Standard_Catalog::find($id);
        $StandardCatalog->image = uploadImage('image', $StandardCatalog->image,'lba-standard-catalog-image');
        $StandardCatalog->title = $request->get('title',$StandardCatalog->title);
        $StandardCatalog->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Standard_Catalog::destroy($id);
    }

    public function shift($id, $shift_id)
    {
        $this->shifting(new LBA_Standard_Catalog, $id, $shift_id);

        return back();
    }
}
