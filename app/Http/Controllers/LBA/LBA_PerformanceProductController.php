<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\LBA\PerformanceProductRequest;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\LBA\LBA_Performance_Product;

class LBA_PerformanceProductController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($cat_id)
    {
        $this->reorganizeSEQ(LBA_Performance_Product::where('cat_id',$cat_id));
        $PerformanceProducts = LBA_Performance_Product::where('cat_id',$cat_id)->search()->sort()->orderBy('seq','desc')->paginate(25);

        foreach($PerformanceProducts as $PerformanceProduct)
        {
            $PerformanceProduct->images = json_decode($PerformanceProduct->images)? json_decode($PerformanceProduct->images):array();
        }

        $this->index_shift($PerformanceProducts, LBA_Performance_Product::where('cat_id',$cat_id));
        return view('LBA.performance_product.index')->with(compact('PerformanceProducts','cat_id'));
    }

    public function create($cat_id)
    {
        return view('LBA.performance_product.create')->with(compact('cat_id'));
    }

    public function store(PerformanceProductRequest $request, $cat_id)
    {
        $PerformanceProduct = LBA_Performance_Product::create(
                array(
                        'seq' => LBA_Performance_Product::where('cat_id',$cat_id)->max('seq') + 1,
                        'cat_id' => $cat_id,
                        'images' => json_encode(uploadMultipleImage('images','lba-performance-product-images')),
                        'title' => $request->get('title'),
                        'description' => $request->get('description'),
                        'type_system' => $request->get('type_system',''),
                        'thickness' => $request->get('thickness',''),
                        'type' => $request->get('type',''),
                        // 'glass_thickness' => $request->get('glass_thickness',''),
                        // 'depth' => $request->get('depth',''),
                    )
            );

        return redirect()->route('lba.performance.product',$cat_id)->with('success','Record created.');
    }

    public function edit($cat_id, $id)
    {
        $PerformanceProduct = LBA_Performance_Product::find($id);
        $PerformanceProduct->images = json_decode($PerformanceProduct->images)? json_decode($PerformanceProduct->images):array();
        return view('LBA.performance_product.edit')->with(compact('PerformanceProduct','cat_id'));
    }

    public function update(PerformanceProductRequest $request, $cat_id, $id)
    {
        $PerformanceProduct = LBA_Performance_Product::find($id);
        $PerformanceProduct->images = (uploadMultipleImage('images')==[])?$PerformanceProduct->images:json_encode(uploadMultipleImage('images','lba-performance-product-images'));
        $PerformanceProduct->title = $request->get('title',$PerformanceProduct->title);
        $PerformanceProduct->description = $request->get('description',$PerformanceProduct->description);
        $PerformanceProduct->type_system = $request->get('type_system',$PerformanceProduct->type_system);
        $PerformanceProduct->thickness = $request->get('thickness',$PerformanceProduct->thickness);
        $PerformanceProduct->type = $request->get('type',$PerformanceProduct->type);
        // $PerformanceProduct->glass_thickness = $request->get('glass_thickness',$PerformanceProduct->glass_thickness);
        // $PerformanceProduct->depth = $request->get('depth',$PerformanceProduct->depth);
        $PerformanceProduct->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($cat_id, $id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Performance_Product::destroy($id);
    }

    public function shift($cat_id, $id, $shift_id)
    {
        $this->shifting(new LBA_Performance_Product, $id, $shift_id);

        return back();
    }
}
