<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
use BUP\Models\LBA\LBA_Password_Requester;

class LBA_PasswordRequesterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $PasswordRequesters = LBA_Password_Requester::search()->sort()->orderBy('created_at','desc')->paginate(25);
        return view('LBA.password_requester.index')->with(compact('PasswordRequesters'));
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return LBA_Password_Requester::destroy($id);
    }
}
