<?php

namespace BUP\Http\Controllers\LBA;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use BUP\Http\Requests;
use BUP\Http\Controllers\APIController;
use Validator, Response, Image;
use Mail;
use Carbon\Carbon;
use BUP\User;
use BUP\UserDevice;
use BUP\Models\LBA\LBA_Bazaar_Catalog;
use BUP\Models\LBA\LBA_Bazaar_Product;
use BUP\Models\LBA\LBA_Bazaar_Item;
use BUP\Models\LBA\LBA_Standard_Catalog;
use BUP\Models\LBA\LBA_Screen_Pop_Up;
use BUP\Models\LBA\LBA_About_Us;
use BUP\Models\LBA\LBA_Contact_Us;
use BUP\Models\LBA\LBA_Password_Page;
use BUP\Models\LBA\LBA_Project_References;
use BUP\Models\LBA\LBA_Performance_Catalog;
use BUP\Models\LBA\LBA_Alloy_Specification;
use BUP\Models\LBA\LBA_Password_Requester;

class LBA_APIController extends APIController
{
    public function index($api = 'pt')
    {
        $app = 'lba';
        $sample = $file_keys = $links = array();
        // $links[] = route($app.'.api',array('login'));
        // $links[] = route($app.'.api',array('logout'));
        $links[] = route($app.'.api',array('pt'));
        $links[] = route($app.'.api',array('screen_popup'));
        $links[] = route($app.'.api',array('bazaar'));
        $links[] = route($app.'.api',array('standard'));
        $links[] = route($app.'.api',array('performance'));
        $links[] = route($app.'.api',array('about_us'));
        $links[] = route($app.'.api',array('password'));
        $links[] = route($app.'.api',array('project_references'));
        $links[] = route($app.'.api',array('alloy_specification'));
        $links[] = route($app.'.api',array('get_password'));

        // $links[] = route($app.'.api',array('attendance_records'));
        // $links[] = route($app.'.api',array('students'));
        // $links[] = route($app.'.api',array('add_student'));
        // $links[] = route($app.'.api',array('edit_student'));
        // $links[] = route($app.'.api',array('delete_student'));
        // $links[] = route($app.'.api',array('scan_code'));
        // $links[] = route($app.'.api',array('check_in'));
        // $links[] = route($app.'.api',array('check_out'));
        // $links[] = route($app.'.api',array('check_out_all'));

        $api_token = session()->get('api_token','');

        switch (strtolower($api)) {
            // case 'login':
            //     $sample = array(
            //             'app' => strtoupper($app),
            //             'email' => 'admin@convep.com',
            //             'password' => 'ABC123abc',
            //             'imei' => 'IOS:123',
            //             'push_token' => 'push_token123',
            //             'width' => '100',
            //             'height' => '100',
            //             'os_version' => '1.0',
            //             'app_version' => '1.0',
            //         );
            //     break;
            // case 'logout':
            //     $sample = array(
            //             'app' => strtoupper($app),
            //             'api_token' => $api_token,
            //         );
            //     break;
            case 'pt':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'imei' => 'IOS:123',
                        'push_token' => 'push_token123',
                        'width' => '100',
                        'height' => '100',
                        'os_version' => '1.0',
                        'app_version' => '1.0',
                    );
                break;
            case 'screen_popup':
                $sample = array(
                        'app' => strtoupper($app),
                        'imei' => 'IOS:123',
                        
                    );
                break;

            case 'bazaar':
                $sample = array(
                        'app' => strtoupper($app),
                        'imei' => 'IOS:123',
                        
                    );
                break;
            case 'standard':
                $sample = array(
                        'app' => strtoupper($app),
                        'imei' => 'IOS:123',
                    );
                break;
            case 'performance':
                $sample = array(
                        'app' => strtoupper($app),
                        'imei' => 'IOS:123',
                    );
                break;
            case 'about_us':
                $sample = array(
                        'app' => strtoupper($app),
                        'imei' => 'IOS:123',
                    );
                break;
            case 'password':
                $sample = array(
                        'app' => strtoupper($app),
                        'imei' => 'IOS:123',
                        'password'=>'ABC123abc',
                    );
                break;
            case 'project_references':
                $sample = array(
                        'app' => strtoupper($app),
                        'imei' => 'IOS:123',
                    );
                break;   
            case 'alloy_specification':
                $sample = array(
                        'app' => strtoupper($app),
                        'imei' => 'IOS:123',
                    );
                break;     
            case 'get_password':
                $sample = array(
                        'app' => strtoupper($app),
                        'imei' => 'IOS:123',
                        'name' =>'',
                        'company' =>'',
                        'email' =>'',
                        'phone' =>'',
                    );
                break;      

            // case 'attendance_records':
            //     $sample = array(
            //             'app' => strtoupper($app),
            //             'api_token' => $api_token,
            //         );
            //     break;
            // case 'students':
            //     $sample = array(
            //             'app' => strtoupper($app),
            //             'api_token' => $api_token,
            //         );
            //     break;
            // case 'add_student':
            //     $file_keys = array('images');
            //     $sample = array(
            //             'app' => strtoupper($app),
            //             'api_token' => $api_token,
            //             'name' => 'Taylor Swift',
            //             'age' => '26',
            //             'contact_name_1' => 'James Swift',
            //             'contact_number_1' => '016-2323123',
            //             'contact_name_2' => 'Samantha Swift',
            //             'contact_number_2' => '05-23433123',
            //         );
            //     break;
            // case 'edit_student':
            //     $file_keys = array('images');
            //     $sample = array(
            //             'app' => strtoupper($app),
            //             'api_token' => $api_token,
            //             'student_id' => '1',
            //             'name' => 'Taylor Swift',
            //             'age' => '26',
            //             'contact_name_1' => 'James Swift',
            //             'contact_number_1' => '016-2323123',
            //             'contact_name_2' => 'Samantha Swift',
            //             'contact_number_2' => '05-23433123',
            //         );
            //     break;
            // case 'delete_student':
            //     $sample = array(
            //             'app' => strtoupper($app),
            //             'api_token' => $api_token,
            //             'student_id' => '1',
            //         );
            //     break;
            // case 'scan_code':
            //     $sample = array(
            //             'app' => strtoupper($app),
            //             'api_token' => $api_token,
            //             'qrcode' => '',
            //             'task' => 'check',
            //         );
            //     break;
            // case 'check_in':
            //     $sample = array(
            //             'app' => strtoupper($app),
            //             'api_token' => $api_token,
            //             'student_id' => '',
            //         );
            //     break;
            // case 'check_out':
            //     $sample = array(
            //             'app' => strtoupper($app),
            //             'api_token' => $api_token,
            //             'student_id' => '',
            //         );
            //     break;
            // case 'check_out_all':
            //     $sample = array(
            //             'app' => strtoupper($app),
            //             'api_token' => $api_token,
            //         );
            //     break;
        }
        return view('api.index')->with(compact('api','sample','file_keys','links','app'));
    }

    public function pt()
    {
        $datas = array();
        $status = array();
        if(isset($this->Request['api_token']) && $this->Request['api_token'] != '')
            $User = $this->getUserIDByApiToken($this->Request['api_token']);
        $os = explode(':', $this->Request['imei'])[0];
        $imei = explode(':', $this->Request['imei'])[1];
        $UserDevice = UserDevice::firstOrCreate(array(
            'user_id' => isset($User)? $User->id:0,
            'imei' => $imei,
            'os' => $os,
            'width' => trim($this->Request['width']),
            'height' => trim($this->Request['height']),
            'os_version' => trim($this->Request['os_version']),
            'app_version' => trim($this->Request['app_version']),
            'app' => trim($this->Request['app']),
        ));
        $UserDevice->push_token = isset($this->Request['push_token'])? trim($this->Request['push_token']):'';
        $UserDevice->save();

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function screen_popup()
    {
        $datas = array();
        $status = array();
        $datas = LBA_Screen_Pop_Up::all();
        

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function bazaar()
    {
        // $datas = array();
        // $datas2 = array();
        // $status = array();
        // $Field = array();
        // $Value = array();
        // $Values = array();
        // foreach(LBA_Bazaar_Catalog::all() as $i => $bazaar){
        //     $datas[$i]= $bazaar;
        //     $datas2[$i]= LBA_Bazaar_Product::where('cat_id',$bazaar->id)->get();
        //     foreach ($datas2[$i] as $value) {
        //         $Values = array();
        //         $datas2[$i]->bazaar_item = $value->bazaar_item;
        //         $Value = isset($datas2[$i]->bazaar_item->first()->values)? array_values(json_decode($datas2[$i]->bazaar_item->first()->values,true)):'';
        //         $Field = array_values(json_decode($value->fields,true));
        //         foreach ($Field as $key => $field) {
        //             // dd($Value);
        //             if(count($Value)>$key ){
        //                 $Values[]=[$field=> $Value[$key]];
        //             }

        //         }
        //         $datas2[$i]->bazaar_item->first()->values = $Values;

        //     }
        //      // dd($Value);
        //     $datas[$i]['bazaar_product']= $datas2;
        //     // dd(LBA_Bazaar_Product::find($i)->bazaar_item);
        //     // dd( $datas[$i]->bazaar_product->bazaar_item=LBA_Bazaar_Product::find($i)->bazaar_item);
        //     // $datas[$i-1]->bazaar_product->where('cat_id',$i);
        // }

        $datas = array();
        $status = array();

        $BazaarCatalogs = LBA_Bazaar_Catalog::all();
        foreach ($BazaarCatalogs as $BazaarCatalog)
        {
            $BazaarProducts = $BazaarCatalog->bazaar_product;
            foreach ($BazaarProducts as $BazaarProduct)
            {
                $BazaarProduct->fields = json_decode($BazaarProduct->fields)? json_decode($BazaarProduct->fields,true):array();
                $BazaarItems = $BazaarProduct->bazaar_item;
                foreach ($BazaarItems as $BazaarItem)
                {
                    $addzero=$BazaarItem->section_no;
                    if(strlen($BazaarItem->section_no)!=5){
                        for($i=1;$i<=(5-strlen($BazaarItem->section_no));$i++){
                            $addzero=substr_replace($addzero,'0',0,0);
                        }
                        $BazaarItem->section_no=$addzero;
                    }
                    $BazaarItem->values = json_decode($BazaarItem->values)? json_decode($BazaarItem->values,true):array();
                }
            }
        }
            
        $datas = $BazaarCatalogs;

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function standard()
    {
        $datas = array();

        $status = array();

        foreach(LBA_Standard_Catalog::all() as $i => $standard){
            $standard = LBA_Standard_Catalog::find($standard->id)->with(array('standard_product'=>function($q)
        {
            $q->where('cat_id',"!=","")->with(array('standard_sd_gallery_category'=>function($q)
            {
                $q->where('prod_id',"!=","")->with(array('standard_sd_gallery'=>function($q)
                {
                    $q->where('gallery_cat_id',"!=","");
                    foreach ($q as $key => $value) {
                        dd($value);
                        
                     }
                 }))->orderBy('seq','desc');
            },
            'standard_tsd_gallery'=>function($q)
            {
                $q->where('prod_id',"!=","");
            
            }))->orderBy('seq','desc');
        
        }))->orderBy('seq','desc')->get();
        
        $datas = $standard;
        }
        


        // foreach(LBA_Standard_Catalog::all() as $i => $standard){
        //     $datas[$i]= $standard;
        //     $datas[$i]->standard_product->where('cat_id',$i);
        // }
            
        
        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function performance()
    {
        $datas = array();
        $status = array();

        foreach(LBA_Performance_Catalog::all() as $i => $performance){
            $performance = LBA_Performance_Catalog::find($performance->id)->with(array('performance_product'=>function($q)
            {
                $q->where('cat_id',"!=","")->with(array('performance_sd_gallery_category'=>function($q)
                {
                    $q->where('prod_id',"!=","")->with(array('performance_sd_gallery'=>function($q)
                    {
                         $q->where('gallery_cat_id',"!=","");
                     }))->orderBy('seq','desc');
                },
                'performance_tsd_gallery'=>function($q)
                {
                    $q->where('prod_id',"!=","");
                
                },'performance_wc_gallery'=>function($q)
                {
                    $q->where('prod_id',"!=","");
                
            },'performance_acc_gallery'=>function($q)
                {
                    $q->where('prod_id',"!=","");
                
                }))->orderBy('seq','desc');
        
            }))->orderBy('seq','desc')->get();
        
            foreach ($performance as $Performance) {
                foreach ($Performance->performance_product as $performance_product) {
                    $performance_product->images = json_decode($performance_product->images,true);
                }            
            }   

            $datas = $performance;
        }

        // for($i=1;$i<=count(LBA_Performance_Catalog::all());$i++){
        //     $datas[]= LBA_Performance_Catalog::find($i);
        //     $datas[$i-1]->performance_product->where('cat_id',$i);
        // }
            
        
        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function about_us()
    {
        $datas = array();
        $status = array();
        $about=LBA_About_Us::find(1);
        $contact=LBA_Contact_Us::orderBy("seq","desc")->get();  
        // $email_label = json_decode($contact->contacts,true);
        $Contact = array();
        $type = array();
        $input = array();
        $tag = array();
        foreach ($contact as $key => $el) {
            $Contact[$key] = $el;
            // $type[] = json_decode($el->contacts);
            $Contact[$key]->contacts=json_decode($el->contacts);
            // $type[] = $el['c_types'];
            // $input[] = $el['c_inputs'];
            // $tag[] = $el['c_tags'];
        }
         // dd($Contact);

        // $tel_label = json_decode($contact->tel_label,true);
        // $tel = array();
        // foreach (array_values($tel_label) as $key => $el) {
        //     $tel[$el]=json_decode($contact->tel,true)['tel'.$key];
        // }
        $datas['About_Us']= $about;
        $datas['Contact_Us']=$Contact;

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function password()
    {
        $datas= array();
        $status = array();
        if(LBA_Password_Page::where('password',$this->Request['password'])->first()){

        }else{
            $status = array('status' => 'failed','message' => array('Invalid password.'));
        }

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function get_password()
    {
        $datas= array();
        $status = array();
        $Requester=LBA_Password_Requester::create(array(
        'name'=>trim($this->Request['name']),
        'company'=>trim($this->Request['company']),
        'email'=> trim($this->Request['email']),
        'phone'=>trim($this->Request['phone']),
        ));
       $datas = LBA_Password_Page::find(1);

       Mail::send('LBA.Email.index', ['password'=>$datas->password,'name'=>trim($this->Request['name'])], function ($message) use($Requester) {
            $message->from('support@convep.com');
            $message->to($Requester->email, $Requester->name)->subject('LBA' . ' - Get Password');
        });
        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function project_references()
    {
        $datas = array();
        $status = array();
        foreach (LBA_Project_References::all() as $key => $Project_Reference) {
            $datas[]=$Project_Reference;
        }

        return $this->response(__FUNCTION__,$status,$datas);
    }
    public function alloy_specification()
    {
        $datas= array();
        $status = array();
        foreach (LBA_Alloy_Specification::all() as $key => $Project_Reference) {
            $datas[]=$Project_Reference;
        }

        return $this->response(__FUNCTION__,$status,$datas);
    }
} 