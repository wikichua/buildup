<?php

namespace BUP\Http\Controllers;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Sample;

class SampleController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
     	$Samples = Sample::search()->sort()->orderBy('seq','desc')->paginate(25);
        return view('sample.index')->with(compact('Samples'));
    }

    public function create()
    {
        return view('sample.create');
    }

    public function store(Request $request)
    {
        $Sample = Sample::create(
                array(
                        'seq' => Sample::max('seq') + 1,
                        'content' => $request->get('content'),
                    )
            );

        return redirect()->route('sample')->with('success','Record created.');
    }

    public function edit($id)
    {
        $Sample = Sample::find($id);
        return view('sample.edit')->with(compact('Sample'));
    }

    public function update(Request $request, $id)
    {
        $Sample = Sample::find($id);
        $Sample->content = $request->get('content',$Sample->content);
        $Sample->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return Sample::destroy($id);
    }

    public function shift($id, $shift_id)
    {
        $this->shifting(new Sample, $id, $shift_id);

        return back();
    }
}
