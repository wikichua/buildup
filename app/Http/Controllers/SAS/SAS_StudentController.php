<?php

namespace BUP\Http\Controllers\SAS;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\SAS\SAS_Student;

class SAS_StudentController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->reorganizeSEQ(new SAS_Student);
     	$Students = SAS_Student::search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($Students, new SAS_Student);
        return view('SAS.student.index')->with(compact('Students'));
    }

    public function create()
    {
        return view('SAS.student.create');
    }

    public function store(Request $request)
    {
        $Student = SAS_Student::create(
                array(
                        'seq' => SAS_Student::max('seq') + 1,
                        'uuid' => uuid(),
                        'name' => $request->get('name'),
                        'photo' => uploadImage('photo','','sas-student-photo'),
                        'age' => $request->get('age','N/A'),
                        'contact_name_1' => $request->get('contact_name_1','N/A'),
                        'contact_number_1' => $request->get('contact_number_1','N/A'),
                        'contact_name_2' => $request->get('contact_name_2','N/A'),
                        'contact_number_2' => $request->get('contact_number_2','N/A'),
                    )
            );

        return redirect()->route('sas.student')->with('success','Record created.');
    }

    public function edit($id)
    {
        $Student = SAS_Student::find($id);
        return view('SAS.student.edit')->with(compact('Student'));
    }

    public function update(Request $request, $id)
    {
        $Student = SAS_Student::find($id);
        $Student->name = $request->get('name',$Student->name);
        $Student->photo = uploadImage('photo',$Student->photo,'sas-student-photo');
        $Student->age = $request->get('age',$Student->age);
        $Student->contact_name_1 = $request->get('contact_name_1',$Student->contact_name_1);
        $Student->contact_number_1 = $request->get('contact_number_1',$Student->contact_number_1);
        $Student->contact_name_2 = $request->get('contact_name_2',$Student->contact_name_2);
        $Student->contact_number_2 = $request->get('contact_number_2',$Student->contact_number_2);
        $Student->save();

        return back()->with('success','Record updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return SAS_Student::destroy($id);
    }

    public function shift($id, $shift_id)
    {
        $this->shifting(new SAS_Student, $id, $shift_id);

        return back();
    }
}
