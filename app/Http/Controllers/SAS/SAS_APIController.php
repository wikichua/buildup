<?php

namespace BUP\Http\Controllers\SAS;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use BUP\Http\Requests;
use BUP\Http\Controllers\APIController;
use Validator, Response, Image;
use Carbon\Carbon;
use BUP\User;
use BUP\UserDevice;
use BUP\Models\SAS\SAS_Student;
use BUP\Models\SAS\SAS_Attendance_Record;

class SAS_APIController extends APIController
{
    public function index($api = 'login')
    {
        $app = 'sas';
        $sample = $file_keys = $links = array();
        $links[] = route($app.'.api',array('login'));
        $links[] = route($app.'.api',array('logout'));
        $links[] = route($app.'.api',array('pt'));

        $links[] = route($app.'.api',array('attendance_records'));
        $links[] = route($app.'.api',array('students'));
        $links[] = route($app.'.api',array('add_student'));
        $links[] = route($app.'.api',array('edit_student'));
        $links[] = route($app.'.api',array('delete_student'));
        $links[] = route($app.'.api',array('scan_code'));
        $links[] = route($app.'.api',array('check_in'));
        $links[] = route($app.'.api',array('check_out'));
        $links[] = route($app.'.api',array('check_out_all'));

        $api_token = session()->get('api_token','');

        switch (strtolower($api)) {
            case 'login':
                $sample = array(
                        'app' => strtoupper($app),
                        'email' => 'admin@convep.com',
                        'password' => 'ABC123abc',
                        'imei' => 'IOS:123',
                        'push_token' => 'push_token123',
                        'width' => '100',
                        'height' => '100',
                        'os_version' => '1.0',
                        'app_version' => '1.0',
                    );
                break;
            case 'logout':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                    );
                break;
            case 'pt':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'imei' => 'IOS:123',
                        'push_token' => 'push_token123',
                        'width' => '100',
                        'height' => '100',
                        'os_version' => '1.0',
                        'app_version' => '1.0',
                    );
                break;
            case 'attendance_records':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                    );
                break;
            case 'students':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                    );
                break;
            case 'add_student':
                $file_keys = array('images');
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'name' => 'Taylor Swift',
                        'age' => '26',
                        'contact_name_1' => 'James Swift',
                        'contact_number_1' => '016-2323123',
                        'contact_name_2' => 'Samantha Swift',
                        'contact_number_2' => '05-23433123',
                    );
                break;
            case 'edit_student':
                $file_keys = array('images');
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'student_id' => '1',
                        'name' => 'Taylor Swift',
                        'age' => '26',
                        'contact_name_1' => 'James Swift',
                        'contact_number_1' => '016-2323123',
                        'contact_name_2' => 'Samantha Swift',
                        'contact_number_2' => '05-23433123',
                    );
                break;
            case 'delete_student':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'student_id' => '1',
                    );
                break;
            case 'scan_code':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'qrcode' => '',
                        'task' => 'check',
                    );
                break;
            case 'check_in':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'student_id' => '',
                    );
                break;
            case 'check_out':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                        'student_id' => '',
                    );
                break;
            case 'check_out_all':
                $sample = array(
                        'app' => strtoupper($app),
                        'api_token' => $api_token,
                    );
                break;
        }
        return view('api.index')->with(compact('api','sample','file_keys','links','app'));
    }

    public function attendance_records()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);

        $AttendanceRecords = SAS_Attendance_Record::whereBetween('created_at',array(date('Y-m-d 00:00:00'),date('Y-m-d 23:59:59')))->where('status','Active')->get();

        $datas = $AttendanceRecords;

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function students()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);

        $datas = SAS_Student::all();

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function add_student()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken(request()->input('api_token'));

         $SAS_Student = SAS_Student::Create(array(
                'seq'               => SAS_Student::max('seq') + 1,
                'uuid'              => uuid(),
                'name'              => trim(request()->input('name')),
                'photo'             => uploadImage('images','','sas-student-photo'),
                'age'               => trim(request()->input('age')),
                'contact_name_1'    => trim(request()->input('contact_name_1')),
                'contact_number_1'  => trim(request()->input('contact_number_1')),
                'contact_name_2'    => trim(request()->input('contact_name_2')),
                'contact_number_2'  => trim(request()->input('contact_number_2')),
            ));

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function edit_student()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken(request()->input('api_token'));

        $Student = SAS_Student::find(request()->input('student_id'));
        $Student->name = trim(request()->input('name'));
        $Student->photo = uploadImage('images',$Student->photo,'sas-student-photo');
        $Student->age = trim(request()->input('age'));
        $Student->contact_name_1 = trim(request()->input('contact_name_1'));
        $Student->contact_number_1 = trim(request()->input('contact_number_1'));
        $Student->contact_name_2 = trim(request()->input('contact_name_2'));
        $Student->contact_number_2 = trim(request()->input('contact_number_2'));
        $Student->save();

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function delete_student()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);

        SAS_Student::destroy($this->Request['student_id']);

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function scan_code()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);

        // dd($this->Request);
        // Log::info($this->Request);

        if($SAS_Student = SAS_Student::where('uuid','like','%'.trim($this->Request['qrcode']).'%')->first())
        {
            if($this->Request['task'] == strtolower('checkin'))
            {
                if(($SAS_Attendance_Record = SAS_Attendance_Record::where('student_id',$SAS_Student->id)->
                    whereBetween('created_at',array(date('Y-m-d 00:00:00'),date('Y-m-d 23:59:59')))->first()) == null)
                {
                    SAS_Attendance_Record::create(array(
                        'seq' => 0,
                        'student_id' => $SAS_Student->id,
                        'status' => 'Active',
                    ));
                }else{
                    if($SAS_Attendance_Record->status == 'Active') 
                    {
                        $status = array('status' => 'failed','message' => array('Student has checked in today.'));
                    }
                    else
                    {
                        $SAS_Attendance_Record->status = 'Active';
                        $SAS_Attendance_Record->save();
                    }
                }
                $datas = $SAS_Student;
            }
            elseif($this->Request['task'] == strtolower('check'))
            {
                $datas = $SAS_Student;
            }
        }else{
            $status = array('status' => 'failed','message' => array('Student record not found.'));
        }

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function check_in()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);

        if($SAS_Student = SAS_Student::find(trim($this->Request['student_id'])))
        {
            if(($SAS_Attendance_Record = SAS_Attendance_Record::where('student_id',$SAS_Student->id)->
                whereBetween('created_at',array(date('Y-m-d 00:00:00'),date('Y-m-d 23:59:59')))->first()) == null)
            {
                SAS_Attendance_Record::create(array(
                    'seq' => 0,
                    'student_id' => $SAS_Student->id,
                    'status' => 'Active',
                ));
            }else{
                if($SAS_Attendance_Record->status == 'Active') 
                {
                    $status = array('status' => 'failed','message' => array('Student has checked in today.'));
                }
                else
                {
                    $SAS_Attendance_Record->status = 'Active';
                    $SAS_Attendance_Record->save();
                }
            }
        }else{
            $status = array('status' => 'failed','message' => array('Student record not found.'));
        }

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function check_out()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);

        if($SAS_Student = SAS_Student::find(trim($this->Request['student_id'])))
        {
            if($SAS_Attendance_Record = SAS_Attendance_Record::where('student_id',$SAS_Student->id)->where('status','Active')
                ->whereBetween('created_at',array(date('Y-m-d 00:00:00'),date('Y-m-d 23:59:59')))->first())
            {
                $SAS_Attendance_Record->status = 'Inactive';
                $SAS_Attendance_Record->save();
            }else{
                $status = array('status' => 'failed','message' => array('Student has checked out.'));
            }
        }else{
            $status = array('status' => 'failed','message' => array('Student record not found.'));
        }

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function check_out_all()
    {
        $datas = array();
        $status = array();
        $User = $this->getUserIDByApiToken($this->Request['api_token']);

        $SAS_Attendance_Records = SAS_Attendance_Record::where('status','Active')
            ->whereBetween('created_at',array(date('Y-m-d 00:00:00'),date('Y-m-d 23:59:59')))->get();

        if($SAS_Attendance_Records->count() > 0)
        {
            foreach($SAS_Attendance_Records as $SAS_Attendance_Record)
            {
                $SAS_Attendance_Record->status = 'Inactive';
                $SAS_Attendance_Record->save();
            }
        }else{
            $status = array('status' => 'failed','message' => array('There are no students to check out today.'));
        }

        return $this->response(__FUNCTION__,$status,$datas);
    }
}