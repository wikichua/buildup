<?php

namespace BUP\Http\Controllers\SAS;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
use BUP\Http\Misc\ShiftingTrait;
use BUP\Models\SAS\SAS_Attendance_Record;

class SAS_AttendanceRecordController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
     	$Attendances = SAS_Attendance_Record::search()->sort()->orderBy('seq','desc')->paginate(25);
        return view('SAS.attendance.index')->with(compact('Attendances'));
    }
}
