<?php

namespace BUP\Http\Controllers;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
// use BUP\Http\Misc\ShiftingTrait;
// use BUP\Sample;

class LinkController extends Controller
{
    // use ShiftingTrait;

    public function __construct()
    {
        // $this->middleware('auth');
    }

    // public function index()
    // {
        // $this->reorganizeSEQ(new Sample);
     	// $Samples = Sample::search()->sort()->orderBy('seq','desc')->paginate(25);
        // $this->index_shift($Samples, new Sample);
        // return view('link.redirect');
    // }

    public function redirect($app)
    {
    	switch ($app) {
    		case 'mychampion':
    			$AND_link = 'https://play.google.com/store/apps/details?id=com.convep.mychampion&hl=en';
    			$IOS_link = 'https://itunes.apple.com/us/app/my-champion/id1169967708?mt=8';
    			break;
            case 'champ':
                $AND_link = 'https://play.google.com/store/apps/details?id=com.convep.mychampion&hl=en';
                $IOS_link = 'https://itunes.apple.com/us/app/my-champion/id1169967708?mt=8';
                break;
    	}
        return view('link.redirect')->with(compact('AND_link','IOS_link'));
    }

    // public function create()
    // {
    //     return view('sample.create');
    // }

    // public function store(Request $request)
    // {
    //     $Sample = Sample::create(
    //             array(
    //                     'seq' => Sample::max('seq') + 1,
    //                     'content' => $request->get('content'),
    //                 )
    //         );

    //     return redirect()->route('sample')->with('success','Record created.');
    // }

    // public function edit($id)
    // {
    //     $Sample = Sample::find($id);
    //     return view('sample.edit')->with(compact('Sample'));
    // }

    // public function update(Request $request, $id)
    // {
    //     $Sample = Sample::find($id);
    //     $Sample->content = $request->get('content',$Sample->content);
    //     $Sample->save();

    //     return back()->with('success','Record updated.');
    // }

    // public function destroy($id)
    // {
    //     session()->flash('success','Record deleted.');
    //     return Sample::destroy($id);
    // }

    // public function shift($id, $shift_id)
    // {
    //     $this->shifting(new Sample, $id, $shift_id);

    //     return back();
    // }
}
