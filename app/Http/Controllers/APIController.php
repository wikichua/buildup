<?php

namespace BUP\Http\Controllers;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
use Validator, Response, Image;
use Carbon\Carbon;
use BUP\User;
use BUP\UserDevice;

class APIController extends Controller
{
    public $Request, $Input;

    public function __construct(Request $Request)
    {
        $this->middleware('auth',array('only'=>array('index')));

        $this->Input = $Request;
        $this->Request = json_decode($Request->get('data'))? json_decode($Request->get('data'),true):array();
        if($Request->has('debug') && $Request->get('debug') == 1)
        {
            dd($Request->all());
        }
    }

    public function validation($Request)
    {
        $Validator = Validator::make($this->Request,$Request->rules(),$Request->messages());
        if($Validator->fails())
        {
            return array(
                    'status' => 'failed',
                    'message' => $Validator->errors()->all(),
                );
        }else{
            return true;
        }
    }

    public function response($module,$status = array(),$datas = array())
    {
        $datas = json_decode(json_encode($datas),true);
        $return = ['AppData' => [
            'module'           => $module,
            'settingIndex'     => date('Y-m-d H:i:s'),
            'portal_url'       => route('dashboard'),
            'download_url'     => route('dashboard'),
            'web_url'          => route('dashboard'),
            'totalRecords'     => count($datas),
            'last_updated_at'  => count($datas)? $this->get_last_updated_at($datas):date('Y-m-d H:i:s'),
            'version'          => '',
            'status'           => isset($status['status'])? $status['status']:'success',
            'message'          => isset($status['message'])? $status['message']:'',
            ]
        ];

        $return['Data'] = array();
        if(count($datas) > 0)
        {
            $this->resize_generate_url_for_images_or_files($datas);
            $return['Data'] = $datas;
        }

        return Response::make(str_replace(': null', ': ""', json_encode($return, JSON_PRETTY_PRINT)))->header('Content-Type', "application/json");
    }

    public function get_last_updated_at($datas)
    {
        $last_updated_at = array();

        foreach ($datas as $key => $value) {
            if(is_array($value))
            {
                $last_updated_at[] = $this->get_last_updated_at($datas[$key]);
            }else{
                if ($key == 'updated_at') {
                    $last_updated_at[] = $value;
                }
            }
        }

        if(count($last_updated_at))
        {
            $time = max(array_map('strtotime', $last_updated_at));
            if($time)
                return date("Y-m-d H:i:s",$time);
        }else{
            return 0;
        }
    }

    public function resize_generate_url_for_images_or_files(&$datas)
    {
        foreach ($datas as $key => $value) {
            if(is_array($value))
            {
                $this->resize_generate_url_for_images_or_files($datas[$key]);
            }else{
                if(strlen($value) <= 255){
                    if (preg_match('/([^\/]+)(?=\.\w+$)?([^\/]+?(\.gif|\.png|\.jpg|\.jpeg|\.bmp))/i',$value) && file_exists(public_path().'/uploads/'.$value)) {
                        // $Image = Image::make(public_path().'/uploads/'.$value);
                        // $actual_width = $Image->width();
                        // $actual_height = $Image->height();
                        $datas[$key] = config('app.url').'/uploads/'.$value;
                        // $datas[$key] = 'http://convep.asuscomm.com:8000/buildup/public/uploads/'.$value; //temp remote url
                        // $datas[$key.'_width'] = $actual_width;
                        // $datas[$key.'_height'] = $actual_height;

                        // $width = isset($this->Request['width'])? $this->Request['width']: 500;
                        // $height = isset($this->Request['height'])? $this->Request['height']: 500;

                        // $resized_value = str_replace(basename($value),'',$value).'resized_'.$width.'X'.$height.'_'.basename($value);
                        // $datas[$key.'_resized'] = config('app.url').'/uploads/'.$resized_value;                        
                        // if(!file_exists(public_path().'/uploads/'.$resized_value))
                        // {
                        //     $Image->resize($width, $height, function ($constraint) {
                        //         $constraint->aspectRatio();
                        //         $constraint->upsize();
                        //     })->save(public_path().'/uploads/'.$resized_value);
                        // }

                        // $thumbnail_value = str_replace(basename($value),'',$value).'thumbnail_200X200_'.basename($value);
                        // $datas[$key.'_thumbnail'] = config('app.url').'/uploads/'.$thumbnail_value;
                        // if(!file_exists(public_path().'/uploads/'.$thumbnail_value))
                        // {
                        //     $Image->resize(200, 200, function ($constraint) {
                        //         $constraint->aspectRatio();
                        //         $constraint->upsize();
                        //     })->save(public_path().'/uploads/'.$thumbnail_value);
                        // }
                    }elseif (preg_match('/([^\/]+)(?=\.\w+$)?([^\/]+?(\.pdf|\.docs|\.xls|\.ppt|\.zip))/i',$value) && file_exists(public_path().'/uploads/'.$value)) {
                        $datas[$key] = config('app.url').'/uploads/'.$value;
                        // $datas[$key] = 'http://convep.asuscomm.com:8000/buildup/public/uploads/'.$value; //temp remote url
                    }
                }
            }
        }
    }

    public function getUserIDByApiToken($api_token='')
    {
        $UserDevice = UserDevice::where('api_token',$api_token)->first();

        if(!$UserDevice){
            throw new \Exception("Error Processing Request", 403);
        }
        
        return $UserDevice->User;
    }

    public function login()
    {
        $status = $this->validation(new \BUP\Http\Requests\AuthRequest);
        $datas = array();
        if ($status === true) {
            if (auth()->once(['email' => $this->Request['email'],'password' => $this->Request['password'], 'status' => 'Active','app' => $this->Request['app']]))
            {
                $User = User::where('email',$this->Request['email'])->where('status','Active')->first();
                $user_id = $User->id;
                $User->save();
                $User->api_token = uuid();
                $datas = $User;
                $status = array();

                session()->put('api_token',$User->api_token);

                $os = explode(':', $this->Request['imei'])[0];
                $imei = explode(':', $this->Request['imei'])[1];
                $UserDevice = UserDevice::firstOrCreate(array(
                    'user_id' => $user_id,
                    'imei' => $imei,
                    'os' => $os,
                    'width' => trim($this->Request['width']),
                    'height' => trim($this->Request['height']),
                    'os_version' => trim($this->Request['os_version']),
                    'app_version' => trim($this->Request['app_version']),
                    'app' => trim($this->Request['app']),
                ));
                $UserDevice->push_token = trim($this->Request['push_token']);
                $UserDevice->api_token = trim($User->api_token);
                $UserDevice->save();
            }else{
                $status = array('status' => 'failed','message' => array('Invalid login credential.'));
            }
        }

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function pt()
    {
        $datas = array();
        $status = array();
        if(isset($this->Request['api_token']) && $this->Request['api_token'] != '')
            $User = $this->getUserIDByApiToken($this->Request['api_token']);
        $os = explode(':', $this->Request['imei'])[0];
        $imei = explode(':', $this->Request['imei'])[1];
        $UserDevice = UserDevice::firstOrCreate(array(
            'user_id' => isset($User)? $User->id:0,
            'imei' => $imei,
            'os' => $os,
            'width' => trim($this->Request['width']),
            'height' => trim($this->Request['height']),
            'os_version' => trim($this->Request['os_version']),
            'app_version' => trim($this->Request['app_version']),
            'app' => trim($this->Request['app']),
        ));
        $UserDevice->push_token = isset($this->Request['push_token'])? trim($this->Request['push_token']):'';
        $UserDevice->save();

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function logout()
    {
        $datas = array();
        $status = array();

        UserDevice::where('api_token',$this->Request['api_token'])->where('app',$this->Request['app'])->forceDelete();

        return $this->response(__FUNCTION__,$status,$datas);
    }
}