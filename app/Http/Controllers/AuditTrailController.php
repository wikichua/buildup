<?php

namespace BUP\Http\Controllers;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\ACLRequest;
use BUP\Http\Controllers\Controller;
use BUP\AuditTrail;

class AuditTrailController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index()
    {
        $AuditTrails = AuditTrail::search()->sort()->orderBy('created_at','desc')->paginate(25);
        return view('audittrail.index')->with(compact('AuditTrails'));
    }

    public function show($id)
    {
        $AuditTrail = AuditTrail::find($id);
        
        return view('audittrail.show')->with(compact('AuditTrail'));
    }
}
