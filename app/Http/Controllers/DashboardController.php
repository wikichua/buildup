<?php

namespace BUP\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

use BUP\Http\Requests;
use BUP\Http\Controllers\Controller;
use LaravelAnalytics;
use Carbon\Carbon;

class DashboardController extends Controller
{
    function __construct() {
    	$this->middleware('auth');
    }
    
    public function index()
    {
        return view('dashboard');
    }
}
