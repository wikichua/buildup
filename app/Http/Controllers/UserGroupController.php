<?php

namespace BUP\Http\Controllers;

use Illuminate\Http\Request;

use BUP\Http\Requests;
use BUP\Http\Requests\UserGroupRequest;
use BUP\Http\Controllers\Controller;
use BUP\UserGroup;

class UserGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $UserGroups = UserGroup::where('id','!=',1)->search()->sort()->paginate(25);
        
        return view('usergroup.index')->with(compact('UserGroups'));
    }

    public function create()
    {
        return view('usergroup.create');
    }

    public function store(UserGroupRequest $request)
    {
        $UserGroup = UserGroup::create(
                array(
                        'name' => $request->get('name'),
                    )
            );

        return redirect()->route('acl.create',array($UserGroup->id))->with('success','Record created. Please create a Access Control Level for '.$request->get('name').'.');
    }

    public function edit($id)
    {
        $UserGroup = UserGroup::find($id);
        return view('usergroup.edit')->with(compact('UserGroup'));
    }

    public function update(UserGroupRequest $request, $id)
    {
        $UserGroup = UserGroup::find($id);
        $UserGroup->name = $request->get('name',$UserGroup->name);
        $UserGroup->save();

        return back()->with('success','Record Updated.');
    }

    public function destroy($id)
    {
        return UserGroup::destroy($id);
    }
}
