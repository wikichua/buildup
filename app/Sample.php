<?php

namespace BUP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use BUP\Http\Misc\SortableTrait;
use BUP\Http\Misc\SearchableTrait;
use BUP\Http\Misc\AuditTrailTrait;

class Sample extends Model
{
	use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'samples';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

}
