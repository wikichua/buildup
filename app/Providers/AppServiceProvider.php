<?php

namespace BUP\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->instance('log', new \Illuminate\Log\Writer(
                (new \Monolog\Logger(
                    $this->app->environment()
                ))->pushHandler(new \Monolog\Handler\StreamHandler(env('LOG_PATH',storage_path('logs/laravel.log'))))
            )
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
