Guys, i created a cms starter kit with laravel 5.2

features:
	
	helper handling upload,
	
	push notification - queue oriented
	
	ui for date time picker, wysiwyg and select2
	
	ACL, User and Usergroup scaftfolding
	
	sample of migration and crud views
	
	API handling + tester (similar to C2C)
	
	sample of unit test

in future, any development which is small or REQUIRED host on client side, we use this to speed up our work.
from time to time, i will upgrade this, so please create a specific routes and include in the RouteServiceProvider