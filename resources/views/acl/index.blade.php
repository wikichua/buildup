@extends('template')

@section('title')
    Access Control
@stop

@section('page-header')
    Access Control
@stop

@section('body')

<div class="panel-container">
    <div class="panel-heading">
        <div class="panel-heading-left">
            <a href="{{ route('usergroup') }}"><i class="fa fa-arrow-left"></i></a>
            <div class="heading-title">
                <h4>Access Control List</h4>
            </div>
        </div>
        <div class="panel-heading-right">
            @if (ACLButtonCheck('USR_ACL','Create'))
                <span>{!! action_update_button(route('acl.create',$usergroup_id)) !!}</span>
            @endif
        </div>
    </div>
    <div class="panel-body">
        <div class="basic-table-container">
            <table class="table table-responsive table-hover">
                <thead>
        			<tr>
        				<th>{!! sortTableHeaderSnippet('Module','module') !!}</th>
        				<th>{!! sortTableHeaderSnippet('Permission','permission') !!}</th>
        			</tr>
                </thead>
                <tbody>
        			@foreach ($ACLs as $ACL)
        			<tr>
        				<td>{{ $modules[$ACL->module] }}</td>
        				<td>{{ count(json_decode($ACL->permission,true))? implode(', ', json_decode($ACL->permission,true)):'No Access' }}</td>
        			</tr>
        			@endforeach
                </tbody>
			</table>
        </div>
    </div>
</div>

<div class="text-center">
    {!! str_replace('/?', '?', $ACLs->appends(request()->all())->render()) !!}
</div>

@stop