@extends('template')

@section('title')
    Access Control
@stop

@section('page-header')
	Access Control
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('acl',$usergroup_id) }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Modify Access Control</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>array('acl.store',$usergroup_id),'name'=>'form','class'=>'form-horizontal','method'=>'post')) !!}
        <div class="form-group">
		@foreach ($modules as $module_key => $module_name)
			<fieldset class="col-sm-4" style="height:250px;">
				<legend style="font-size:1em; color:#7BE2AE;">{{ $module_name }}</legend>
					<div class="checkbox"><label>{!! Form::checkbox('select_all','','',array('class'=>'checkAll','data-modulekey'=>$module_key)) !!} Select All/Unselect All</label></div>
					@foreach ($roles[$module_key] as $permission)
						<div class="checkbox"><label>{!! Form::checkbox($module_key.'[]', $permission, (isset($acl[$module_key]) && in_array($permission,$acl[$module_key])? true:false),array('class'=>'checkItem-'.$module_key)) !!} {{ $permission }}</label></div>
					@endforeach
			</fieldset>
		@endforeach
		</div>
		<div class="form-group">
			<div class="col-sm-12 text-center">
				<a href="{{ route('acl',$usergroup_id) }}" class='btn btn-back'>Back</a>
				{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
			</div>
		</div>
		{!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	$(".checkAll").change(function () {
		var $moduleKey = $(this).data('modulekey');
	    $(".checkItem-"+$moduleKey+":checkbox").prop('checked', $(this).prop("checked"));
	});
});
</script>
@stop