@extends('template')

@section('title')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left"><h4>{{ config('cms.modules.LBA_PASS_RQ') }}</h4></div>
		<div class="panel-heading-right">
			<button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button>
		</div>
	</div>
	<div class="panel-body">
		<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						<th>{!! sortTableHeaderSnippet('Name','name') !!}</th>
						<th>{!! sortTableHeaderSnippet('Company','company') !!}</th>
						<th>{!! sortTableHeaderSnippet('Email','email') !!}</th>
						<th>{!! sortTableHeaderSnippet('Phone','phone') !!}</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($PasswordRequesters as $PasswordRequester)
					<tr>
						<td>{{ $PasswordRequester->name }}</td>
						<td>{{ $PasswordRequester->company }}</td>
						<td>{{ $PasswordRequester->email }}</td>
						<td>{{ $PasswordRequester->phone }}</td>
						<td class="table-btn-action text-center">
							{{-- @if (ACLButtonCheck('LBA_PASS_RQ','Delete'))
							<a class="btn btn-basic delete" data-href="{{ route('lba.password.page.destroy',array($PasswordRequester->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif --}}
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::label('name', 'Name') !!}
					{!! searchTableHeaderSnippet('name') !!}
				</div>
				<div class="form-group">
					{!! Form::label('company', 'Company') !!}
					{!! searchTableHeaderSnippet('company') !!}
				</div>
				<div class="form-group">
					{!! Form::label('email', 'Email') !!}
					{!! searchTableHeaderSnippet('email') !!}
				</div>
				<div class="form-group">
					{!! Form::label('phone', 'Phone') !!}
					{!! searchTableHeaderSnippet('phone') !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $PasswordRequesters->appends(request()->all())->render()) !!}
</div>	

@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop