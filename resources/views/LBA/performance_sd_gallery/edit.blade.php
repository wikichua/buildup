@extends('template')

@section('name')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('lba.performance.sd.gallery',array($cat_id, $prod_id, $gallery_cat_id)) }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Create {{ config('cms.modules.LBA_PFM_SD') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>array('lba.performance.sd.gallery.update', $cat_id, $prod_id, $gallery_cat_id, $PerformanceSDGallery->id),'name'=>'register_form','class'=>'form-horizontal','method'=>'put','files'=>true)) !!}
	        <div class="form-group">
				{!! Form::label('image', 'Image', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<div class="col-sm-12 imgForm-container">
						<div class="imgBrowse-img-frame object-fit-contain">
							<img id="previewHere" src="{{ asset(imgTagShow($PerformanceSDGallery->image,'default')) }}">
						</div>
						<div class="imgBrowse-browse-frame">
							{!! Form::file('image', array('class'=>"form-control", 'id'=>'img-browse', 'style'=>'display:none')) !!}
							<button type="button" class="btn btn-basic" onclick="javascript: $('#img-browse').trigger('click');">
								<i class='fa fa-lg fa-camera' data-toggle="tooltip" title="Browse image"></i>
							</button>
							{!! Form::text('', '', array('class'=>"form-control", 'readonly'=>"readonly")) !!}
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('section_no', 'Section No.', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('section_no', $PerformanceSDGallery->section_no, array('class'=>"form-control",'placeholder'=>"eg. 1287")) !!}
				</div>
			</div>  
			{{-- <div class="form-group">
				{!! Form::label('title', 'Title', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('title', old('title',$PerformanceSDGallery->title), array('class'=>"form-control",'placeholder'=>"eg. Title")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('description', 'Description', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::textarea('description', old('description',$PerformanceSDGallery->description), array('class'=>"form-control",'placeholder'=>"eg. This is a description box.",'rows'=>3)) !!}
				</div>
			</div>    --}}
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					<a href="{{ route('lba.performance.sd.gallery',array($cat_id, $prod_id, $gallery_cat_id)) }}" class='btn btn-back'>Back</a>
					{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
				</div>
			</div>
		{!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
});
</script>
@stop