@extends('template')

@section('title')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('lba.contact.us') }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Edit {{ config('cms.modules.LBA_CTC_US') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>array('lba.contact.us.update', $Contact->id) ,'name'=>'register_form','class'=>'form-horizontal','method'=>'put')) !!}
			<div class="form-group">
				{!! Form::label('title', 'Name', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('title', old('title', $Contact->title), array('class'=>"form-control",'placeholder'=>"eg. Headquarters")) !!}
				</div>
			</div> 
			<div class="form-group">
				{!! Form::label('address', 'Address', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::textarea('address', old('address', $Contact->address), array('class'=>"form-control",'placeholder'=>"eg. 111, Jalan HQ, Taman HQ, Selangor",'rows'=>3)) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('latitude', 'Latitude', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('latitude', old('latitude', $Contact->latitude), array('class'=>"form-control",'placeholder'=>"eg. 1.112233")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('longitude', 'Longitude', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('longitude', old('longitude', $Contact->longitude), array('class'=>"form-control",'placeholder'=>"eg. 2.223344")) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('contacts', 'Contact Methods', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<div id='contacts-row'>
						@foreach ($Contact->contacts as $contact)
						<div class="row-template">
							<div class="row">
								<div class="col-md-12">
									<span class="row-template-label">Method 1</span>
									<a href="#" class="delete-row-template hidden pull-right" data-toggle='tooltip' title='Remove'><i class="fa fa-times"></i></a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									{!! Form::select('c_types[]', config('cms.dropdown.contact_types'),$contact->c_types, array('class'=>"form-control", 'placeholder'=>"Select an option")) !!}
								</div>
								<div class="col-md-8">
									<div class="input-group">
										<span class="input-group-addon">Tag</span>
										{!! Form::text('c_tags[]',$contact->c_tags, array('class'=>"form-control", 'placeholder'=>"eg. General")) !!}
									</div>
								</div>
							</div>
							<div class="input-group">
								<span class="input-group-addon">Input</span>
								{!! Form::text('c_inputs[]', $contact->c_inputs, array('class'=>"form-control", 'placeholder'=>"eg. 016-1122335")) !!}
							</div>
						</div>
						@endforeach
					</div>
					<button type="button" class="btn" id='addContact'>Add contact</button>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					<a href="{{ route('lba.contact.us') }}" class='btn btn-back'>Back</a>
					{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
				</div>
			</div>
		{!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script id="contacts-row-template" type="text/x-handlebars-template">
	<div class="row-template">
		<div class="row">
			<div class="col-md-12">
				<span class="row-template-label">Method 1</span>
				<a href="#" class="delete-row-template pull-right" data-toggle='tooltip' title='Remove'><i class="fa fa-times"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				{!! Form::select('c_types[]', config('cms.dropdown.contact_types'),'', array('class'=>"form-control", 'placeholder'=>"Select an option")) !!}
			</div>
			<div class="col-md-8">
				<div class="input-group">
					<span class="input-group-addon">Tag</span>
					{!! Form::text('c_tags[]','', array('class'=>"form-control", 'placeholder'=>"eg. General")) !!}
				</div>
			</div>
		</div>
		<div class="input-group">
			<span class="input-group-addon">Input</span>
			{!! Form::text('c_inputs[]', '', array('class'=>"form-control", 'placeholder'=>"eg. 016-1122335")) !!}
		</div>
	</div>
</script>

<script type="text/javascript" src="{{ asset('assets/js/handlebars-v4.0.5.js') }}"></script>
<script type="text/javascript">
	$(function() {
		function reorganizeRowLabel() {
		    $('.row-template-label').each(function(index, element) {
		    	this.innerText = 'Method' + ' ' + (index + 1);
		    });
		}

		function hideShowDeleteRowBtn() {
			var delBtn = $('.delete-row-template');
			var btnCount = delBtn.length;
			if (!(btnCount > 1)) {
				delBtn.addClass('hidden');
			} else {
				delBtn.removeClass('hidden');
			}
		}

		$('#addContact').on('click', function(e) {
			e.preventDefault();
			var source = $("#contacts-row-template").html();
			var template = Handlebars.compile(source);
			var html = template();
			$('#contacts-row').append(html);
			reorganizeRowLabel();
			hideShowDeleteRowBtn();
		});

		$(document).on('click','.delete-row-template',function(event) {
	        event.preventDefault();
	        var $self = $(this);
	        alertify.confirm('Remove row?').set('onok', function(closeEvent){
	            if($self.attr('href') != '#')
	            {
	                $.ajax({
	                    url: $self.data('href'),
	                    type: 'DELETE',
	                    success: function(result){
	                        $self.closest('.row-template').remove();
	                        alertify.success('Row removed.');
	                        reorganizeRowLabel();
	                    }
	                });
	            }else{
	                $self.closest('.row-template').remove();
	                alertify.success('Row removed.');
	                reorganizeRowLabel();
	                hideShowDeleteRowBtn();
	            }
	        });
	    });
	    
	    reorganizeRowLabel();
	    hideShowDeleteRowBtn();
	});
</script>
@stop