@extends('template')

@section('title')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('lba.contact.us') }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Create {{ config('cms.modules.LBA_CTC_US') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>'lba.contact.us.store','name'=>'register_form','class'=>'form-horizontal','method'=>'post')) !!}
			<div class="form-group">
				{!! Form::label('title', 'Name', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('title', old('title'), array('class'=>"form-control",'placeholder'=>"eg. Headquarters")) !!}
				</div>
			</div> 
			<div class="form-group">
				{!! Form::label('address', 'Address', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::textarea('address', old('address'), array('class'=>"form-control",'placeholder'=>"eg. 111, Jalan HQ, Taman HQ, Selangor",'rows'=>3)) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('latitude', 'Latitude', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('latitude', old('latitude'), array('class'=>"form-control",'placeholder'=>"eg. 1.112233")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('longitude', 'Longitude', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('longitude', old('longitude'), array('class'=>"form-control",'placeholder'=>"eg. 2.223344")) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('contacts', 'Contact Methods', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<div id='contacts-row'>
						<div class="row-template">
							<div class="row">
								<div class="col-md-12">
									<span class="row-template-label">Method 1</span>
									<a href="#" class="delete-row-template hidden pull-right" data-toggle='tooltip' title='Remove'><i class="fa fa-times"></i></a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									{!! Form::select('c_types[]', config('cms.dropdown.contact_types'),'', array('class'=>"form-control", 'placeholder'=>"Select an option")) !!}
								</div>
								<div class="col-md-8">
									<div class="input-group">
										<span class="input-group-addon">Tag</span>
										{!! Form::text('c_tags[]','', array('class'=>"form-control", 'placeholder'=>"eg. General")) !!}
									</div>
								</div>
							</div>
							<div class="input-group">
								<span class="input-group-addon">Input</span>
								{!! Form::text('c_inputs[]', '', array('class'=>"form-control", 'placeholder'=>"eg. 016-1122335")) !!}
							</div>
						</div>
					</div>
					<button type="button" class="btn" id='addContact'>Add contact</button>
				</div>
			</div>

			{{-- <div class="form-group">
				{!! Form::label('email_label0', 'Email Label 1', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('email_label0', old('email_label'), array('class'=>"form-control",'placeholder'=>"Email Label")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('email0', 'Email 1', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::email('email0', old('email'), array('class'=>"form-control",'placeholder'=>"Email")) !!}
					
				</div>
			</div> 

			<div class="email1"hidden="">
				<div class="form-group " >
					{!! Form::label('email_label1', 'Email Label 2', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('email_label1', old('email_label'), array('class'=>"form-control",'placeholder'=>"Email Label")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('email1', 'Email 2', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::email('email1', old('email'), array('class'=>"form-control",'placeholder'=>"Email")) !!}
					</div>
				</div>
			</div>

			<div class="email2"hidden="">
				<div class="form-group " >
					{!! Form::label('email_label2', 'Email Label 3', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('email_label2', old('email_label'), array('class'=>"form-control",'placeholder'=>"Email Label")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('email2', 'Email 3', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::email('email2', old('email'), array('class'=>"form-control",'placeholder'=>"Email")) !!}
					</div>
				</div>
			</div>

			<div class="email3"hidden="">
				<div class="form-group " >
					{!! Form::label('email_label3', 'Email Label 4', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('email_label3', old('email_label'), array('class'=>"form-control",'placeholder'=>"Email Label")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('email3', 'Email 4', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::email('email3', old('email'), array('class'=>"form-control",'placeholder'=>"Email")) !!}
					</div>
				</div>
			</div>

			<div class="email4"hidden="">
				<div class="form-group " >
					{!! Form::label('email_label4', 'Email Label 5', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('email_label4', old('email_label'), array('class'=>"form-control",'placeholder'=>"Email Label")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('email4', 'Email 5', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::email('email4', old('email'), array('class'=>"form-control",'placeholder'=>"Email")) !!}
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="col-sm-10"></div>
				<div class="col-sm-1">
				<div class="email_minus" hidden="">
					<a class="btn btn-danger "><i class="fa fa-minus"></i></a>
				</div>
				<a class="btn btn-info email_add" ><i class="fa fa-plus"></i></a>

				</div>
			</div>

			<div class="form-group">
				{!! Form::label('tel_label0', 'Tel Label 1', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('tel_label0', old('tel_label'), array('class'=>"form-control",'placeholder'=>"Tel Label")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('tel0', 'Tel 1', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('tel0', old('tel'), array('class'=>"form-control",'placeholder'=>"Tel")) !!}
				</div>
			</div>

			<div class="tel1"hidden="">
				<div class="form-group " >
					{!! Form::label('tel_label1', 'Tel Label 2', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('tel_label1', old('tel_label'), array('class'=>"form-control",'placeholder'=>"Tel Label")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('tel1', 'Tel 2', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('tel1', old('tel'), array('class'=>"form-control",'placeholder'=>"Tel")) !!}
					</div>
				</div>
			</div>

			<div class="tel2"hidden="">
				<div class="form-group " >
					{!! Form::label('tel_label2', 'Tel Label 3', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('tel_label2', old('tel_label'), array('class'=>"form-control",'placeholder'=>"Tel Label")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('tel2', 'Tel 3', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('tel2', old('tel'), array('class'=>"form-control",'placeholder'=>"Tel")) !!}
					</div>
				</div>
			</div>

			<div class="tel3"hidden="">
				<div class="form-group " >
					{!! Form::label('tel_label3', 'Tel Label 4', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('tel_label3', old('tel_label'), array('class'=>"form-control",'placeholder'=>"Tel Label")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('tel3', 'Tel 4', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('tel3', old('tel'), array('class'=>"form-control",'placeholder'=>"Tel")) !!}
					</div>
				</div>
			</div>

			<div class="tel4"hidden="">
				<div class="form-group " >
					{!! Form::label('tel_label4', 'Tel Label 5', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('tel_label4', old('tel_label'), array('class'=>"form-control",'placeholder'=>"Tel Label")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('tel4', 'Tel 5', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('tel4', old('tel'), array('class'=>"form-control",'placeholder'=>"Tel")) !!}
					</div>
				</div>
			</div>

			<div class="form-group">
			<div class="col-sm-10"></div>
				<div class="col-sm-1">
				<div class="tel_minus" hidden="">
					<a class="btn btn-danger "><i class="fa fa-minus"></i></a>
				</div>
				<a class="btn btn-info tel_add" ><i class="fa fa-plus"></i></a>

				</div>
			</div> --}}

			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					<a href="{{ route('lba.contact.us') }}" class='btn btn-back'>Back</a>
					{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
				</div>
			</div>
		{!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script id="contacts-row-template" type="text/x-handlebars-template">
	<div class="row-template">
		<div class="row">
			<div class="col-md-12">
				<span class="row-template-label">Method 1</span>
				<a href="#" class="delete-row-template pull-right" data-toggle='tooltip' title='Remove'><i class="fa fa-times"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				{!! Form::select('c_types[]', config('cms.dropdown.contact_types'),'', array('class'=>"form-control", 'placeholder'=>"Select an option")) !!}
			</div>
			<div class="col-md-8">
				<div class="input-group">
					<span class="input-group-addon">Tag</span>
					{!! Form::text('c_tags[]','', array('class'=>"form-control", 'placeholder'=>"eg. General")) !!}
				</div>
			</div>
		</div>
		<div class="input-group">
			<span class="input-group-addon">Input</span>
			{!! Form::text('c_inputs[]', '', array('class'=>"form-control", 'placeholder'=>"eg. 016-1122335")) !!}
		</div>
	</div>
</script>

<script type="text/javascript" src="{{ asset('assets/js/handlebars-v4.0.5.js') }}"></script>
<script type="text/javascript">
	$(function() {
		function reorganizeRowLabel() {
		    $('.row-template-label').each(function(index, element) {
		    	this.innerText = 'Method' + ' ' + (index + 1);
		    });
		}

		function hideShowDeleteRowBtn() {
			var delBtn = $('.delete-row-template');
			var btnCount = delBtn.length;
			if (!(btnCount > 1)) {
				delBtn.addClass('hidden');
			} else {
				delBtn.removeClass('hidden');
			}
		}

		$('#addContact').on('click', function(e) {
			e.preventDefault();
			var source = $("#contacts-row-template").html();
			var template = Handlebars.compile(source);
			var html = template();
			$('#contacts-row').append(html);
			reorganizeRowLabel();
			hideShowDeleteRowBtn();
		});

		$(document).on('click','.delete-row-template',function(event) {
	        event.preventDefault();
	        var $self = $(this);
	        alertify.confirm('Remove row?').set('onok', function(closeEvent){
	            if($self.attr('href') != '#')
	            {
	                $.ajax({
	                    url: $self.data('href'),
	                    type: 'DELETE',
	                    success: function(result){
	                        $self.closest('.row-template').remove();
	                        alertify.success('Row removed.');
	                        reorganizeRowLabel();
	                    }
	                });
	            }else{
	                $self.closest('.row-template').remove();
	                alertify.success('Row removed.');
	                reorganizeRowLabel();
	                hideShowDeleteRowBtn();
	            }
	        });
	    });
	    
	    reorganizeRowLabel();
	    hideShowDeleteRowBtn();
	});
</script>
@stop

{{-- @section('scripts')
<script>
$(function(){

	// var email_no=0;
	// var tel_no=0;
	// $('.email_add').on('click',function(){
	// 	email_no++;
	// 	$('.email'+email_no).show();
		
	// 	if (email_no<1){
	// 		$('.email_minus').hide();

	// 	}else{
	// 		$('.email_minus').show();
	// 	};
	// 	if(email_no>3){
	// 		$('.email_add').hide();
	// 	};
	// });

	// $('.email_minus').on('click',function(){
	// 	$('.email'+email_no).hide();
	// 	$('#email_label'+email_no).val(null);
	// 	$('#email'+email_no).val(null);
	// 	email_no--;
	// 	if (email_no<1){
	// 		$('.email_minus').hide();

	// 	}else{
	// 		$('.email_minus').show();
	// 	};
		
	// 	if(email_no<=3){
	// 		$('.email_add').show();
	// 	};
	// });
	// 	$('.tel_add').on('click',function(){
	// 	tel_no++;
	// 	$('.tel'+tel_no).show();
	// 	if (tel_no<1){
	// 		$('.tel_minus').hide();

	// 	}else{
	// 		$('.tel_minus').show();
	// 	};
	// 	if(tel_no>3){
	// 		$('.tel_add').hide();
	// 	};
	// });

	// $('.tel_minus').on('click',function(){
	// 	$('.tel'+tel_no).hide();
	// 	$('#tel'+tel_no).val(null);
	// 	$('#tel_label'+tel_no).val(null);
	// 	tel_no--;
	// 	if (tel_no<1){
	// 		$('.tel_minus').hide();

	// 	}else{
	// 		$('.tel_minus').show();
	// 	};
		
	// 	if(tel_no<=3){
	// 		$('.tel_add').show();
	// 	};
	// });
	
});
</script>
@stop --}}