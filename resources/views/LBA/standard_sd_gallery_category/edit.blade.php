@extends('template')

@section('name')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('lba.standard.sd.gallery.category',array($cat_id,$prod_id)) }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Edit {{ config('cms.modules.LBA_STD_SD_CAT') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>array('lba.standard.sd.gallery.category.update',$cat_id,$prod_id,$StandardSDGalleryCategory->id),'name'=>'register_form','class'=>'form-horizontal','method'=>'put','files'=>true)) !!}
	        <div class="form-group">
				{!! Form::label('title', 'Category', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('title', old('title',$StandardSDGalleryCategory->title), array('class'=>"form-control",'placeholder'=>"eg. Outer")) !!}
				</div>
			</div>    
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					<a href="{{ route('lba.standard.sd.gallery.category',array($cat_id,$prod_id)) }}" class='btn btn-back'>Back</a>
					{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
				</div>
			</div>
		{!! Form::close() !!}
    </div>
</div>

@stop

@section('scripts')
<script>
$(function(){
});
</script>
@stop