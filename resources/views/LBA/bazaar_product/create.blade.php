@extends('template')

@section('name')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('lba.bazaar.product',array($cat_id)) }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Create {{ config('cms.modules.LBA_BAZ_PROD') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>array('lba.bazaar.product.store',$cat_id),'name'=>'register_form','class'=>'form-horizontal','method'=>'post','files'=>true)) !!}
	        <div class="form-group">
				{!! Form::label('image', 'Image', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<div class="col-sm-12 imgForm-container">
						<div class="imgBrowse-img-frame object-fit-contain">
							<img id="previewHere" src="{{ asset(imgTagShow('','default')) }}">
						</div>
						<div class="imgBrowse-browse-frame">
							{!! Form::file('image', array('class'=>"form-control", 'id'=>'img-browse', 'style'=>'display:none')) !!}
							<button type="button" class="btn btn-basic" onclick="javascript: $('#img-browse').trigger('click');">
								<i class='fa fa-lg fa-camera' data-toggle="tooltip" title="Browse image"></i>
							</button>
							{!! Form::text('', '', array('class'=>"form-control", 'readonly'=>"readonly")) !!}
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('title', 'Product', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('title', old('title'), array('class'=>"form-control",'placeholder'=>"eg. Flat Bars")) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('fields', 'Fields', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<div class="row" id='fields-row'>
						<div class="col-lg-12 row-template">
							<div class="input-group">
								<span class="input-group-addon row-template-label">Field 1</span>
								{!! Form::text('fields[]', '', array('class'=>"form-control", 'placeholder'=>"eg. A (mm)")) !!}
								<a href="#" class="input-group-addon delete-row-template hidden" data-toggle='tooltip' title='Remove'><i class="fa fa-times"></i></a>
							</div>
						</div>
					</div>
					<button type="button" class="btn" id='addField'>Add field</button>
					<span class="notify-red">*Ensure fields created are correct, any changes especially removing the fields may affect the data of child items.</span>
				</div>
			</div>
      
			{{-- @for($i=0;$i<5;$i++)
			<div class={{ 'field'.$i }} {!! ($i==0)?'':'hidden' !!}>
				<div class="form-group">
					{!! Form::label('field'.$i , 'Field'.' '.($i+1) , array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('field'.$i , old('field'.$i ), array('class'=>"form-control",'placeholder'=>"Field")) !!}
					</div>
				</div>
			</div>			
			@endfor
			<div class="form-group">
				<div class="col-sm-10"></div>
				<div class="col-sm-1">
				<div class="field_minus" hidden>
					<a class="btn btn-danger "><i class="fa fa-minus"></i></a>
				</div>
				<a class="btn btn-info field_add" ><i class="fa fa-plus"></i></a>
				</div>
			</div> --}}

			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					<a href="{{ route('lba.bazaar.product',array($cat_id)) }}" class='btn btn-back'>Back</a>
					{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
				</div>
			</div>
		{!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script id="fields-row-template" type="text/x-handlebars-template">
<div class="col-lg-12 row-template">
	<div class="input-group">
		<span class="input-group-addon row-template-label"></span>
		{!! Form::text('fields[]', '', array('class'=>"form-control", 'placeholder'=>"eg. A (mm)")) !!}
		<a href="#" class="input-group-addon delete-row-template" data-toggle='tooltip' title='Remove'><i class="fa fa-times"></i></a>
	</div>
</div>
</script>

<script type="text/javascript" src="{{ asset('assets/js/handlebars-v4.0.5.js') }}"></script>
<script type="text/javascript">
	$(function() {
		function reorganizeRowLabel() {
		    $('.row-template-label').each(function(index, element) {
		    	this.innerText = 'Field' + ' ' + (index + 1);
		    });
		}

		function hideShowDeleteRowBtn() {
			var delBtn = $('.delete-row-template');
			var btnCount = delBtn.length;
			if (!(btnCount > 1)) {
				delBtn.addClass('hidden');
			} else {
				delBtn.removeClass('hidden');
			}
		}

		$('#addField').on('click', function(e) {
			e.preventDefault();
			var source = $("#fields-row-template").html();
			var template = Handlebars.compile(source);
			var html = template();
			$('#fields-row').append(html);
			reorganizeRowLabel();
			hideShowDeleteRowBtn();
		});

		$(document).on('click','.delete-row-template',function(event) {
	        event.preventDefault();
	        var $self = $(this);
	        alertify.confirm('Remove row?').set('onok', function(closeEvent){
	            if($self.attr('href') != '#')
	            {
	                $.ajax({
	                    url: $self.data('href'),
	                    type: 'DELETE',
	                    success: function(result){
	                        $self.closest('.row-template').remove();
	                        alertify.success('Row removed.');
	                        reorganizeRowLabel();
	                    }
	                });
	            }else{
	                $self.closest('.row-template').remove();
	                alertify.success('Row removed.');
	                reorganizeRowLabel();
	                hideShowDeleteRowBtn();
	            }
	        });
	    });
	    
	    reorganizeRowLabel();
	    hideShowDeleteRowBtn();
	});
</script>
@stop

{{-- @section('scripts')
<script>
$(function(){
	var field_no=0;
	$('.field_add').on('click',function(){
		field_no++;
		$('.field'+field_no).show();
		
		if (field_no<1){
			$('.field_minus').hide();

		}else{
			$('.field_minus').show();
		};
		if(field_no>3){
			$('.field_add').hide();
		};
	});

	$('.field_minus').on('click',function(){
		$('.field'+field_no).hide();
		$('#field_label'+field_no).val(null);
		$('#field'+field_no).val(null);
		field_no--;
		if (field_no<1){
			$('.field_minus').hide();
		}else{
			$('.field_minus').show();
		};
		
		if(field_no<=3){
			$('.field_add').show();
		};
	});	
});
</script>
@stop --}}

