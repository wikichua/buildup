@extends('template')

@section('name')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('lba.bazaar.product',array($cat_id)) }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Edit {{ config('cms.modules.LBA_BAZ_PROD') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>array('lba.bazaar.product.update',$cat_id,$BazaarProduct->id),'name'=>'register_form','class'=>'form-horizontal','method'=>'put','files'=>true)) !!}
            <div class="form-group">
				{!! Form::label('image', 'Image', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<div class="col-sm-12 imgForm-container">
						<div class="imgBrowse-img-frame object-fit-contain">
							<img id="previewHere" src="{{ asset(imgTagShow($BazaarProduct->image,'default')) }}">
						</div>
						<div class="imgBrowse-browse-frame">
							{!! Form::file('image', array('class'=>"form-control", 'id'=>'img-browse', 'style'=>'display:none')) !!}
							<button type="button" class="btn btn-basic" onclick="javascript: $('#img-browse').trigger('click');">
								<i class='fa fa-lg fa-camera' data-toggle="tooltip" title="Browse Image"></i>
							</button>
							{!! Form::text('', '', array('class'=>"form-control", 'readonly'=>"readonly")) !!}
						</div>
					</div>
				</div>
			</div>
	        <div class="form-group">
				{!! Form::label('title', 'Product', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('title', old('title',$BazaarProduct->title), array('class'=>"form-control",'placeholder'=>"eg. Flat Bars")) !!}
				</div>
			</div>    

			<div class="form-group">
				{!! Form::label('fields', 'Fields', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<div class="row" id='fields-row'>
						@foreach ($BazaarProduct->fields as $field)
						<div class="col-lg-12 row-template">
							<div class="input-group">
								<span class="input-group-addon row-template-label"></span>
								{!! Form::text('fields[]', $field, array('class'=>"form-control", 'placeholder'=>"eg. A (mm)")) !!}
								<a href="#" class="input-group-addon delete-row-template" data-toggle='tooltip' title='Remove'><i class="fa fa-times"></i></a>
							</div>
						</div>
						@endforeach
					</div>
					<button type="button" class="btn" id='addField'>Add field</button>
					<span class="notify-red">*Ensure fields created are correct, any changes especially removing the fields may affect the data of child items.</span>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					<a href="{{ route('lba.bazaar.product',array($cat_id)) }}" class='btn btn-back'>Back</a>
					{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
				</div>
			</div>
		{!! Form::close() !!}
    </div>
</div>

@stop

@section('scripts')
<script id="fields-row-template" type="text/x-handlebars-template">
<div class="col-lg-12 row-template">
	<div class="input-group">
		<span class="input-group-addon row-template-label"></span>
		{!! Form::text('fields[]', '', array('class'=>"form-control", 'placeholder'=>"eg. A (mm)")) !!}
		<a href="#" class="input-group-addon delete-row-template" data-toggle='tooltip' title='Remove'><i class="fa fa-times"></i></a>
	</div>
</div>
</script>

<script type="text/javascript" src="{{ asset('assets/js/handlebars-v4.0.5.js') }}"></script>
<script type="text/javascript">
	$(function() {
		function reorganizeRowLabel() {
		    $('.row-template-label').each(function(index, element) {
		    	this.innerText = 'Field' + ' ' + (index + 1);
		    });
		}

		function hideShowDeleteRowBtn() {
			var delBtn = $('.delete-row-template');
			var btnCount = delBtn.length;
			if (!(btnCount > 1)) {
				delBtn.addClass('hidden');
			} else {
				delBtn.removeClass('hidden');
			}
		}

		$('#addField').on('click', function(e) {
			e.preventDefault();
			var source = $("#fields-row-template").html();
			var template = Handlebars.compile(source);
			var html = template();
			$('#fields-row').append(html);
			reorganizeRowLabel();
			hideShowDeleteRowBtn()
		});

		$(document).on('click','.delete-row-template',function(event) {
	        event.preventDefault();
	        var $self = $(this);
	        alertify.confirm('Remove row?').set('onok', function(closeEvent){
	            if($self.attr('href') != '#')
	            {
	                $.ajax({
	                    url: $self.data('href'),
	                    type: 'DELETE',
	                    success: function(result){
	                        $self.closest('.row-template').remove();
	                        alertify.success('Row removed.');
	                        reorganizeRowLabel();
	                    }
	                });
	            }else{
	                $self.closest('.row-template').remove();
	                alertify.success('Row removed.');
	                reorganizeRowLabel();
	                hideShowDeleteRowBtn()
	            }
	        });
	    });
	    
	    reorganizeRowLabel();
	    hideShowDeleteRowBtn()
	});
</script>
@stop