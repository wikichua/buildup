@extends('template')

@section('name')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('lba.alloy.specification') }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Create {{ config('cms.modules.LBA_ALOY_SPE') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>'lba.alloy.specification.store','name'=>'register_form','class'=>'form-horizontal','method'=>'post','files'=>true)) !!}
			<div class="form-group">
				{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('name', old('name'), array('class'=>"form-control",'placeholder'=>"eg. Alloy 1017")) !!}
				</div>
			</div>        

			<div class="form-group">
				{!! Form::label('source', 'Source', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<div class="col-sm-12 fileForm-container">
						<div class="fileBrowse-browse-frame">
							{!! Form::file('source', array('class'=>"form-control", 'id'=>'alloy-specification-file-browse', 'style'=>'display:none')) !!}
							<button type="button" class="btn btn-basic" onclick="javascript: $('#alloy-specification-file-browse').trigger('click');">
								<i class='fa fa-lg fa-folder-o' data-toggle="tooltip" title="Browse file"></i>
							</button>
							{!! Form::text('', '', array('class'=>"form-control", 'readonly'=>"readonly",'placeholder'=> config('cms.placeholder.file').'? MB')) !!}
						</div>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					<a href="{{ route('lba.alloy.specification') }}" class='btn btn-back'>Back</a>
					{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
				</div>
			</div>
		{!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){

});
</script>
@stop