@extends('template')

@section('title')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left"><h4>{{ config('cms.modules.LBA_ALOY_SPE') }}</h4></div>
		<div class="panel-heading-right">
			<button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button>
			@if (ACLButtonCheck('LBA_ALOY_SPE','Create'))
	       		<span>{!! action_add_button(route('lba.alloy.specification.create')) !!}</span>
	        @endif
		</div>
	</div>
	<div class="panel-body">
		<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						<th class="col-sm-1 shift_column"></th>
						<th>{!! sortTableHeaderSnippet('Name','name') !!}</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($AlloySpecifications as $AlloySpecification)
					<tr>
						<td class="shift_column">
							{!! shifting_buttons($AlloySpecification,route('lba.alloy.specification.shift',array($AlloySpecification->id,$AlloySpecification->p_id)),route('lba.alloy.specification.shift',array($AlloySpecification->id,$AlloySpecification->n_id))) !!}
						</td>
						<td>{{ $AlloySpecification->name }}</td>
						<td class="table-btn-action text-center">
							@if (ACLButtonCheck('LBA_ALOY_SPE','Update'))
							<a class="btn btn-basic" href="{{ route('lba.alloy.specification.edit',array($AlloySpecification->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('LBA_ALOY_SPE','Delete'))
							<a class="btn btn-basic delete" data-href="{{ route('lba.alloy.specification.destroy',array($AlloySpecification->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</td>
						
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::label('name', 'Name') !!}
					{!! searchTableHeaderSnippet('name') !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $AlloySpecifications->appends(request()->all())->render()) !!}
</div>	

@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop