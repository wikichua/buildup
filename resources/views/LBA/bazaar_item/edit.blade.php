@extends('template')

@section('name')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('lba.bazaar.item',array($cat_id, $prod_id)) }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Edit {{ config('cms.modules.LBA_BAZ_ITM') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>array('lba.bazaar.item.update',$cat_id, $prod_id, $BazaarItem->id),'name'=>'register_form','class'=>'form-horizontal','method'=>'put','files'=>true)) !!}
            <div class="form-group">
				{!! Form::label('image', 'Image', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<div class="col-sm-12 imgForm-container">
						<div class="imgBrowse-img-frame object-fit-contain">
							<img id="previewHere" src="{{ asset(imgTagShow($BazaarItem->image,'default')) }}">
						</div>
						<div class="imgBrowse-browse-frame">
							{!! Form::file('image', array('class'=>"form-control", 'id'=>'img-browse', 'style'=>'display:none')) !!}
							<button type="button" class="btn btn-basic" onclick="javascript: $('#img-browse').trigger('click');">
								<i class='fa fa-lg fa-camera' data-toggle="tooltip" title="Browse Image"></i>
							</button>
							{!! Form::text('', '', array('class'=>"form-control", 'readonly'=>"readonly")) !!}
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('section_no', 'Section No.', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('section_no', old('section_no',$BazaarItem->section_no), array('class'=>"form-control",'readonly','placeholder'=>"eg. 1287")) !!}
				</div>
			</div>
	        <div class="form-group">
				{!! Form::label('title', 'Title', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('title', old('title',$BazaarItem->title), array('class'=>"form-control",'placeholder'=>"Title")) !!}
				</div>
			</div>
			<hr />
			@foreach($BazaarProduct->fields as $fieldKey => $field)
				@foreach ($BazaarItem->values as $valueKey => $value)
					@if ($fieldKey === $valueKey)
			      		<div class="form-group">
							{!! Form::label($fieldKey, $field, array('class'=>'col-sm-3 control-label')) !!}
							<div class="col-sm-7">
								{!! Form::text($fieldKey, old($value,$value), array('class'=>"form-control",'placeholder'=>$field)) !!}
							</div>
						</div>
					@endif
      			@endforeach
      		@endforeach
			<hr />
      		<div class="form-group">
				{!! Form::label('weight', 'Weight (kg/m)', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('weight', old('weight',$BazaarItem->weight), array('class'=>"form-control",'placeholder'=>"eg. 5.511")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('ap', 'AP (mm)', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('ap', old('ap',$BazaarItem->ap), array('class'=>"form-control",'placeholder'=>"eg. 180.74")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('alloy_type', 'Alloy Type & Temper', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('alloy_type', old('alloy_type',$BazaarItem->alloy_type), array('class'=>"form-control",'placeholder'=>"eg. 6061 - T6511")) !!}
				</div>
			</div>   
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					<a href="{{ route('lba.bazaar.item',array($cat_id, $BazaarItem->prod_id)) }}" class='btn btn-back'>Back</a>
					{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
				</div>
			</div>
		{!! Form::close() !!}
    </div>
</div>

@stop

@section('scripts')
<script>
$(function(){
});
</script>
@stop