@extends('template')

@section('name')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('lba.standard.product',array($cat_id)) }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Edit {{ config('cms.modules.LBA_STD_PROD') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>array('lba.standard.product.update',$cat_id,$StandardProduct->id),'name'=>'register_form','class'=>'form-horizontal','method'=>'put','files'=>true)) !!}
			<div class="form-group">
				{!! Form::label('title', 'Product', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('title', old('title',$StandardProduct->title), array('class'=>"form-control",'placeholder'=>"eg. SCW01")) !!}
				</div>
			</div>    
			<div class="form-group">
				{!! Form::label('description', 'Description', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::textarea('description', old('description',$StandardProduct->description), array('class'=>"form-control",'placeholder'=>"eg. This is a description box.",'rows'=>3)) !!}
				</div>
			</div>   
			<hr />
			<div class="form-group">
				{!! Form::label('type_system', 'Type_system', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('type_system', old('type_system',$StandardProduct->type_system), array('class'=>"form-control",'placeholder'=>"eg. Type_system")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('thickness', 'Thickness', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('thickness', old('thickness',$StandardProduct->thickness), array('class'=>"form-control",'placeholder'=>"eg. Thickness")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('type', 'Type', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::select('type', array("Conceal"=>"Conceal","Exposed"=>"Exposed"),$StandardProduct->type, array('class'=>"form-control")) !!}
				</div>
			</div>  
			{{-- <div class="form-group">
				{!! Form::label('glass_thickness', 'Glass_thickness', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('glass_thickness', old('glass_thickness',$StandardProduct->glass_thickness), array('class'=>"form-control",'placeholder'=>"eg. Glass_thickness")) !!}
				</div>
			</div>    
			<div class="form-group">
				{!! Form::label('depth', 'Depth', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('depth', old('depth',$StandardProduct->depth), array('class'=>"form-control",'placeholder'=>"eg. Depth")) !!}
				</div>
			</div> --}}
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					<a href="{{ route('lba.standard.product',array($cat_id)) }}" class='btn btn-back'>Back</a>
					{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
				</div>
			</div>
		{!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){

});
</script>
@stop