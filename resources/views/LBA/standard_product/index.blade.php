@extends('template')

@section('title')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('lba.standard.catalog') }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>{{ config('cms.modules.LBA_STD_PROD') }}</h4>
			</div>
		</div>
		<div class="panel-heading-right">
			<button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button>
			@if (ACLButtonCheck('LBA_STD_PROD','Create'))
	       		<span>{!! action_add_button(route('lba.standard.product.create',array($cat_id))) !!}</span>
	        @endif
		</div>
	</div>
	<div class="panel-body">
		<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						<th class="col-sm-1 shift_column"></th>
						<th>{!! sortTableHeaderSnippet('Product','title') !!}</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($StandardProducts as $StandardProduct)
					<tr>
						<td class="shift_column">
							{!! shifting_buttons($StandardProduct,route('lba.standard.product.shift',array($cat_id,$StandardProduct->id,$StandardProduct->p_id)),route('lba.standard.product.shift',array($cat_id,$StandardProduct->id,$StandardProduct->n_id))) !!}
						</td>
						<td>{{ $StandardProduct->title }}</td>
						<td class="table-btn-action text-center">
							@if (ACLButtonCheck('LBA_STD_TSD','Read'))
							<a class="btn btn-basic" href="{{ route('lba.standard.tsd.gallery',array($cat_id,$StandardProduct->id)) }}" data-toggle='tooltip' title='Typical Shop Drawing'><i class="fa fa-picture-o"></i></a>
							@endif
							@if (ACLButtonCheck('LBA_STD_SD_CAT','Read'))
							<a class="btn btn-basic" href="{{ route('lba.standard.sd.gallery.category',array($cat_id,$StandardProduct->id)) }}" data-toggle='tooltip' title='Section Details'><i class="fa fa-map"></i></a>
							@endif
							@if (ACLButtonCheck('LBA_STD_PROD','Update'))
							<a class="btn btn-basic" href="{{ route('lba.standard.product.edit',array($cat_id,$StandardProduct->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('LBA_STD_PROD','Delete'))
							<a class="btn btn-basic delete" data-href="{{ route('lba.standard.product.destroy',array($cat_id,$StandardProduct->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::label('title', 'Product') !!}
					{!! searchTableHeaderSnippet('title') !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $StandardProducts->appends(request()->all())->render()) !!}
</div>	

@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop