@extends('template')

@section('name')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('lba.screen.popup') }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Create {{ config('cms.modules.LBA_SC_PU') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>array('lba.screen.popup.update',$ScreenPopUp->id),'name'=>'register_form','class'=>'form-horizontal','method'=>'put','files'=>true)) !!}
			<div class="form-group">
				{!! Form::label('image', 'Image', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<div class="col-sm-12 imgForm-container">
						<div class="imgBrowse-img-frame object-fit-contain">
							<img id="previewHere" src="{{ asset(imgTagShow($ScreenPopUp->image,'default')) }}">
						</div>
						<div class="imgBrowse-browse-frame">
							{!! Form::file('image', array('class'=>"form-control", 'id'=>'img-browse', 'style'=>'display:none')) !!}
							<button type="button" class="btn btn-basic" onclick="javascript: $('#img-browse').trigger('click');">
								<i class='fa fa-lg fa-camera' data-toggle="tooltip" title="Browse image"></i>
							</button>
							{!! Form::text('', '', array('class'=>"form-control", 'readonly'=>"readonly")) !!}
						</div>
					</div>
				</div>
			</div>        
			<div class="form-group">
				{!! Form::label('url', 'Url', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('url', old('url',$ScreenPopUp->url), array('class'=>"form-control",'placeholder'=>"eg. http://www.convep.com")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('published_at', 'Publish Date ', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('published_at', old('published_at',$ScreenPopUp->published_at->format('Y-m-d')), array('class'=>"form-control datepicker",'placeholder'=>"eg. 2016-11-05")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('expired_at', 'Expiry Date', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('expired_at', old('expired_at',$ScreenPopUp->expired_at->format('Y-m-d')), array('class'=>"form-control datepicker",'placeholder'=>"eg. 2016-11-06")) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					<a href="{{ route('lba.screen.popup') }}" class='btn btn-back'>Back</a>
					{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
				</div>
			</div>
		{!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){

});
</script>
@stop