@extends('template')

@section('title')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left"><h4>{{ config('cms.modules.LBA_SC_PU') }}</h4></div>
		<div class="panel-heading-right">
			{{-- <button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button> --}}
			@if (ACLButtonCheck('LBA_SC_PU','Create'))
	       		<span>{!! action_add_button(route('lba.screen.popup.create')) !!}</span>
	        @endif
		</div>
	</div>
	<div class="panel-body">
		<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						{{-- <th class="col-sm-1 shift_column"></th> --}}
						<th>Image</th>
						<th>Url</th>
						<th>{!! sortTableHeaderSnippet('Publish Date','published_at') !!}</th>
						<th>{!! sortTableHeaderSnippet('Expiry Date','expired_at') !!}</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($ScreenPopUps as $ScreenPopUp)
					<tr>
						{{-- <td class="shift_column">
							{!! shifting_buttons($ScreenPopUp,route('lba.screen.popup.shift',array($ScreenPopUp->id,$ScreenPopUp->p_id)),route('lba.screen.popup.shift',array($ScreenPopUp->id,$ScreenPopUp->n_id))) !!}
						</td> --}}
						<td>
							<div class="table-img-frame object-fit-cover" alt="{{ $ScreenPopUp->title }} image">
								<img src="{{ asset(imgTagShow($ScreenPopUp->image,'default')) }}">
							</div>
						</td>
						<td><a href="{{ url( $ScreenPopUp->url) }}">{{ $ScreenPopUp->url }}</a></td>
						<td>{{ $ScreenPopUp->published_at->format('Y-m-d') }}</td>
						<td>{{ $ScreenPopUp->expired_at->format('Y-m-d') }}</td>
						<td class="table-btn-action text-center">
							@if (ACLButtonCheck('LBA_PROJ_REF','Update'))
							<a class="btn btn-basic" href="{{ route('lba.screen.popup.edit',array($ScreenPopUp->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('LBA_PROJ_REF','Delete'))
							<a class="btn btn-basic delete" data-href="{{ route('lba.screen.popup.destroy',array($ScreenPopUp->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</td>
						
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{{-- {!! Form::label('Image', 'image') !!}
					{!! searchTableHeaderSnippet('image') !!} --}}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $ScreenPopUps->appends(request()->all())->render()) !!}
</div>	

@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop