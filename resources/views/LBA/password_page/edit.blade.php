@extends('template')

@section('name')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<h4>{{ config('cms.modules.LBA_PASS_PG') }}</h4>
		</div>
	</div>
    <div class="panel-body">
        {!! Form::open(array('route'=>array('lba.password.page.update',$PasswordPage->id),'name'=>'register_form','class'=>'form-horizontal','method'=>'put','files'=>true)) !!}
			<div class="form-group">
				{!! Form::label('password', 'Password', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('password', old('password',$PasswordPage->password), array('class'=>"form-control",'placeholder'=>"Password")) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
				</div>
			</div>
		{!! Form::close() !!}
    </div>
</div>

@stop

@section('scripts')
<script>
$(function(){
});
</script>
@stop