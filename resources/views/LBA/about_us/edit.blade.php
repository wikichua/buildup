@extends('template')

@section('title')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<h4>{{ config('cms.modules.LBA_ABT_US') }}</h4>
		</div>
	</div>
    <div class="panel-body">
    	{!! Form::open(array('route'=>array('lba.about.us.update'),'name'=>'register_form','class'=>'form-horizontal','method'=>'put','files'=>true)) !!}
        <div class="form-group">
			{!! Form::label('image', 'Image', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				<div class="col-sm-12 imgForm-container">
					<div class="imgBrowse-img-frame object-fit-contain">
						<img id="previewHere" src="{{ asset(imgTagShow($About->image,'default')) }}">
					</div>
					<div class="imgBrowse-browse-frame">
						{!! Form::file('image', array('class'=>"form-control", 'id'=>'img-browse', 'style'=>'display:none')) !!}
						<button type="button" class="btn btn-basic" onclick="javascript: $('#img-browse').trigger('click');">
							<i class='fa fa-lg fa-camera' data-toggle="tooltip" title="Browse Image"></i>
						</button>
						{!! Form::text('', '', array('class'=>"form-control", 'readonly'=>"readonly")) !!}
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('description', 'Description', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::textarea('description', old('description',$About->description), array('class'=>"form-control",'placeholder'=>"This is LB Aluminium.",'rows'=>3)) !!}
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-7">
				{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
			</div>
		</div>
		{!! Form::close() !!}
    </div>
</div>

@stop

@section('scripts')
<script>
$(function(){
});
</script>
@stop