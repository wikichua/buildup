@extends('template')

@section('title')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left"><h4>{{ config('cms.modules.LBA_BAZ_CAT') }}</h4></div>
		<div class="panel-heading-right">
			<button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button>
			@if (ACLButtonCheck('LBA_BAZ_CAT','Create'))
	       		<span>{!! action_add_button(route('lba.bazaar.catalog.create')) !!}</span>
	        @endif
		</div>
	</div>
	<div class="panel-body">
		<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						<th class="col-sm-1 shift_column"></th>
						<th>Image</th>
						<th>{!! sortTableHeaderSnippet('Catalog','title') !!}</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($BazaarCatalogs as $BazaarCatalog)
					<tr>
						<td class="shift_column">
							{!! shifting_buttons($BazaarCatalog,route('lba.bazaar.catalog.shift',array($BazaarCatalog->id,$BazaarCatalog->p_id)),route('lba.bazaar.catalog.shift',array($BazaarCatalog->id,$BazaarCatalog->n_id))) !!}
						</td>
						<td>
							<div class="table-img-frame object-fit-cover" alt="{{ $BazaarCatalog->title }} image">
								<img src="{{ asset(imgTagShow($BazaarCatalog->image,'default')) }}">
							</div>
						</td>
						<td>{{ $BazaarCatalog->title }}</td>
						<td class="table-btn-action text-center">
							@if (ACLButtonCheck('LBA_BAZ_PROD','Read'))
							<a class="btn btn-basic" href="{{ route('lba.bazaar.product',array($BazaarCatalog->id)) }}" data-toggle='tooltip' title='Bazaar Products'><i class="fa fa-folder"></i></a>
							@endif
							@if (ACLButtonCheck('LBA_BAZ_CAT','Update'))
							<a class="btn btn-basic" href="{{ route('lba.bazaar.catalog.edit',array($BazaarCatalog->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('LBA_BAZ_CAT','Delete'))
							<a class="btn btn-basic delete" data-href="{{ route('lba.bazaar.catalog.destroy',array($BazaarCatalog->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::label('title', 'Catalog') !!}
					{!! searchTableHeaderSnippet('title') !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $BazaarCatalogs->appends(request()->all())->render()) !!}
</div>	

@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop