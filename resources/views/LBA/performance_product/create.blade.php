@extends('template')

@section('name')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('lba.performance.product',array($cat_id)) }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Create {{ config('cms.modules.LBA_PFM_PROD') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>array('lba.performance.product.store',$cat_id),'name'=>'register_form','class'=>'form-horizontal','method'=>'post','files'=>true)) !!}
	        {{-- <div class="form-group">
				{!! Form::label('image', 'Image', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<div class="col-sm-12 imgForm-container">
						<div class="imgBrowse-img-frame object-fit-contain">
							<img id="previewHere" src="{{ asset(imgTagShow('','default')) }}">
						</div>
						<div class="imgBrowse-browse-frame">
							{!! Form::file('image', array('class'=>"form-control", 'id'=>'img-browse', 'style'=>'display:none')) !!}
							<button type="button" class="btn btn-basic" onclick="javascript: $('#img-browse').trigger('click');">
								<i class='fa fa-lg fa-camera' data-toggle="tooltip" title="Browse image"></i>
							</button>
							{!! Form::text('', '', array('class'=>"form-control", 'readonly'=>"readonly")) !!}
						</div>
					</div>
				</div>
			</div> --}}
			<div class="form-group">
				{!! Form::label('images', 'Images', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::file('images[]', array('class'=>"form-control", 'multiple'=>"multiple")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('title', 'Title', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('title', old('title'), array('class'=>"form-control",'placeholder'=>"eg. PCW01-40")) !!}
				</div>
			</div>      
			<div class="form-group">
				{!! Form::label('description', 'Description', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::textarea('description', old('description'), array('class'=>"form-control",'placeholder'=>"eg. This is a description box.",'rows'=>3)) !!}
				</div>
			</div> 
			<hr />
			<div class="form-group">
				{!! Form::label('type_system', 'Type System', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('type_system', old('type_system'), array('class'=>"form-control",'placeholder'=>"eg.")) !!}
				</div>
			</div>  
			<div class="form-group">
				{!! Form::label('thickness', 'Thickness', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('thickness', old('thickness'), array('class'=>"form-control",'placeholder'=>"eg.")) !!}
				</div>
			</div>  
			<div class="form-group">
				{!! Form::label('type', 'Type', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::select('type', array("Conceal"=>"Conceal","Exposed"=>"Exposed"),'', array('class'=>"form-control")) !!}
				</div>
			</div>  
			{{-- <div class="form-group">
				{!! Form::label('glass_thickness', 'Glass Thickness', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('glass_thickness', old('glass_thickness'), array('class'=>"form-control",'placeholder'=>"eg.")) !!}
				</div>
			</div>  
			<div class="form-group">
				{!! Form::label('depth', 'Depth', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('depth', old('depth'), array('class'=>"form-control",'placeholder'=>"eg.")) !!}
				</div>
			</div>   --}}

			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					<a href="{{ route('lba.performance.product',array($cat_id)) }}" class='btn btn-back'>Back</a>
					{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
				</div>
			</div>
		{!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){

});
</script>
@stop