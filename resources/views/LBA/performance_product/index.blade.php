@extends('template')

@section('title')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('lba.performance.catalog') }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>{{ config('cms.modules.LBA_PFM_PROD') }}</h4>
			</div>
		</div>
		<div class="panel-heading-right">
			<button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button>
			@if (ACLButtonCheck('LBA_PFM_PROD','Create'))
	       		<span>{!! action_add_button(route('lba.performance.product.create',array($cat_id))) !!}</span>
	        @endif
		</div>
	</div>
	<div class="panel-body">
		<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						<th class="col-sm-1 shift_column"></th>
						<th>Image</th>
						<th>{!! sortTableHeaderSnippet('Product','title') !!}</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($PerformanceProducts as $PerformanceProduct)
					<tr>
						<td class="shift_column">
							{!! shifting_buttons($PerformanceProduct,route('lba.performance.product.shift',array($cat_id,$PerformanceProduct->id,$PerformanceProduct->p_id)),route('lba.performance.product.shift',array($cat_id,$PerformanceProduct->id,$PerformanceProduct->n_id))) !!}
						</td>
						<td>
							<div class="table-img-frame object-fit-cover" alt="{{ $PerformanceProduct->title }} image">
								<img src="{{ asset(imgTagShow((($PerformanceProduct->images)? $PerformanceProduct->images[0] : ''),'default')) }}">
							</div>
						</td>
						<td>{{ $PerformanceProduct->title }}</td>
						<td class="table-btn-action text-center">
							@if (ACLButtonCheck('LBA_PFM_TSD','Read'))
							<a class="btn btn-basic" href="{{ route('lba.performance.tsd.gallery',array($cat_id, $PerformanceProduct->id)) }}" data-toggle='tooltip' title='Typical Shop Drawing'><i class="fa fa-picture-o"></i></a>
							@endif
							@if (ACLButtonCheck('LBA_PFM_WC','Read'))
							<a class="btn btn-basic" href="{{ route('lba.performance.wc.gallery',array($cat_id, $PerformanceProduct->id)) }}" data-toggle='tooltip' title='Winload Chart'><i class="fa fa-area-chart"></i></a>
							@endif
							@if (ACLButtonCheck('LBA_PFM_SD','Read'))
							<a class="btn btn-basic" href="{{ route('lba.performance.sd.gallery.category',array($cat_id, $PerformanceProduct->id)) }}" data-toggle='tooltip' title='Section Details'><i class="fa fa-map"></i></a>
							@endif
							@if (ACLButtonCheck('LBA_PFM_ACC','Read'))
							<a class="btn btn-basic" href="{{ route('lba.performance.acc.gallery',array($cat_id, $PerformanceProduct->id)) }}" data-toggle='tooltip' title='Accessories'><i class="fa fa-wrench"></i></a>
							@endif
							@if (ACLButtonCheck('LBA_PFM_PROD','Update'))
							<a class="btn btn-basic" href="{{ route('lba.performance.product.edit',array($cat_id, $PerformanceProduct->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('LBA_PFM_PROD','Delete'))
							<a class="btn btn-basic delete" data-href="{{ route('lba.performance.product.destroy',array($cat_id, $PerformanceProduct->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::label('title', 'Product') !!}
					{!! searchTableHeaderSnippet('title') !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $PerformanceProducts->appends(request()->all())->render()) !!}
</div>	

@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop