@extends('template')

@section('name')
	{{ config('cms.applications.LBA') }}
@stop

@section('page-header')
	{{ config('cms.applications.LBA') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('lba.performance.product',array($cat_id)) }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Edit {{ config('cms.modules.LBA_PFM_PROD') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>array('lba.performance.product.update',$cat_id, $PerformanceProduct->id),'name'=>'register_form','class'=>'form-horizontal','method'=>'put','files'=>true)) !!}
            {{-- <div class="form-group">
				{!! Form::label('image', 'Image', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<div class="col-sm-12 imgForm-container">
						<div class="imgBrowse-img-frame object-fit-contain">
							<img id="previewHere" src="{{ asset(imgTagShow($PerformanceProduct->image,'default')) }}">
						</div>
						<div class="imgBrowse-browse-frame">
							{!! Form::file('image', array('class'=>"form-control", 'id'=>'img-browse', 'style'=>'display:none')) !!}
							<button type="button" class="btn btn-basic" onclick="javascript: $('#img-browse').trigger('click');">
								<i class='fa fa-lg fa-camera' data-toggle="tooltip" title="Browse Image"></i>
							</button>
							{!! Form::text('', '', array('class'=>"form-control", 'readonly'=>"readonly")) !!}
						</div>
					</div>
				</div>
			</div> --}}
			<div class="form-group">
				{!! Form::label('current_images', 'Current Images', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
				@if ($PerformanceProduct->images)
					@foreach (array_chunk($PerformanceProduct->images,3) as $imageChunk)
					<div class="row add-row-padding">
						@foreach ($imageChunk as $image)
						<div class="col-xs-4 remove-space">
							<a href="{{ asset(imgTagShow($image,'default')) }}" data-lightbox="performance-product-img">
								<div class="object-fit-cover" style="width: 100%; height: 120px">
									<img src="{{ asset(imgTagShow($image,'default')) }}" />
								</div>
							</a>
						</div>
						@endforeach
					</div>
					@endforeach
				@else
					<span class="form-control">No image available</span>
				@endif
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('images', 'Images', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::file('images[]', array('class'=>"form-control", 'multiple'=>"multiple")) !!}
					<span class="notify-red">*Current function only allow replacement of all images on save. May include checkbox selection for data manipulation flexibility in future development.</span>
				</div>
			</div>
	        <div class="form-group">
				{!! Form::label('title', 'Title', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('title', old('title',$PerformanceProduct->title), array('class'=>"form-control",'placeholder'=>"Title")) !!}
				</div>
			</div>    
			<div class="form-group">
				{!! Form::label('description', 'Description', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::textarea('description', old('description',$PerformanceProduct->description), array('class'=>"form-control",'placeholder'=>"eg. This is a description box.",'rows'=>3)) !!}
				</div>
			</div> 
			<hr />
			<div class="form-group">
				{!! Form::label('type_system', 'Type System', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('type_system', old('type_system',$PerformanceProduct->type_system), array('class'=>"form-control",'placeholder'=>"eg.")) !!}
				</div>
			</div>  
			<div class="form-group">
				{!! Form::label('thickness', 'Thickness', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('thickness', old('thickness',$PerformanceProduct->thickness), array('class'=>"form-control",'placeholder'=>"eg.")) !!}
				</div>
			</div>  
			<div class="form-group">
				{!! Form::label('type', 'Type', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::select('type', array("Conceal"=>"Conceal","Exposed"=>"Exposed"),$PerformanceProduct->type, array('class'=>"form-control")) !!}
				</div>
			</div>  
			{{-- <div class="form-group">
				{!! Form::label('glass_thickness', 'Glass Thickness', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('glass_thickness', old('glass_thickness',$PerformanceProduct->glass_thickness), array('class'=>"form-control",'placeholder'=>"eg.")) !!}
				</div>
			</div>  
			<div class="form-group">
				{!! Form::label('depth', 'Depth', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('depth', old('depth',$PerformanceProduct->depth), array('class'=>"form-control",'placeholder'=>"eg.")) !!}
				</div>
			</div>   --}}    
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					<a href="{{ route('lba.performance.product',array($PerformanceProduct->cat_id)) }}" class='btn btn-back'>Back</a>
					{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
				</div>
			</div>
		{!! Form::close() !!}
    </div>
</div>

@stop

@section('scripts')
<script>
$(function(){
});
</script>
@stop