<!doctype html>
<html><head>
    <meta charset="utf-8">
    <title>{{ config('cms.brand','CMS Starter Convep Mobilogy') }} @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Convep Mobilogy Wiki Chua">

    <!-- Le styles -->
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('assets/css/tcd-login.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/font-style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/flexslider.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/alertifyjs/css/alertify.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/alertifyjs/css/themes/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">    
  </head>
  <body>
  
    <div id="container">
        <div id="body">
            <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-10 col-lg-offset-4 col-md-offset-4 col-sm-offset-3 col-xs-offset-1">
                <h1 class="text-center">{{ config('cms.brand') }}</h1>
                <hr />
                {!! Form::open(array('route' => array('login'),'name' => 'form','class' => 'form-horizontal', 'method' => 'post','files'=>false)) !!}
                    <div class="form-group">
                        {!! Form::text('email', old('email'), array('class' => 'form-control input-lg','placeholder' => 'someone@email.com')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::password('password', array('class' => 'form-control input-lg','placeholder' => 'Password')) !!}
                    </div>
                <hr />
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg submit" name='login'><i class="fa fa-sign-in"></i> Log In</button>
                        <a class="reset_pass" href="#">Lost your password?</a>
                    </div>  
                {!! Form::close() !!}
            </div>
            </div>
        </div>
        <div id="footer">@include('footer')</div>
    </div>


    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/alertifyjs/alertify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.flexslider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/cms.js') }}"></script>

    @include('alert')
  
</body></html>