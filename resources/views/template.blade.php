<!doctype html>
<html><head>
    <meta charset="utf-8">
    <title>{{ config('cms.brand','CMS Starter Convep Mobilogy') }} | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Convep Mobilogy Wiki Chua">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Le styles -->
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/jquery.datetimepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/font-style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/alertifyjs/css/alertify.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/alertifyjs/css/themes/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/flexslider.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/Trumbowyg/ui/trumbowyg.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lightbox2/css/lightbox.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css') }}" rel="stylesheet">
    {{--  <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('assets/css/tcd.css') }}" rel="stylesheet">
    
    @yield('style')

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="{{ asset('img/bup-favicon.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">    
  </head>
  <body>

    <div id="container" class="sidebar-opened">
        <div id="sidebar">@include('navbar')</div>
        <div id="page-header">
            <div id="page-header-left">@yield('page-header')</div>
            <div id="page-header-right">
                @if(Session::has('original_user_id'))
                <a href="{{ route('user.switch',0) }}" class="btn btn-switch-user"><i class="fa fa-random"></i>Switch Back</a>
                @else
                <a href="{{ route('logout') }}" class="btn btn-logout"><i class="fa fa-sign-out"></i>Log Out</a>
                @endif
            </div>
        </div>
        <div id="body">@yield('body')</div>
        <div id="footer">@include('footer')</div>
    </div>

    <script type="text/javascript" src="//code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="//momentjs.com/downloads/moment.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.datetimepicker.full.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/alertifyjs/alertify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.flexslider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/autosize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/blockUI.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/Trumbowyg/trumbowyg.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/lightbox2/js/lightbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/chartjs-2.1.6/dist/Chart.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/cms.js') }}"></script>

    @include('alert')

    <script type="text/javascript">
    $(document).ready(function(){
        alertify.defaults.glossary.title = '{{ config('cms.brand') }}';
        alertify.defaults.theme.ok = "btn btn-submit";
        alertify.defaults.theme.cancel = "btn btn-cancel";
        alertify.defaults.theme.input = "form-control";
    });
    </script>

    @yield('scripts')

    <script>
        // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        // })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        // ga('create', 'UA-17925977-19', 'auto');
        // ga('send', 'pageview');
    </script>

    <script src="https://www.gstatic.com/firebasejs/live/3.0/firebase.js"></script>
    <script>
      // Initialize Firebase
      var config = {
        apiKey: "AIzaSyCrqVNiWibHCgdkyUYdOo8MdCu_ZsvrMnM",
        authDomain: "cms-stater.firebaseapp.com",
        databaseURL: "https://cms-stater.firebaseio.com",
        storageBucket: "",
      };
      firebase.initializeApp(config);
    </script>
  
</body></html>