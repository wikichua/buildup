@extends('template')

@section('title')
	User
@stop

@section('page-header')
	User
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left"><h4>User List</h4></div>
		<div class="panel-heading-right">
			<button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button>
			@if (ACLButtonCheck('USR_MGMT','Create'))
	       		<span>{!! action_add_button(route('user.create')) !!}</span>
	        @endif
		</div>
	</div>
	<div class="panel-body">
    	<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						<th>Picture</th>
						<th>{!! sortTableHeaderSnippet('Name ','name') !!}</th>
						<th>{!! sortTableHeaderSnippet('Group ','usergroup-name') !!}</th>
						<th>{!! sortTableHeaderSnippet('Email ','email') !!}</th>
						<th>{!! sortTableHeaderSnippet('Status','status') !!}</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($Users as $User)
					<tr>
						<td>
							<div class="table-img-frame object-fit-cover" alt="{{ $User->name }} image">
								<img src="{{ asset(imgTagShow($User->photo,'profile')) }}">
							</div>
						</td>
						<td>{{ $User->name }}</td>
						<td>{{ $User->usergroup->name or 'N/A' }}</td>
						<td>{{ $User->email }}</td>
						<td>{{ $User->status }}</td>
						<td class="table-btn-action text-center">
							@if (ACLButtonCheck('USR_MGMT','Update'))
							<a class="btn btn-basic" href="{{ route('user.edit',array($User->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (auth()->user()->isAdmin && !session()->has('original_user_id'))
							<a class="btn btn-basic" href="{{ route('user.switch',array($User->id)) }}" data-toggle='tooltip' title='Switch User'><i class="fa fa-random"></i></a>
							@endif
							@if (ACLButtonCheck('USR_MGMT','Delete'))
							<a class="btn btn-basic delete" data-href="{{ route('user.destroy',array($User->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::label('name', 'Name') !!}
					{!! searchTableHeaderSnippet('name') !!}
				</div>
				<div class="form-group">
					{!! Form::label('usergroup-name', 'Group') !!}
					{!! searchTableHeaderSnippet('usergroup-name') !!}
				</div>
				<div class="form-group">
					{!! Form::label('email', 'Email') !!}
					{!! searchTableHeaderSnippet('email') !!}
				</div>
				<div class="form-group">
					{!! Form::label('status', 'Status') !!}
					{!! searchTableHeaderSnippet('status') !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $Users->appends(request()->all())->render()) !!}
</div>	

@stop