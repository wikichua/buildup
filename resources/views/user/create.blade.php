@extends('template')

@section('title')
	User
@stop

@section('page-header')
	User
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('user') }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Add User</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>'user.store','name'=>'register_form','class'=>'form-horizontal','method'=>'post','files'=>true)) !!}
		<div class="form-group">
			{!! Form::label('photo', 'Profile Picture', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				<div class="col-sm-12 imgForm-container">
					<div class="imgBrowse-img-frame object-fit-contain">
						<img id="previewHere" src="{{ asset(imgTagShow('','profile')) }}">
					</div>
					<span>{{ config('cms.notify.img').'? x ? px' }}</span>
					<div class="imgBrowse-browse-frame">
						{!! Form::file('photo', array('class'=>"form-control", 'id'=>'img-browse', 'style'=>'display:none')) !!}
						<button type="button" class="btn btn-basic" onclick="javascript: $('#img-browse').trigger('click');">
							<i class='fa fa-lg fa-camera' data-toggle="tooltip" title="Browse image"></i>
						</button>
						{!! Form::text('', '', array('class'=>"form-control", 'readonly'=>"readonly",'placeholder'=>config('cms.placeholder.img').'? MB')) !!}
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('name', 'Full Name', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('name', old('name'), array('class'=>"form-control",'placeholder'=>"eg. Akiyama Shinichi")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('email', 'Email', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('email', old('email'), array('class'=>"form-control",'placeholder'=>"eg. akiyama@lgame.com")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('password', 'Password', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::password('password', array('class'=>"form-control")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('password_confirmation', 'Confirm Password', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::password('password_confirmation', array('class'=>"form-control")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('usergroup_id', 'User Group', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::select('usergroup_id',$usergroups,old('usergroup_id'), array('class'=>"form-control select2",'placeholder'=>'Please select an option')) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('app', 'Application', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::select('app',array(''=>'None') + $applications,old('app'), array('class'=>"form-control select2",'placeholder'=>'Please select an option')) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('status', 'Status', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::checkbox('status','Active',old('status'), array('data-toggle'=>"switch-active")) !!}
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-7">
				<a href="{{ route('user') }}" class='btn btn-back'>Back</a>
				{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
			</div>
		</div>
		{!! Form::close() !!}
    </div>
</div>

@stop

@section('scripts')
<script>
$(function(){
});
</script>
@stop