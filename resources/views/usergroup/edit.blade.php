@extends('template')

@section('title')
    User Group
@stop

@section('page-header')
	User Group
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('usergroup') }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Edit User Group</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>array('usergroup.update',$UserGroup->id),'name'=>'register_form','class'=>'form-horizontal','method'=>'put','files'=>true)) !!}
		<div class="form-group">
			{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('name', old('name',$UserGroup->name), array('class'=>"form-control",'placeholder'=>"Name")) !!}
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-7">
				<a href="{{ route('usergroup') }}" class='btn btn-back'>Back</a>
				{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
			</div>
		</div>
		{!! Form::close() !!}
    </div>
</div>
@stop