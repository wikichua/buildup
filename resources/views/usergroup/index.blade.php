@extends('template')

@section('title')
    User Group
@stop

@section('page-header')
	User Group
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left"><h4>User Group List</h4></div>
		<div class="panel-heading-right">
			<button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button>
			@if (ACLButtonCheck('USR_GRP_MGMT','Create'))
	       		<span>{!! action_add_button(route('usergroup.create')) !!}</span>
	        @endif
		</div>
	</div>
	<div class="panel-body">
    	<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						<th>{!! sortTableHeaderSnippet('Name','name') !!}</th>
						<th>{!! sortTableHeaderSnippet('Modified','updated_at') !!}</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($UserGroups as $UserGroup)
					<tr>
						<td>{{ $UserGroup->name }}</td>
						<td>{{ $UserGroup->updated_at->toDayDateTimeString() }}</td>
						<td class="table-btn-action text-center">
							@if (ACLButtonCheck('USR_ACL','Read'))
							<a class="btn btn-basic" href="{{ route('acl',array($UserGroup->id)) }}" data-toggle='tooltip' title='Access Control Level'><i class="fa fa-unlock-alt"></i></a>
							@endif
							@if (ACLButtonCheck('USR_GRP_MGMT','Update'))
							<a class="btn btn-basic" href="{{ route('usergroup.edit',array($UserGroup->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('USR_GRP_MGMT','Delete'))
							<a class="btn btn-basic delete" data-href="{{ route('usergroup.destroy',array($UserGroup->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::label('name', 'Name') !!}
					{!! searchTableHeaderSnippet('name') !!}
				</div>
				<div class="form-group">
					{!! Form::label('updated_at', 'Modified') !!}
					{!! searchTableHeaderSnippet('updated_at') !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $UserGroups->appends(request()->all())->render()) !!}
</div>
       
@stop