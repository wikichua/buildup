@extends('template')

@section('title')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('page-header')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left"><h4>{{ config('cms.modules.CHAMP_CT') }}</h4></div>
		<div class="panel-heading-right">
			<button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button>
			@if (ACLButtonCheck('CHAMP_CT','Create'))
	       		<span>{!! action_add_button(route('champ.content.create')) !!}</span>
	        @endif
		</div>
	</div>
	<div class="panel-body">
		<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						<th>{!! sortTableHeaderSnippet('Character','character') !!}</th>
						<th>{!! sortTableHeaderSnippet('Level','level') !!}</th>
						<th>{!! sortTableHeaderSnippet('Language','language') !!}</th>
						<th>{!! sortTableHeaderSnippet('File','file') !!}</th>
						<th class="text-center">Action</th>
						
					</tr>
				</thead>
				<tbody>
					@foreach ($Contents as $Content)
					<tr>
						<td>{{ $Content->character }}</td>
						<td>{{ $Content->level }}</td>
						<td>{{ $Content->language }}</td>
						<td><a  href={{ URL::to("/uploads/".$Content->file) }} "http://localhost/buildup/public/uploads/{{ $Content->file }}">{{ $Content->file }}</a></td>
						<td>
						@if (ACLButtonCheck('CHAMP_CT','Update'))
							<a class="btn btn-basic" href="{{ route('champ.question_image',array($Content->id)) }}" data-toggle='tooltip' title='image'><i class="fa fa-picture-o"></i></a>
							@endif
							@if (ACLButtonCheck('CHAMP_CT','Update'))
							<a class="btn btn-basic" href="{{ route('champ.content.edit',array($Content->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('CHAMP_CT','Delete'))
							<a class="btn btn-basic delete" data-href="{{ route('champ.content.destroy',array($Content->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::label('created_at', 'Redeemed On') !!}
					{!! searchTableHeaderSnippet('created_at') !!}
				</div>
				<div class="form-group">
					{!! Form::label('Language', 'language') !!}
					{!! searchTableHeaderSnippet('language') !!}
				</div>
				<div class="form-group">
					{!! Form::label('Level', 'level') !!}
					{!! searchTableHeaderSnippet('level') !!}
				</div>
				<div class="form-group">
					{!! Form::label('Character', 'character') !!}
					{!! searchTableHeaderSnippet('character') !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $Contents->appends(request()->all())->render()) !!}
</div>	

@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop