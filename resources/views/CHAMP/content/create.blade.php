@extends('template')

@section('title')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('page-header')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('champ.content') }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Create {{ config('cms.modules.CHAMP_CT') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>'champ.content.store','name'=>'register_form','class'=>'form-horizontal','method'=>'post','files'=>true)) !!}
        <div class="form-group">
				<div class="form-group">
				{!! Form::label('file', 'XML File', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::file('file', array('class'=>"form-control")) !!}
				</div>
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('character', 'Character', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::select('character', array("Doctor"=>"Doctor","Athlete"=>"Athlete","Celebrity"=>"Celebrity","Pilot"=>"Pilot","Chef"=>"Chef",),'', array('class'=>"form-control")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('level', 'Level', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::select('level',array("1"=>"1","2"=>"2","3"=>"3","4"=>"4","5"=>"5"), old('level'), array('class'=>"form-control")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('language', 'Language', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::select('language',array("English"=>"English","BahasaMelayu"=>"Bahasa Melayu"), old('language'), array('class'=>"form-control")) !!}
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-7">
				<a href="{{ route('champ.content') }}" class='btn btn-back'>Back</a>
				{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
			</div>
		</div>
		{!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	
});
</script>
@stop