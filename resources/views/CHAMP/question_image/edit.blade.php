@extends('template')

@section('title')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('page-header')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('champ.question_image',array($Question_image->content_id)) }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Edit {{ config('cms.modules.CHAMP_QIMG') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>array('champ.question_image.update',$Question_image->id),'name'=>'register_form','class'=>'form-horizontal','method'=>'put','files'=>true)) !!}
            <div class="form-group">
			{!! Form::label('image', 'Image', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				<div class="col-sm-12 imgForm-container">
					<div class="imgBrowse-img-frame object-fit-contain">
						<img id="previewHereImg" src="{{ asset(imgTagShow($Question_image->image,'default')) }}">
					</div>
					<div class="imgBrowse-browse-frame">
						{!! Form::file('image', array('class'=>"form-control", 'id'=>'img-browse', 'style'=>'display:none')) !!}
						<button type="button" class="btn btn-basic" onclick="javascript: $('#img-browse').trigger('click');">
							<i class='fa fa-lg fa-camera' data-toggle="tooltip" title="Browse image"></i>
						</button>
						{!! Form::text('', '', array('class'=>"form-control", 'readonly'=>"readonly")) !!}
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('name', old('name',$Question_image->name), array('class'=>"form-control",'placeholder'=>"eg. Product 1 Voucher")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('image_for', 'Image for', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::select('image_for', array("Image"=>"Queastion Image","ImageDYK "=>"DYK Image"),$Question_image->image_for, array('class'=>"form-control")) !!}
			</div>
		</div>

			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					<a href="{{ route('champ.question_image',array($Question_image->content_id)) }}" class='btn btn-back'>Back</a>
					{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
				</div>
			</div>
	</div>
	
		
		{!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	
});
</script>
@stop