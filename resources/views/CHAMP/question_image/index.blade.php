@extends('template')

@section('title')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('page-header')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('champ.content') }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>{{ config('cms.modules.CHAMP_QIMG') }}</h4>
				
			</div>
		</div>
		<div class="panel-heading-right">
			<button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button>
			@if (ACLButtonCheck('CHAMP_QIMG','Create'))
	       		<span>{!! action_add_button(route('champ.question_image.create',array($Content_id))) !!}</span>
	        @endif
		</div>
	</div>
	<div class="panel-body">
		<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						<th>{!! sortTableHeaderSnippet('Image Code','name') !!}</th>
						<th>{!! sortTableHeaderSnippet('Image Type','image_for') !!}</th>
						<th>{!! sortTableHeaderSnippet('Image','image') !!}</th>
						<th class="text-center">Action</th>
						
					</tr>
				</thead>
				<tbody>
					@foreach ($Question_images as $Question_image)
					<tr>
						<td>{{ $Question_image->name }}</td>
						<td>{{ $image_for[trim($Question_image->image_for)] }}</td>
						<td>
							<div class="table-img-frame object-fit-cover" alt="{{ $Question_image->name }} image">
								<img src="{{ asset(imgTagShow($Question_image->image,'default')) }}">
							</div>
						</td>
						<td class="table-btn-action text-center">
							@if (ACLButtonCheck('CHAMP_QIMG','Update'))
							<a class="btn btn-basic" href="{{ route('champ.question_image.edit',array($Question_image->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>app/Http/Controllers/CHAMP/CHAMP_Question_ImageController.php
							@endif
							@if (ACLButtonCheck('CHAMP_QIMG','Delete'))
							<a class="btn btn-basic delete" data-href="{{ route('champ.question_image.destroy',array($Question_image->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>


<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">

				<div class="form-group">
					{!! Form::label('name', 'Name') !!}
					{!! searchTableHeaderSnippet('name') !!}
				</div>

				<div class="form-group">
					{!! Form::label('image_for', 'Image for') !!}
					{!! searchTableHeaderSnippet('image_for') !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $Question_images->appends(request()->all())->render()) !!}
</div>	

@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop