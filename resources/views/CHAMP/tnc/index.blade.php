<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>My Champion : Terms & Conditions</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">

        <!-- Le styles -->
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- <link rel="shortcut icon" href="assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
        <link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">     -->
        <style>
            @media (max-width: 768px) {
              .nav-justified > li {
                display: table-cell;
                width: 1%;
              }
            }

            #tnc {
                padding: 35px;
            }

            #tnc h5 {
                font-size: 16px;
            }
        </style>
    </head>
    <body>

        <ul class="nav nav-pills nav-justified">
            <li role="presentation" class="active">
                <a data-toggle="tab" href="#tnc">
                    <h4>Terms & Conditions</h4>
                </a>
            </li>
            <li role="presentation">
                <a href="http://www.ccmberhad.com/personal-data-protection">
                    <h4>PDPA</h4>
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div id="tnc" class="tab-pane fade in active" role="tabpanel" aria-labelledby="TnC">
                <h4 class="text-center"><strong>MY CHAMPION Mobile App Contest Terms & Conditions</strong></h4>
                <br />
                <h5><strong>I. Organiser</strong></h5>
                <p>CCM Pharmaceuticals Sdn Bhd ("CCMPSB").</p>
                <br />
                <h5><strong>II. General</strong></h5>
                <ul>
                    <li>
                        <p>These terms and conditions are known as the "Rules".</p>
                    </li>
                    <li>
                        <p>These Rules apply to MY CHAMPION Mobile App Contest ("Contest") and by participating in the Contest, you agree to be bound by the Rules.</p>
                    </li>
                    <li>
                        <p>This Contest is organised by CCMPSB and CCMPSB reserves the right to cancel or amend all or any part of the Contest and/or the Rules without notice for any event that is outside of CCMPSB's reasonable control. It is the responsibility of participants to keep themselves informed as to any changes to the Rules.</p>
                    </li>
                    <li>
                        <p>CCMPSB belongs under the group of Chemical Company of Malaysia Bhd. (CCMB).</p>
                    </li>
                    <li>
                        <p>In the event of any dispute regarding the Rules, the conduct or results of each submission, or any other matter relating to the Contest, the decision of CCMPSB shall be final and unchallengeable and no correspondence or discussion shall be entered into, comment issued, or reason given in respect of any decision made by CCMPSB.</p>
                    </li>
                </ul>
                <br />
                <h5><strong>III. Name of Promotion</strong></h5>
                <p>MY CHAMPION Mobile App Contest</p>
                <br />
                <h5><strong>IV. Eligibility Criteria</strong></h5>
                <ul>
                    <li>
                        <p>The Contest is open to all Malaysian citizens.</p>
                    </li>
                    <li>
                        <p>Subsidiaries, distributors, sales representatives, retailers and advertising, promotion agencies and all other service agencies and providers involved with the campaign, and members of their immediate family (spouse, parent, child, sibling and their respective spouses, regardless of where they reside) and households of each, whether or not related are not eligible to participate in the Contest.</p>
                    </li>
                    <li>
                        <p>Submissions received before the commencement of the Contest Period and after the close of the Contest Period will be disqualified and ineligible for consideration.</p>
                    </li>
                    <li>
                        <p>CCMPSB has the right to verify the validity of any submission and reserves the right to disqualify any participant for tampering with the submission process, submitting a submission not in accordance with these terms and conditions, repetitive or invalid or if the participant is engaged in unlawful conduct that jeopardises the fair and proper conduct of the Contest.</p>
                    </li>
                </ul>
                <br />
                <h5><strong>V. Contest Mechanics</strong></h5>
                <ul>
                    <li>
                        <p>Participants would need to complete all levels before submitting their final total score.</p>
                    </li>
                    <li>
                        <p>Participants would need to sign in to their Facebook account to register their details to be eligible to win.</p>
                    </li>
                    <li>
                        <p>Winners will be determined based on the highest score and fastest time taken to complete all levels. There will be a total of three winners throughout the Contest Period.</p>
                    </li>
                    <li>
                        <p>Participants can only win once.</p>
                    </li>
                </ul>
                <br />
                <h5><strong>VI. Notification of Winners</strong></h5>
                <ul>
                    <li>
                        <p>There will be three prizes in total to be given out throughout the Contest Period.</p>
                    </li>
                    <li>
                        <p>The prize for the Contest Period is ONE (1) Samsung Tablet A 8.0 16GB model.</p>
                    </li>
                </ul>
                <br />
                <h5><strong>VII. Contest Prizes</strong></h5>
                <ul>
                    <li>
                        <p>Winners will be notified via email. In the event if:</p>
                        <ul>
                            <li type="a">
                                <p>The winner is disqualified;</p>
                            </li>
                            <li type="a">
                                <p>The winner cannot be contacted by CCMPSB for five (5) days to conduct the verification and prize claiming process;</p>
                            </li>
                            <li type="a">
                                <p>The registration details winners submitted is found to be ineligible for any reasons herein; </p>
                            </li>
                            <li type="a">
                                <p>There is a failure to claim the Prize. The Prize may be forfeited and CCMPSB reserves the right to select a new winner.</p>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <p>CCMPSB is entitled to replace the contest prize with another item of similar value at any time without any prior notice.</p>
                    </li>
                    <li>
                        <p>The winners agree to the use of his/her name and image in any publicity material in connection to this Contest.</p>
                    </li>
                    <li>
                        <p>CCMPSB may or may not publish the names of the winners on the website/Facebook page of the Contest and it is CCMPSB's absolute discretion if they choose to do so or not, and their decision is final.</p>
                    </li>
                    <li>
                        <p>Apple is not a sponsor or involved in the activity(contest) in any manner.</p>
                    </li>
                </ul>
                <br />
                <h5><strong>VIII. Disclosure of information to outside parties</strong></h5>
                <p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our Website and Facebook pages and application, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property, or safety.</p>
                <br />
                <h5><strong>IX. Governing Law and Jurisdiction</strong></h5>
                <p>The Rules and any dispute or claim arising out of or in connection with them or their subject matter (including the Contest), existence, negotiation, validity, termination or enforceability (including non-contractual disputes or claims) shall be governed by and construed in accordance with Malaysian law. The Courts of Malaysia shall have exclusive jurisdiction in relation to any dispute or claim arising out of or in connection with the Rules or their subject matter, existence, negotiation, validity, termination or enforceability (including non-contractual disputes or claims).</p>
                <br />
            </div>
        </div>

        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap.js') }}"></script>

    </body>
</html>