
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if !mso]><!-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        table {border-collapse: collapse;}
    </style>
    <![endif]-->
</head>
<body style="margin: 0 !important; padding: 0; background-color: #f6f8f1; font-family: Arial, Helvetica, sans-serif;">
    <center class="wrapper" style="width: 100%; table-layout: fixed; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%">
        <div class="webkit" style="max-width: 600px; margin: 0 auto">
            <!--[if (gte mso 9)|(IE)]>
            <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td>
            <![endif]-->
            <table class="outer" align="center" cellpadding="0" cellspacing="0" border="0" width="95%" style="margin: 0 auto; max-width: 600px">

                <tr>
                    <td bgcolor="#44525f" style="padding:60px 30px 60px 30px">
                        <table cellpadding="0" cellspacing="0" border="0" width="0%" style="max-width: 600px">
                            <tr>
                                <td style="color: #EBEBEB; font-weight: bold; font-size: 32px">
                                    MYCHAMPION
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td class="innerpadding borderbottom" bgcolor="#FAFAFA" style="padding: 30px 30px 30px 30px; border-bottom: 1px solid #f2eeed">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="h2" style="padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold">
                                    Welcome to MyChampion 
                                </td>
                            </tr>
                            <tr>
                                <td class="bodycopy" style="font-size: 16px; line-height: 22px;">
                                    HI {{ $name }},<br/>
                                    <br/>

                                    Thanks for registering with us.
                                    <br/>
                                    <br/>

                                    Your account has been created .Please click this <a href={{ $active_link }}>
                                    activation link to active your account</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td class="footer" bgcolor="#44525f" style="padding:15px 30px 15px 30px">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="footercopy" style="font-family: sans-serif; font-size: 14px; color: #ffffff">
                                    Copyright &copy; {{ date("Y") }} Convep Mobilogy Sdn. Bhd.<br/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </div>
    </center>
</body>
</html>