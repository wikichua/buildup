@extends('template')

@section('title')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('page-header')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left"><h4>{{ config('cms.modules.CHAMP_SCO') }}</h4></div>
		<div class="panel-heading-right">
			<button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button>
		</div>
	</div>
	<div class="panel-body">
		<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						<th>{!! sortTableHeaderSnippet('Submitted On','updated_at') !!}</th>
						<th>{!! sortTableHeaderSnippet('Player','user-name') !!}</th>
						<th>{!! sortTableHeaderSnippet('Character ID','character_id') !!}</th>
						<th>{!! sortTableHeaderSnippet('Level','level') !!}</th>
						<th>{!! sortTableHeaderSnippet('Score','best_score') !!}</th>
						<th>{!! sortTableHeaderSnippet('Duration','total_duration') !!}</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($Scores as $Score)
					<tr>
						<td>{{ $Score->updated_at->format('d/M/Y h:iA') }}</td>
						<td>{{ $Score->user->name }}</td>
						<td>{{ $Score->character_id }}</td>
						<td>{{ $Score->level }}</td>
						<td>{{ $Score->best_score }}</td>
						<td>{{ $Score->total_duration }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::label('updated_at', 'Submitted On') !!}
					{!! searchTableHeaderSnippet('updated_at') !!}
				</div>
				<div class="form-group">
					{!! Form::label('player', 'Player') !!}
					{!! searchTableHeaderSnippet('user-name') !!}
				</div>
				<div class="form-group">
					{!! Form::label('character_id', 'Character ID') !!}
					{!! searchTableHeaderSnippet('character_id') !!}
				</div>
				<div class="form-group">
					{!! Form::label('level', 'Level') !!}
					{!! searchTableHeaderSnippet('level') !!}
				</div>
				<div class="form-group">
					{!! Form::label('best_score', 'Score') !!}
					{!! searchTableHeaderSnippet('best_score') !!}
				</div>
				<div class="form-group">
					{!! Form::label('total_duration', 'Duration') !!}
					{!! searchTableHeaderSnippet('total_duration') !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $Scores->appends(request()->all())->render()) !!}
</div>	

@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop