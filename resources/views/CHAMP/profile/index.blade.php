@extends('template')

@section('title')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('page-header')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left"><h4>{{ config('cms.modules.CHAMP_PRO') }}</h4></div>
		<div class="panel-heading-right">
			<button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button>
		</div>
	</div>
	<div class="panel-body">
		<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						<th>{!! sortTableHeaderSnippet('Created On','created_at') !!}</th>
						<th>{!! sortTableHeaderSnippet('Player Name','user-name') !!}</th>
						<th>{!! sortTableHeaderSnippet('FB ID','fb_id') !!}</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($Profiles as $Profile)
					<tr>
						<td>{{ $Profile->created_at->format('d/M/Y h:iA') }}</td>
						<td>{{ $Profile->user->name }}</td>
						<td>{{ $Profile->fb_id }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::label('created_at', 'Created On') !!}
					{!! searchTableHeaderSnippet('created_at') !!}
				</div>
				<div class="form-group">
					{!! Form::label('player', 'Player Name') !!}
					{!! searchTableHeaderSnippet('user-name') !!}
				</div>
				<div class="form-group">
					{!! Form::label('fb_id', 'FB ID') !!}
					{!! searchTableHeaderSnippet('fb_id') !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $Profiles->appends(request()->all())->render()) !!}
</div>	

@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop