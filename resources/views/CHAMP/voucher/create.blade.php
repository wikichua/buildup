@extends('template')

@section('title')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('page-header')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('champ.voucher') }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Create {{ config('cms.modules.CHAMP_VOU') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>'champ.voucher.store','name'=>'register_form','class'=>'form-horizontal','method'=>'post','files'=>true)) !!}
        
		<div class="form-group">
			{!! Form::label('name', 'Serial Number', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('name', old('name'), array('class'=>"form-control",'placeholder'=>"eg. Serial Number")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('description', 'Description', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::textarea('description', old('description'), array('class'=>"form-control",'placeholder'=>"eg. 20% Discount on selected product",'rows'=>3)) !!}
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-7">
				<a href="{{ route('champ.voucher') }}" class='btn btn-back'>Back</a>
				{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
			</div>
		</div>
		{!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	
});
</script>
@stop