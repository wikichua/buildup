@extends('template')

@section('title')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('page-header')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('champ.voucher') }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>Edit {{ config('cms.modules.CHAMP_VOU') }}</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>array('champ.voucher.update',$Voucher->id),'name'=>'register_form','class'=>'form-horizontal','method'=>'put','files'=>true)) !!}
        
		<div class="form-group">
			{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('name', old('name',$Voucher->name), array('class'=>"form-control",'placeholder'=>"eg. Product 1 Voucher")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('description', 'Description', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::textarea('description', old('description',$Voucher->description), array('class'=>"form-control",'placeholder'=>"eg. 20% Discount on selected product",'rows'=>3)) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('level', 'Level', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('level', old('level',$Voucher->level), array('class'=>"form-control",'placeholder'=>"eg. ",'rows'=>3,'readonly')) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('character', 'Character', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('character', old('character',$Voucher->character), array('class'=>"form-control",'placeholder'=>"eg. ",'rows'=>3,'readonly')) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('issued_at', 'Issued at', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('issued_at', old('issued_at',$Voucher->issued_at), array('class'=>"form-control",'placeholder'=>"eg. ",'rows'=>3,'readonly')) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('issued_to_user_id', 'User', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('issued_to_user_id', old('issued_to_user_id',$Voucher->issued_to_user_id), array('class'=>"form-control",'placeholder'=>"eg. ",'rows'=>3,'readonly')) !!}
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-7">
				<a href="{{ route('champ.voucher') }}" class='btn btn-back'>Back</a>
				{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
			</div>
		</div>
		{!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	
});
</script>
@stop