@extends('template')

@section('title')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('page-header')
	{{ config('cms.applications.CHAMP') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left"><h4>{{ config('cms.modules.CHAMP_VOU') }}</h4></div>
		<div class="panel-heading-right">
		<a href={{ route('champ.voucher.active') }}  data-toggle="tooltip" class="btn  btn-sm {{ ($Active=="YES")?"btn-success":"btn-danger" }}"><i class="fa fa-checked"></i> {{ ($Active=="YES")?"Actived":"Inactived" }}</a>

			<button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button>
			@if (ACLButtonCheck('CHAMP_VOU','Create'))
	       		<span>{!! action_add_button(route('champ.voucher.create')) !!}</span>
	        @endif
		</div>
	</div>
	<div class="panel-body">
		<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						<th>{!! sortTableHeaderSnippet('Created On','created_at') !!}</th>
						<th>{!! sortTableHeaderSnippet('Name','name') !!}</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($Vouchers as $Voucher)
					<tr>
						<td>{{ $Voucher->updated_at->format('d/M/Y g:iA') }}</td>
						<td>{{ $Voucher->name }}</td>
						<td class="table-btn-action text-center">
							@if (ACLButtonCheck('CHAMP_VOU','Update'))
							<a class="btn btn-basic" href="{{ route('champ.voucher.edit',array($Voucher->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('CHAMP_VOU','Delete'))
							<a class="btn btn-basic delete" data-href="{{ route('champ.voucher.destroy',array($Voucher->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::label('created_at', 'Created On') !!}
					{!! searchTableHeaderSnippet('created_at') !!}
				</div>
				<div class="form-group">
					{!! Form::label('expired_at', 'Expire On') !!}
					{!! searchTableHeaderSnippet('expired_at') !!}
				</div>
				<div class="form-group">
					{!! Form::label('name', 'Name') !!}
					{!! searchTableHeaderSnippet('name') !!}
				</div>
				<div class="form-group">
					{!! Form::label('level', 'Level') !!}
					{!! searchTableHeaderSnippet('level') !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $Vouchers->appends(request()->all())->render()) !!}
</div>	

@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop