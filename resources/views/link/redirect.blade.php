<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Redirecting</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">

        <!-- Le styles -->
        <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- <link rel="shortcut icon" href="assets/ico/favicon.ico"> -->

        <style>
            html, body {
                height: 100%;
                width: 100%;
                padding: 0px;
                margin: 0px;
            }

            .container {
                position: relative;
                height: 100%;
            }

            .row {
                position: absolute;
                -webkit-transform: translate(-50%,-50%);
                -moz-transform: translate(-50%,-50%);
                -ms-transform: translate(-50%,-50%);
                transform: translate(-50%,-50%);
                left: 50%;
                top: 50%;
            }

            img {
                height: 60px;
            }
        </style>

    </head>
    <body>

        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap.js') }}"></script>

        <script type="text/javascript">
        $(document).ready(function(){
            var isMobile = {
                Android: function() {
                    return navigator.userAgent.match(/Android/i);
                },
                BlackBerry: function() {
                    return navigator.userAgent.match(/BlackBerry/i);
                },
                iOS: function() {
                    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                },
                Opera: function() {
                    return navigator.userAgent.match(/Opera Mini/i);
                },
                Windows: function() {
                    return navigator.userAgent.match(/IEMobile/i);
                },
                any: function() {
                    return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
                }
            };

            if (isMobile.any()) {
                if (isMobile.Android()) {
                    window.location.href = '{{ $AND_link }}';
                } else if (isMobile.iOS()) {
                    window.location.href = '{{ $IOS_link }}';
                }
            }
        });
        </script>
        
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <a class="btn center-block" href="{{ $AND_link }}">
                        <img src="{{ asset('img/googleplay-logo.png') }}" />
                    </a>
                </div>
                <div class="col-md-6">
                    <a class="btn center-block" href="{{ $IOS_link }}">
                        <img src="{{ asset('img/appstore-logo.png') }}" />
                    </a>
                </div>
            </div>
        </div>


    </body>
</html>