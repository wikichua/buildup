@extends('template')

@section('title')
	Audit Trail
@stop

@section('page-header')
	Audit Trail
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left"><h4>Trail List</h4></div>
		<div class="panel-heading-right">
			<button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button>
		</div>
	</div>
	<div class="panel-body">
		<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						<th>{!! sortTableHeaderSnippet('Date Time','created_at') !!}</th>
						<th>{!! sortTableHeaderSnippet('User','user-name') !!}</th>
						<th>{!! sortTableHeaderSnippet('Action','action') !!}</th>
						<th>{!! sortTableHeaderSnippet('Table/Model','model') !!}</th>
						<th class="text-center" style='width:15%;'>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($AuditTrails as $AuditTrail)
					<tr>
						<td>{{ $AuditTrail->created_at->format('d/M/Y g:i a') }}</td>
						<td>{{ $AuditTrail->user->name or 'System' }}</td>
						<td>{{ ucwords($AuditTrail->action) }}</td>
						<td>{{ ucwords(str_replace('mcmc\\','',$AuditTrail->model)) }}</td>
						<td class="table-btn-action text-center">
							@if (ACLButtonCheck('ADT_MGMT','Read'))
							<a class="btn btn-basic" href="{{ route('audit_trail.show',array($AuditTrail->id)) }}" data-toggle='tooltip' title='View'><i class="fa fa-eye"></i></a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
        </div>
    </div>
</div>

<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::label('created_at', 'Date Time') !!}
					{!! searchTableHeaderSnippet('created_at') !!}
				</div>
				<div class="form-group">
					{!! Form::label('user-name', 'User') !!}
					{!! searchTableHeaderSnippet('user-name') !!}
				</div>
				<div class="form-group">
					{!! Form::label('action', 'Action') !!}
					{!! searchTableHeaderSnippet('action') !!}
				</div>
				<div class="form-group">
					{!! Form::label('model', 'Table / Model') !!}
					{!! searchTableHeaderSnippet('model') !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $AuditTrails->appends(request()->all())->render()) !!}
</div>	
			
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
	@if(isSearchOrSortExecuted() === 'false')
		$('.shift_column').remove();
	@endif
});
</script>
@stop