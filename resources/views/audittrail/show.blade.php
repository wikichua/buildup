@extends('template')

@section('title')
	Audit Trail
@stop

@section('page-header')
	Audit Trail
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<a href="{{ route('audit_trail') }}"><i class="fa fa-arrow-left"></i></a>
			<div class="heading-title">
				<h4>View Trail</h4>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">User / Date Time</label>
				<div class="col-sm-10">
					<p class="form-control">{{ $AuditTrail->user->name or 'System' }} / <strong class="text-danger">{{ $AuditTrail->created_at->format('d/M/Y g:i a') }}</strong></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Action / Model / ID</label>
				<div class="col-sm-10">
					<p class="form-control">{{ ucwords($AuditTrail->action) }} / {{ ucwords(str_replace('mcmc\\','',$AuditTrail->model)) }} / {{ ucwords($AuditTrail->model_id) }}</p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Changes</label>
				<div class="col-sm-5 bg-danger">
					<p class="text-center text-danger"><strong>New Data</strong></p>
					{!! jsonToHTMLList($AuditTrail->new_data) !!}
				</div>
				<div class="col-sm-5 bg-warning">
					<p class="text-center text-warning"><strong>Old Data</strong></p>
					{!! jsonToHTMLList($AuditTrail->old_data) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-12">
					<a href="{{ route('audit_trail') }}" class='btn btn-back'>Back</a>
				</div>
			</div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
});
</script>
@stop