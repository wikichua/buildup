<div id="sidebar-brand">
  <a href="{{ route('dashboard') }}">
    <div class="brand-name">
      <img src="{{ asset('img/bup-logo.png') }}">
      <h3>Build Up</h3>
    </div>
  </a>
  <span id="toggle-nav"><i class="fa fa-bars"></i></span>
</div>

<div id="sidebar-profile" style="background: url({{ asset('img/smr-green-bg.png') }}) no-repeat center center">
  <div class="overlay"></div>
  <a href="{{ route('profile') }}">
    <div class="img-frame object-fit-cover">
      <img src="{{ asset(imgTagShow(auth()->user()->photo,'profile')) }}" class="img-circle">
    </div>
    <p class="text-center">{{ auth()->user()->name }}</p>
  </a>
</div>

<div id="sidebar-list">
  <ul class="list-unstyled">
    <li class="li-separator">General</li>
    <li class="{{ activeOnCurrentRoute('dashboard') }}">
      <a href="{{ route('dashboard') }}">Dashboard</a>
    </li>

    @if (ACLAllButtonsCheck(array('USR_MGMT','USR_GRP_MGMT','ADT_MGMT'),$action = 'Read'))
      <li>
        <a href="#" class="list-group-title" data-toggle="collapse" data-target="#adminPanel">Administrative <span class="caret"></span></a>
        <ul class="collapse" id="adminPanel">
        @if(ACLButtonCheck('USR_MGMT',$action = 'Read'))
          <li class="{{ activeOnCurrentRoute('user') }}">
            <a href="{{ route('user') }}"><i class="fa fa-user"></i>User</a>
          </li>
        @endif
        @if(ACLButtonCheck('USR_GRP_MGMT',$action = 'Read'))
          <li class="{{ activeOnCurrentRoute('acl|\busergroup') }}">
            <a href="{{ route('usergroup') }}"><i class="fa fa-users"></i>User Group</a>
          </li>
        @endif
        @if(ACLButtonCheck('ADT_MGMT',$action = 'Read'))
          <li class="{{ activeOnCurrentRoute('audit_trail') }}">
            <a href="{{ route('audit_trail') }}"><i class="fa fa-history"></i>Audit Trail</a>
          </li>
        @endif
        </ul>
      </li>
    @endif

    <li class="li-separator">Projects</li>

    @foreach (config('cms.applications') as $code => $application)
      <?php $modules = array(); ?>
      @foreach (config('cms.menus') as $module)
        <?php
          if (preg_match('/^'.$code.'_/',$module)) {
            $modules[] = $module;
          }
        ?>
      @endforeach

      @if (ACLAllButtonsCheck($modules,$action = 'Read'))
        <li>
          <a href="#" class="list-group-title" data-toggle="collapse" data-target="#{{ $code }}">{{ $application }} <span class="caret"></span></a>
          <ul class="collapse" id="{{ $code }}">
          @foreach ($modules as $module)
            @if(ACLButtonCheck($module,$action = 'Read'))
              <li class="{{ activeOnCurrentRoute(config('cms.navbar_active_route.'.$module)) }}">
                <a href="{{ route(config('cms.index_route_name.'.$module)) }}"><i class="{{ config('cms.icons.'.$module) }}"></i>{{ config('cms.modules.'.$module) }}</a>
              </li>
            @endif
          @endforeach
          </ul>
        </li>
      @endif

    @endforeach
  </ul>
</div>