@extends('template')

@section('title')
	{{ config('cms.applications.SAS') }}
@stop

@section('page-header')
	{{ config('cms.applications.SAS') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<h4>Create {{ config('cms.modules.SAS_STU') }}</h4>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>'sas.student.store','name'=>'register_form','class'=>'form-horizontal','method'=>'post','files'=>true)) !!}
        <div class="form-group">
			{!! Form::label('photo', 'Photo', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				<div class="col-sm-12 imgForm-container">
					<div class="imgBrowse-img-frame object-fit-contain">
						<img id="previewHere" src="{{ asset(imgTagShow('','default')) }}">
					</div>
					<div class="imgBrowse-browse-frame">
						{!! Form::file('photo', array('class'=>"form-control", 'id'=>'img-browse', 'style'=>'display:none')) !!}
						<button type="button" class="btn btn-basic" onclick="javascript: $('#img-browse').trigger('click');">
							<i class='fa fa-lg fa-camera' data-toggle="tooltip" title="Browse photo"></i>
						</button>
						{!! Form::text('', '', array('class'=>"form-control", 'readonly'=>"readonly")) !!}
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('name', old('name'), array('class'=>"form-control",'placeholder'=>"Name")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('age', 'Age', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('age', old('age'), array('class'=>"form-control",'placeholder'=>"Age")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('contact_name_1', 'Contact Name 1', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('contact_name_1', old('contact_name_1'), array('class'=>"form-control",'placeholder'=>"Contact Name 1")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('contact_number_1', 'Contact Number 1', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('contact_number_1', old('contact_number_1'), array('class'=>"form-control",'placeholder'=>"Contact Number 1")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('contact_name_2', 'Contact Name 2', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('contact_name_2', old('contact_name_2'), array('class'=>"form-control",'placeholder'=>"Contact Name 2")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('contact_number_2', 'Contact Number 2', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('contact_number_2', old('contact_number_2'), array('class'=>"form-control",'placeholder'=>"Contact Number 2")) !!}
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-7">
				<a href="{{ route('sas.student') }}" class='btn btn-back'>Back</a>
				{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
			</div>
		</div>
		{!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){

});
</script>
@stop