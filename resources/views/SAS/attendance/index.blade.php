@extends('template')

@section('title')
	{{ config('cms.applications.SAS') }}
@stop

@section('page-header')
	{{ config('cms.applications.SAS') }}
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left"><h4>{{ config('cms.modules.SAS_ATT_REC') }}</h4></div>
		<div class="panel-heading-right">
			<button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button>
		</div>
	</div>
	<div class="panel-body">
		<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						<th>{!! sortTableHeaderSnippet('Check In','created_at') !!}</th>
						<th>Photo</th>
						<th>{!! sortTableHeaderSnippet('Name','student-name') !!}</th>
						<th>{!! sortTableHeaderSnippet('Status','student-status') !!}</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($Attendances as $Attendance)
					<tr>
						<td>{{ $Attendance->created_at->format('d/M/Y h:iA') }}</td>
						<td>
							<div class="table-img-frame object-fit-cover" alt="{{ $Attendance->student->name }} image">
								<img src="{{ asset(imgTagShow($Attendance->student->photo,'profile')) }}">
							</div>
						</td>
						<td>{{ $Attendance->student->name }}</td>
						<td>{{ $Attendance->student->status }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::label('created_at', 'Check In') !!}
					{!! searchTableHeaderSnippet('created_at') !!}
				</div>
				<div class="form-group">
					{!! Form::label('name', 'Name') !!}
					{!! searchTableHeaderSnippet('student-name') !!}
				</div>
				<div class="form-group">
					{!! Form::label('status', 'Status') !!}
					{!! searchTableHeaderSnippet('student-status') !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $Attendances->appends(request()->all())->render()) !!}
</div>	

@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop