@extends('template')

@section('title')
	Sample
@stop

@section('page-header')
	Sample
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<h4>Edit Sample</h4>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>array('sample.update',$Sample->id),'name'=>'register_form','class'=>'form-horizontal','method'=>'put','files'=>true)) !!}
		<div class="form-group">
			{!! Form::label('content', 'Content', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('content', old('content',$Sample->content), array('class'=>"form-control",'placeholder'=>"Content")) !!}
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-7">
				<a href="{{ route('sample') }}" class='btn btn-back'>Back</a>
				{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
			</div>
		</div>
		{!! Form::close() !!}
    </div>
</div>

@stop

@section('scripts')
<script>
$(function(){
});
</script>
@stop