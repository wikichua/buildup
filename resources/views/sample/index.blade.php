@extends('template')

@section('title')
	Navigations
@stop

@section('page-header')
	Navigations
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left"><h4>Sample</h4></div>
		<div class="panel-heading-right">
			<button type="button" class="btn btn-basic" data-toggle="modal" data-target=".modal-search">
				<i class="fa fa-search" data-toggle="tooltip" title="Search"></i>
			</button>
			@if (ACLButtonCheck('SAMPLE_MGMT','Create'))
	       		<span>{!! action_add_button(route('sample.create')) !!}</span>
	        @endif
		</div>
	</div>
	<div class="panel-body">
		<div class="basic-table-container">
        	<table class="table table-responsive table-hover">
	        	<thead>
					<tr>
						<th class="col-sm-1 shift_column"></th>
						<th>{!! sortTableHeaderSnippet('Content','content') !!}</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($Samples as $Sample)
					<tr>
						<td class="shift_column">
							{!! shifting_buttons($Sample,route('sample.shift',array($Sample->id,$Sample->p_id)),route('sample.shift',array($Sample->id,$Sample->n_id))) !!}
						</td>
						<td>{{ $Sample->content }}</td>
						<td class="table-btn-action text-center">
							@if (ACLButtonCheck('SAMPLE_MGMT','Update'))
							<a class="btn btn-basic" href="{{ route('sample.edit',array($Sample->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('SAMPLE_MGMT','Delete'))
							<a class="btn btn-basic delete" data-href="{{ route('sample.destroy',array($Sample->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade modal-search" role="dialog">
	<div class="modal-dialog">
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-search"></i>Search Fields</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::label('content', 'Content') !!}
					{!! searchTableHeaderSnippet('content') !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! search_reset_buttons() !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="text-center">
	{!! str_replace('/?', '?', $Samples->appends(request()->all())->render()) !!}
</div>	

@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
	@if(isSearchOrSortExecuted() === 'false')
		$('.shift_column').remove();
	@endif
});
</script>
@stop