@extends('template')

@section('title')
	Sample
@stop

@section('page-header')
	Sample
@stop

@section('body')

<div class="panel-container">
	<div class="panel-heading">
		<div class="panel-heading-left">
			<h4>Create Sample</h4>
		</div>
	</div>
	<div class="panel-body">
        {!! Form::open(array('route'=>'sample.store','name'=>'register_form','class'=>'form-horizontal','method'=>'post','files'=>true)) !!}
        <div class="form-group">
			{!! Form::label('image', 'Image', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				<div class="col-sm-12 imgForm-container">
					<div class="imgBrowse-img-frame object-fit-contain">
						<img id="previewHere" src="{{ asset(imgTagShow('','default')) }}">
					</div>
					<div class="imgBrowse-browse-frame">
						{!! Form::file('image', array('class'=>"form-control", 'id'=>'img-browse', 'style'=>'display:none')) !!}
						<button type="button" class="btn btn-basic" onclick="javascript: $('#img-browse').trigger('click');">
							<i class='fa fa-lg fa-camera' data-toggle="tooltip" title="Browse image"></i>
						</button>
						{!! Form::text('', '', array('class'=>"form-control", 'readonly'=>"readonly")) !!}
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('content', 'Content', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::textarea('content', old('content'), array('class'=>"form-control",'placeholder'=>"Content",'rows'=>3)) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('datetime', 'Date Time', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('datetime', old('datetime'), array('class'=>"form-control datetimepicker",'placeholder'=>"datetime")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('date', 'Date', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('date', old('date'), array('class'=>"form-control datepicker",'placeholder'=>"date")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('time', 'Time', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::text('time', old('time'), array('class'=>"form-control timepicker",'placeholder'=>"time")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('select', 'Select', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::select('select',array('A','B','C'), old('select'), array('class'=>"form-control select2",'placeholder'=>"select")) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('select', 'Select', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::select('select',array('A','B','C'), old('select'), array('class'=>"form-control select2",'multiple')) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('editor', 'Editor', array('class'=>'col-sm-3 control-label')) !!}
			<div class="col-sm-7">
				{!! Form::textarea('editor', old('editor'), array('class'=>"form-control editor",'placeholder'=>"editor",'rows'=>3)) !!}
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-7">
				<a href="{{ route('sample') }}" class='btn btn-back'>Back</a>
				{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
			</div>
		</div>
		{!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){

});
</script>
@stop